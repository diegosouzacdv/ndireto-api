package com.ndireto.api.storage;

import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import com.ndireto.api.dto.ArquivoDTO;

public class ArquivoStorageRunnable implements Runnable {
	
	private MultipartFile[] files;
	private DeferredResult<ArquivoDTO> resultado;
	private ArquivoStorage arquivoStorage;
	private String local; 
	
	public ArquivoStorageRunnable(MultipartFile[] files, DeferredResult<ArquivoDTO> resultado, ArquivoStorage arquivoStorage,String local) {
		this.files = files;
		this.resultado = resultado;
		this.arquivoStorage = arquivoStorage;
		this.local = local;
	}

	@Override
	public void run() {
		String nomeArquivo = this.arquivoStorage.salvarTemporariamente(files,this.local);
		String contentType = files[0].getContentType();
		resultado.setResult(new ArquivoDTO(nomeArquivo, contentType,this.local));
	}

}
