package com.ndireto.api.storage.local;



import static java.nio.file.FileSystems.getDefault;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.ndireto.api.storage.ArquivoStorage;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;


@Profile("local")
@Component
public class ArquivoStorageLocal implements ArquivoStorage {


		private static final Logger logger = LoggerFactory.getLogger(ArquivoStorageLocal.class);
		
		private Path local;
		private Path localTemporario;

		
		
		public Path getLocal() {
			
			this.local = getDefault().getPath(defaultDirectory());
			
			return local;
		}


		public void setLocal(Path local) {
			this.local = local;
		}


		public Path getLocalTemporario() {
			
			return localTemporario;
		}


		public void setLocalTemporario(Path localTemporario) {
			this.localTemporario = localTemporario;
		}


		private static String defaultDirectory()
		{
		    String OS = System.getProperty("os.name").toUpperCase();
		    
		    String nome = "";
		    
		    if (OS.contains("WIN"))
		    	nome = System.getenv("APPDATA");
		    else if (OS.contains("MAC"))
		    	nome = System.getProperty("user.home") + "/Library/Application "
		                + "Support";
		    else if (OS.contains("NUX"))
		    	nome = File.separator+"usr"+File.separator+"share"+File.separator+"tomcat8";
		   
		    return nome;
		}
		
		
		public ArquivoStorageLocal() {
			this(getDefault().getPath(defaultDirectory()));
		}
		
		public ArquivoStorageLocal(Path path) {
			this.local = path;
			
		}
		
		

		@Override
		public String salvarTemporariamente(MultipartFile[] files,String diretorio) { 
			
			
			criarPastas(diretorio);
			
			String novoNome = null;
			if (files != null && files.length > 0) {
				MultipartFile arquivo = files[0];
				novoNome = renomearArquivo(arquivo.getOriginalFilename());
				
				String local =  defaultDirectory()+getDefault().getSeparator()+diretorio+getDefault().getSeparator()+"temp"+getDefault().getSeparator()+ novoNome;
				
				try {
					arquivo.transferTo(new File(local));
				} catch (IOException e) {
					throw new RuntimeException("Erro salvando a foto na pasta temporária"); 
				}
				
				
			}
			salvar(novoNome,diretorio);
			return novoNome;
		}
		
		@Override
		public byte[] recuperarFotoTemporaria(String nome) {
			try {
				return Files.readAllBytes(this.local.resolve(nome));
			} catch (IOException e) {
				throw new RuntimeException("Erro lendo a foto temporária");
			}
		}
		
		@Override
		public void salvar(String arquivo,String diretorio) {
			
			this.local = getDefault().getPath(defaultDirectory()+getDefault().getSeparator()+diretorio);
			this.localTemporario = getDefault().getPath(defaultDirectory()+getDefault().getSeparator()+diretorio+getDefault().getSeparator()+"temp");
			
			try {
				Files.move(this.localTemporario.resolve(arquivo), this.local.resolve(arquivo));
			} catch (IOException e) {
				throw new RuntimeException("Erro movendo a foto para destino final");
			}
			
			if(!arquivo.toLowerCase().endsWith(".pdf")){
				
				try {
					Thumbnails.of(this.local.resolve(arquivo).toString()).size(842,1191).toFile(this.local.resolve(arquivo).toString());
					Thumbnails.of(this.local.resolve(arquivo).toString()).size(70, 70).toFiles(Rename.PREFIX_DOT_THUMBNAIL);
					
				} catch (IOException e) {
					throw new RuntimeException("Erro gerando thumbnail");
				}
			}
			
			try {
			Files.deleteIfExists(localTemporario);
			} catch (IOException e) {
				throw new RuntimeException("Erro criando pasta para salvar foto");
			}
			
		}
		
		@Override
		public byte[] recuperar(String nome,String diretorio) {
			this.local = getDefault().getPath(defaultDirectory()+File.separator+diretorio);
			System.err.println(defaultDirectory()+File.separator+diretorio);
			try {
				return Files.readAllBytes(this.local.resolve(nome));
			} catch (IOException e) {
				throw new RuntimeException("Erro lendo a foto");
			}
		}
		
		
		private void criarPastas(String local) {
			try {
				
				
				Files.createDirectories(getDefault().getPath(defaultDirectory(),local));
				//this.localTemporario = getDefault().getPath(this.local.toString(), "temp");
				Files.createDirectories(getDefault().getPath(defaultDirectory()+getDefault().getSeparator()+local, "temp"));
				
				
			} catch (IOException e) {
				throw new RuntimeException("Erro criando pasta para salvar foto");
			}
		}
		
		private String renomearArquivo(String nomeOriginal) {
			
			
			String tem[] = nomeOriginal.split("\\."); 
			
			String novoNome = getNomeAleatorio(5) + "." +tem[tem.length-1];
			
			
			if (logger.isDebugEnabled()) {
				logger.debug(String.format("Nome original: %s, novo nome: %s", nomeOriginal, novoNome));
			}
			
			return novoNome;
			
		}


		@Override
		public int remover(String nome, String diretorio) {
				
			this.local = getDefault().getPath(defaultDirectory()+File.separator+diretorio);
			
			try {
				Files.deleteIfExists(this.local.resolve(nome));
				Files.deleteIfExists(this.local.resolve("thumbnail."+nome));
				return 1;
			} catch (IOException e) {
				return 0;
			}
		}

		public String getNomeAleatorio(int tamanho) {
			char[] chart ={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
			char[] senha= new char[tamanho];
			int chartLenght = chart.length;
			Random rdm = new Random();
			
			for (int i=0; i<tamanho; i++) {
				senha[i] = chart[rdm.nextInt(chartLenght)];
			}
			
			return new String(senha);
		
		}
	
}