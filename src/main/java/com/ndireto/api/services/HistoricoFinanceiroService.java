package com.ndireto.api.services;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndireto.api.entity.HistoricoFinanceiro;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.repository.HistoricoFinanceiroRepository;
import com.ndireto.api.repository.TipoHistoricoFinanceiroRepository;

@Service
public class HistoricoFinanceiroService {

	@Autowired
	private HistoricoFinanceiroRepository historicoFinanceiroRepository;
	
	@Autowired
	private TipoHistoricoFinanceiroRepository tipoHistoricoFinanceiroRepository;
	
	
	
	public void gerarDebitoAnuidadeParaAnoPosterior(Pessoa pessoa,Pessoa usuario) {
		
		HistoricoFinanceiro historicoFinanceiro = new HistoricoFinanceiro();
		historicoFinanceiro.setDataMovimentacao(LocalDateTime.now().plusYears(1));
		historicoFinanceiro.setValor(99f);
		historicoFinanceiro.setPessoa(pessoa);
		historicoFinanceiro.setCodigoUsuario(usuario);
		historicoFinanceiro.setTipoHistoricoFinanceiro(tipoHistoricoFinanceiroRepository.findById(3));
		historicoFinanceiroRepository.save(historicoFinanceiro);
		
		//ENVIAR EMAL AVISANDO DA COBRANÇA
		//mailer.enviarAvisoNovoHistoricoFinanceiroAnuidade(pessoa);	
		
	}

	public void marcarLida(HistoricoFinanceiro historicoFinanceiro) {
		historicoFinanceiro.setDataVisualizacao(LocalDateTime.now());
		historicoFinanceiroRepository.saveAndFlush(historicoFinanceiro);
	}
	
	
	
}
