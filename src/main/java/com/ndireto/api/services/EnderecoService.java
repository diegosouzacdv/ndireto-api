package com.ndireto.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndireto.api.entity.Endereco;
import com.ndireto.api.repository.EnderecoRepository;

@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository enderecoRepository;

	public void salvarEndereco(Endereco endereco) {
		// TODO Auto-generated method stub
		
		enderecoRepository.saveAndFlush(endereco);
	}

	public void excluirEndereco(Integer id) {
		if(id!=0) {
		enderecoRepository.deleteById(id);
		}
		
	}
	
	

}
