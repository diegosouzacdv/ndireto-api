package com.ndireto.api.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.ndireto.api.dto.SmsDTO;
import com.ndireto.api.entity.Pessoa;


@Service
public class SmsService {
	  
	static String API_KEY = "6657b128-43e6-44bb-a273-305aa7739938";
	  
	
	@Autowired
	private MensagemService mensagemService;
	
	  public void enviarSMS(SmsDTO smsDTO){
		  Gson gson = new Gson();
		  
		  RestTemplate restTemplate = new RestTemplate();
		 
		  HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		   
		    String parametros = gson.toJson(smsDTO);
		    
		    parametros = parametros.replaceAll("auth_key", "auth-key"); 
		    
		    HttpEntity<String> request = 
		    	      new HttpEntity<String>(parametros, headers);
		    
		    String retorno = 
		    	      restTemplate.postForObject("https://sms.comtele.com.br/api/v2/send?auth-key=6657b128-43e6-44bb-a273-305aa7739938", request, String.class);
	  
	  }

	public void enviarMensagemBoasVindas(Pessoa pessoa) {
		
		SmsDTO sms = new SmsDTO();
		sms.setReceivers(pessoa.getTelefoneZap());
		sms.setContent(mensagemService.getTextoBoasVindasSms(pessoa));
//		sms.setContent("Ola"+pessoa.getNome());
		sms.setSender("BoasVindas");
		
		enviarSMS(sms);
		
	}
	
		public void enviarMensagemJaCadastrado(Pessoa pessoa) {
		
		SmsDTO sms = new SmsDTO();
		sms.setReceivers(pessoa.getTelefoneZap());
		sms.setContent(mensagemService.getJatemosSeuCadastro(pessoa));
//		sms.setContent("Ola"+pessoa.getNome());
		sms.setSender("BoasVindas");
		
		enviarSMS(sms);
		
	}
	
	
	
}
