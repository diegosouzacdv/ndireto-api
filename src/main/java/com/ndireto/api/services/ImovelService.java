package com.ndireto.api.services;

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.ndireto.api.dto.EstruturaImovelDTO;
import com.ndireto.api.dto.ItensImovelDTO;
import com.ndireto.api.dto.LocalizacaoDTO;
import com.ndireto.api.dto.TerrenoImovelDTO;
import com.ndireto.api.dto.ValoresImovelDTO;
import com.ndireto.api.entity.Imovel;
import com.ndireto.api.entity.ImovelItens;
import com.ndireto.api.entity.Itens;
import com.ndireto.api.repository.ImovelItensRepository;
import com.ndireto.api.repository.ImovelRepository;

@Service
public class ImovelService {

	@Autowired
	private ImovelRepository imovelRepository;
	
	@Autowired
	private ItensService itensService;
	
	@Autowired 
	ImovelItensRepository imovelItensRepository;
	

	public void atribuirErros(@Valid Imovel imovel, BindingResult result) {
		// TODO Auto-generated method stub

		if(imovel.getTipoImovel()==null){
			result.rejectValue("tipoImovel","","Tipo de Imóvel é Obrigatório");
		}

		if(imovel.getTipoNegociacao()==null){
			result.rejectValue("tipoNegociacao","","Tipo de Negociação é Obrigatório");
		}

		if(imovel.getValor()==null){
			result.rejectValue("valor","","Valor é Obrigatório");
		}

	}

	public Imovel salvar(@Valid Imovel imovel) {
		// TODO Auto-generated method stub
		return imovelRepository.saveAndFlush(imovel);

	}



	public  String[] getNullPropertyNames(Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();

		for (PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (srcValue == null ) {
				emptyNames.add(pd.getName());
			}
		}

		String[] result = new String[emptyNames.size()];
		return emptyNames.toArray(result);
	}

	public void verificaCaracteristicas(Imovel imovelSalvo, Imovel imovel) {


		if(imovel.getTipoImovel()!=null && imovelSalvo.getTipoImovel()!=null && imovelSalvo.getTipoImovel().getId()!=imovel.getTipoImovel().getId()) {
			imovelSalvo.setTipoImovel(imovel.getTipoImovel());
		}
		else {
			imovelSalvo.setTipoImovel(null);
		}
		if(imovel.getTipoNegociacao()!=null && imovelSalvo.getTipoNegociacao()!=null && imovelSalvo.getTipoNegociacao().getId()!=imovel.getTipoNegociacao().getId()) {
			imovelSalvo.setTipoNegociacao(imovel.getTipoNegociacao());
		}
		else {
			imovelSalvo.setTipoNegociacao(null);
		}

	}


	public void verificaValores(Imovel imovelSalvo, Imovel imovel) {


		if(imovel.getMotivoVenda()!=null && imovelSalvo.getMotivoVenda()!=null &&  imovelSalvo.getMotivoVenda().getId()!=imovel.getMotivoVenda().getId()) {
			imovelSalvo.setMotivoVenda(imovel.getMotivoVenda());
		}

		else{
			imovelSalvo.setMotivoVenda(null);
		}

		if(imovelSalvo.getValorIPTU()!=null && imovelSalvo.getValorIPTU()!=imovel.getValorIPTU()) {
			imovelSalvo.setValorIPTU(imovel.getValorIPTU());
		}

		if(imovelSalvo.getValorCondominio()!=null &&imovelSalvo.getValorCondominio()!=imovel.getValorCondominio()) {
			imovelSalvo.setValorCondominio(imovel.getValorCondominio());
		}


		if(imovelSalvo.getValor()!=null &&!imovelSalvo.getValor().equals(imovel.getValor())) {
			imovelSalvo.setValor(imovel.getValor());
		}

	}
	
	public List<Imovel> buscarTodos() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		return imoveis;
	}
	
	public Integer buscarQuantidadeTodosImoveis() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		Integer quantidade = imoveis.size();
		return quantidade;
	}
	
	
	public List<Imovel> buscarAnunciados() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		return imoveis;
	}
	
	public Integer buscarQuantidadeAnunciadosImoveis() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		Integer quantidade = imoveis.size();
		return quantidade;
	}
	
	
	public List<Imovel> buscarMeus() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		return imoveis;
	}
	
	public Integer buscarQuantidadeMeusImoveis() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		Integer quantidade = imoveis.size();
		return quantidade;
	}
	
	
	public List<Imovel> buscarFotografados() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		return imoveis;
	}
	
	public Integer buscarQuantidadeFotografadosImoveis() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		Integer quantidade = imoveis.size();
		return quantidade;
	}
	
	public List<Imovel> buscarVisitados() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		return imoveis;
	}
	
	public Integer buscarQuantidadeVisitadosImoveis() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		Integer quantidade = imoveis.size();
		return quantidade;
	}
	
	
	public List<Imovel> buscarDisponiveisParaVisitaCorretor() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		return imoveis;
	}
	
	public Integer buscarQuantidadeDisponiveisParaVisitaCorretor() {
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastroDesc(2);
		Integer quantidade = imoveis.size();
		return quantidade;
	}
	
	


	public Imovel buscar(Integer codigoImovel) {
		Optional<Imovel> opt = imovelRepository.findById(codigoImovel);
		Imovel retorno = new Imovel();
		if(opt.isPresent()) {
			retorno = opt.get();
		}
		return retorno;
	}

	/**
	 * VERIFICA SE O IMOVEL TEM UM ITEM PASSADO COMO PARAMETRO
	 * @param itensImovelDTO
	 * @return
	 */
	public boolean contemItem(ItensImovelDTO itensImovelDTO) {
		//Itens item = itensService.buscar(itensImovelDTO.getCodigoItem()); //
		Imovel imovel = buscar(itensImovelDTO.getCodigoImovel()); //itensRepository.findById(itensImovelDTO.getCodigoItem());
		
		for(Integer obj: imovel.getItensMarcados()) {
			if(obj.equals(itensImovelDTO.getCodigoItem())) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * REMOVER UM ITEM DE UM IMÓVEL
	 * @param itensImovelDTO
	 */
	public void removeItem(ItensImovelDTO itensImovelDTO) {
		Imovel imovel = buscar(itensImovelDTO.getCodigoImovel()); 
		ImovelItens imovelItens = new ImovelItens();
		
		for(ImovelItens obj: imovel.getImovelItens()) {
			if(obj.getItens().getId()==itensImovelDTO.getCodigoItem().intValue()) {
				imovelItens = obj;
				break;
			}
		}
		System.err.println(itensImovelDTO);
		imovelItensRepository.delete(imovelItens.getId());
		
	}

	/**
	 * ADICIONAR UM ITEM A UM IMÓVEL
	 * @param itensImovelDTO
	 */
	public void adicionaItem(ItensImovelDTO itensImovelDTO) {
		ImovelItens imovelItens = new ImovelItens();
		Imovel imovel = buscar(itensImovelDTO.getCodigoImovel()); 
		Itens iten = itensService.buscar(itensImovelDTO.getCodigoItem());
		
		imovelItens.setImovel(imovel);
		imovelItens.setItens(iten);
		
		System.err.println(imovelItens.toString());
		imovelItensRepository.save(imovelItens);
		
	}

	public EstruturaImovelDTO estruturaImovel(EstruturaImovelDTO estruturaImovelDTO) {
		Imovel imovel = imovelRepository.findOne(estruturaImovelDTO.getCodigoImovel());
		if (imovel != null) {
			imovel.setQtdBanheiros(estruturaImovelDTO.getQtdBanheiros());
			imovel.setQtdQuartos(estruturaImovelDTO.getQtdQuartos());
			imovel.setQtdSuites(estruturaImovelDTO.getQtdSuites());
			imovel.setVagas(estruturaImovelDTO.getVagas());
		}
		imovelRepository.save(imovel);
		return estruturaImovelDTO;
	}
	
	
	public LocalizacaoDTO localizacaoImovel(LocalizacaoDTO localizacaoDTO) {
		Imovel imovel = imovelRepository.findOne(localizacaoDTO.getCodigoImovel());
		if (imovel != null) {
			imovel.setLatitude(localizacaoDTO.getLatitude());
			imovel.setLongitude(localizacaoDTO.getLongitude());
		}
		imovelRepository.save(imovel);
		return localizacaoDTO;
	}

	public TerrenoImovelDTO terrenoImovel(TerrenoImovelDTO terrenoImovelDTO) {
		Imovel imovel = imovelRepository.findOne(terrenoImovelDTO.getCodigoImovel());
		if (imovel != null) {
			imovel.setAreautil(terrenoImovelDTO.getAreautil());
			imovel.setAreatotal(terrenoImovelDTO.getAreatotal());
		}
		imovelRepository.save(imovel);
		return terrenoImovelDTO;
	}

	public ValoresImovelDTO valoresImovel(ValoresImovelDTO valoresImovelDTO) {
		
		Imovel imovel = imovelRepository.findOne(valoresImovelDTO.getCodigoImovel());
		if (imovel != null) {
			imovel.setValor(valoresImovelDTO.getValor());
			imovel.setValorCondominio(valoresImovelDTO.getValorCondominio());
			imovel.setValorIPTU(valoresImovelDTO.getValorIPTU());
		}
		imovelRepository.save(imovel);
		return valoresImovelDTO;
	}



}
