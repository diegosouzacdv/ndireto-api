package com.ndireto.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndireto.api.dto.AgendarVisitaDTO;
import com.ndireto.api.entity.Imovel;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.StatusImovel;
import com.ndireto.api.entity.TipoVisita;
import com.ndireto.api.entity.Visita;
import com.ndireto.api.mail.Mailer;
import com.ndireto.api.repository.DiasSemanaRepository;
import com.ndireto.api.repository.DisponibilidadeVisitaImovelRepository;
import com.ndireto.api.repository.StatusImovelRepository;
import com.ndireto.api.repository.VisitaRepository;
import com.ndireto.api.storage.ArquivoStorage;

@Service
public class VisitaService {
	
	
	@Autowired
	private ImovelService  imovelService;
	
	@Autowired
	private ArquivoStorage arquivoStorage;
	
	@Autowired
	private AnexoService anexoService;
	
	@Autowired
	private DiasSemanaRepository diasSemanaRepository;

	@Autowired
	private DisponibilidadeVisitaImovelRepository disponibilidadeVisitaImovelRepository;
	
	@Autowired
	private VisitaRepository visitaRepository;
	
	@Autowired
	private MensagemService mensagemService;
	
	@Autowired
	private Mailer mailer;
	
	@Autowired
	private StatusImovelRepository statusImovelRepository;
	
	
	public Visita salvarNovaVisitaFotografo(AgendarVisitaDTO visitaDto) {
		
		Visita  retorno  = new Visita();
		retorno = visitaDto.fromObjeto();
		
		Pessoa pessoaCadastrante = new Pessoa();
		Pessoa pessoaComprador = new Pessoa();
		
		pessoaCadastrante.setId(visitaDto.getCadastranteCodigo());
		pessoaComprador.setId(visitaDto.getCadastranteCodigo());
		retorno.setPessoaCadastrante(pessoaCadastrante);
		retorno.setPessoaComprador(pessoaComprador);
		
		TipoVisita tipoVisita =  new TipoVisita();
		
		tipoVisita.setId(2);
		
		retorno.setTipoVisita(tipoVisita);
		
		retorno = visitaRepository.save(retorno);
		
		mensagemService.enviarMensagemVisita(retorno);
		//enviarEmailVisita(retorno);
		
		
		Imovel imovel =  imovelService.buscar(retorno.getImovel().getId());
		StatusImovel status = 	statusImovelRepository.findById(13);
		imovel.setStatusImovel(status);
		imovelService.salvar(imovel);
		
		return retorno;
		
	}
	
	public void enviarEmailVisita(Visita visita) {
		mailer.enviarMensagemVisita(visita);

	}
	
	

}
