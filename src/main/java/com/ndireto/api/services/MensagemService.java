package com.ndireto.api.services;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.ndireto.api.entity.Imovel;
import com.ndireto.api.entity.Mensagem;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.UsuarioSistema;
import com.ndireto.api.entity.Visita;
import com.ndireto.api.repository.MensagemRepository;
import com.ndireto.api.repository.PessoaRepository;

@Service
public class MensagemService {
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	public void salvar(Mensagem mensagem) {
		mensagemRepository.save(mensagem);
		
	}
	
	public Mensagem salvarRetorno(Mensagem mensagem) {
		 mensagem = mensagemRepository.saveAndFlush(mensagem);
		return mensagem;
	}

	public void marcarLida(Mensagem mensagem) {
		mensagem.setDataRecebimento(LocalDateTime.now());
		mensagem.setAlerta(1);
		mensagemRepository.saveAndFlush(mensagem);
	}

	public void atribuirErros(Mensagem msg, BindingResult result) {
		if(msg.getDestinatario() == null || msg.getDestinatario().getId() == 0){
			result.rejectValue("destinatario.id","","Informe o Destinatário da Mensagem!");
		} 
		if(msg.getAssunto() == null || msg.getAssunto() == ""){
			result.rejectValue("assunto","","Informe um Assunto para a Mensagem!");
		} 
	}


	public void marcarLidaPopUp(Mensagem mensagem) {
		mensagem.setAlerta(1);
		mensagemRepository.save(mensagem);
	}

	public Mensagem salvarNovaMsg(@Valid Mensagem mensagem, UsuarioSistema usuario) {
		// TODO Auto-generated method stub
		mensagem.setRemetente(usuario.getPessoa());
		mensagem.setDataInclusao(LocalDateTime.now());
		mensagem.setAtivo(1);
		mensagem.setAlerta(0);
		
		return mensagemRepository.saveAndFlush(mensagem);
	}

	public void incluirAcaoMsg(Mensagem msg) {
		// TODO Auto-generated method stub
		msg.setAcao("ler/"+msg.getId());
		mensagemRepository.saveAndFlush(msg);
	}

	public void enviarMensagemVisita(@Valid Visita visita) {
		// TODO Auto-generated method stub
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		//Enviando mensagem para o comprador
		Mensagem mensagem = new Mensagem();
		
		mensagem.setRemetente(visita.getPessoaCadastrante());
		mensagem.setDataInclusao(LocalDateTime.now());
		mensagem.setAssunto("Visita agendada no dia "+visita.getDataHoraVisita());
		mensagem.setDestinatario(visita.getPessoaComprador());
		mensagem.setObservacao(" Visita Agendada para o Imóvel situado em  "+visita.getImovel().getEnderecoFormatado()+" no dia "+visita.getDataHoraVisita().format(formatter)+" para mais detalhes acesse : http://ndireto.com.br/clube ");
		mensagem.setAtivo(1);
		mensagem.setAlerta(0);
		
		mensagem = mensagemRepository.saveAndFlush(mensagem);
		
		incluirAcaoMsg(mensagem);
		
		
		Mensagem mensagemProp = new Mensagem();
		
		mensagemProp.setRemetente(visita.getPessoaCadastrante());
		mensagemProp.setDataInclusao(LocalDateTime.now());
		mensagemProp.setAssunto("Você tem uma visita agendada no dia "+visita.getDataHoraVisita().format(formatter));
		mensagemProp.setDestinatario(visita.getImovel().getProprietario());
		mensagem.setObservacao("Visita Agendada para o Imóvel situado em  "+visita.getImovel().getEnderecoFormatado()+" no dia "+visita.getDataHoraVisita().format(formatter)+"  para mais detalhes acesse : http://ndireto.com.br/clube");
		mensagemProp.setAtivo(1);
		mensagemProp.setAlerta(0);
		
		mensagemProp = mensagemRepository.saveAndFlush(mensagemProp);
		
		incluirAcaoMsg(mensagemProp);
		
		
		if(visita.getPessoaColaborador()!=null) {
			Mensagem mensagemColab = new Mensagem();
			
			mensagemColab.setRemetente(visita.getPessoaCadastrante());
			mensagemColab.setDataInclusao(LocalDateTime.now());
			mensagemColab.setAssunto("Você tem uma visita agendada no dia "+visita.getDataHoraVisita());
			mensagemColab.setDestinatario(visita.getPessoaColaborador());
			mensagemColab.setObservacao("Negócio Direto - Visita Agendada para o Imóvel Código "+visita.getImovel().getId()+" no dia "+visita.getDataHoraVisita());
			mensagemColab.setAtivo(1);
			mensagemColab.setAlerta(0);
			
			mensagemColab = mensagemRepository.saveAndFlush(mensagemColab);
			
			incluirAcaoMsg(mensagemColab);
		}
	}
	
	
	
	public void enviarMensagemBoasVindas(Pessoa pessoa) {
		// TODO Auto-generated method stub
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		//Enviando mensagem para o comprador
		Mensagem mensagem = new Mensagem();
		
		String obsencode ="";
		try {
			obsencode = URLEncoder.encode(getTextoBoasVindas(pessoa),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			 obsencode = "";
		}
		
		
		mensagem.setRemetente(pessoa);
		mensagem.setDataInclusao(LocalDateTime.now());
		mensagem.setAssunto("Bem vindo ao clube");
		mensagem.setDestinatario(pessoa);
		mensagem.setObservacao(getTextoBoasVindas(pessoa));
		mensagem.setObservacaoencode(obsencode);
		mensagem.setAtivo(1);
		mensagem.setAlerta(0);
		mensagem = mensagemRepository.saveAndFlush(mensagem);
		
		incluirAcaoMsg(mensagem);
		
		
	}
	
	
	public void enviarMensagemJaCadastrado(Pessoa pessoa) {
		// TODO Auto-generated method stub
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		//Enviando mensagem para o comprador
		Mensagem mensagem = new Mensagem();
		
		String obsencode = "";
		
		try {
			obsencode = URLEncoder.encode(getJatemosSeuCadastro(pessoa),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			 obsencode = "";
		}
		
		mensagem.setRemetente(pessoa);
		mensagem.setDataInclusao(LocalDateTime.now());
		mensagem.setAssunto("Bem vindo");
		mensagem.setDestinatario(pessoa);
		mensagem.setObservacao(getJatemosSeuCadastro(pessoa));
		mensagem.setObservacao(obsencode);
		mensagem.setAtivo(1);
		mensagem.setAlerta(0);
		mensagem = mensagemRepository.saveAndFlush(mensagem);
		
		incluirAcaoMsg(mensagem);
		
		
	}
	
	public String  getTextoBoasVindasSms(Pessoa pessoa) {
		StringBuffer retorno =new StringBuffer();
		retorno.append("Ola *"+pessoa.getNome()+"* ");
		retorno.append("\r\nSeja bem-vinda(o) à Negócio Direto – criada por proprietários de imóveis – como você! ");
		retorno.append("\r\nNós da Negócio Direto avaliamos os custos de uma imobiliária e concluirmos que é possível cobrar um valor justo e, porque não fixo? As despesas para anunciar e vender um imóvel são fixas. Desse modo, criamos nosso próprio modelo de negócio - fazemos tudo que as melhores imobiliárias fazem, mas cobrarmos um valor fixo e condizente com o serviço prestado.");
		retorno.append("\r\nAssim, a ND cobra um valor fixo de R$ 15.000,00, independente do valor do seu imóvel.");
		retorno.append("\r\nVocê irá se surpreender! Faremos o possível para que a sua jornada de vender seu imóvel seja rápida, transparente e com total segurança! Aguarde o contato do nosso sócio fundador!");
		return retorno.toString();
		
		
	}
	
	public String  getTextoBoasVindas(Pessoa pessoa) {
		StringBuffer retorno =new StringBuffer();
		retorno.append("\n\nOlá *"+pessoa.getNome()+"* !");
		retorno.append("\n\nSeja bem-vinda(o) à Negócio Direto – criada por proprietários de imóveis – como você! ");
		retorno.append("\n\nNós da Negócio Direto avaliamos os custos de uma imobiliária e concluirmos que é possível cobrar um valor justo e, porque não fixo? As despesas para anunciar e vender um imóvel são fixas. Desse modo, criamos nosso próprio modelo de negócio - fazemos tudo que as melhores imobiliárias fazem, mas cobrarmos um valor fixo e condizente com o serviço prestado.");
		retorno.append("\n\nAssim, a ND cobra um valor fixo de R$ 15.000,00, independente do valor do seu imóvel.");
		retorno.append("\n\nVocê irá se surpreender! Faremos o possível para que a sua jornada de vender seu imóvel seja rápida, transparente e com total segurança! Aguarde o contato do nosso sócio fundador!");
		return retorno.toString();
		
		
	}
	
	public String  getJatemosSeuCadastro(Pessoa pessoa) {
		StringBuffer retorno =new StringBuffer();
		retorno.append("Olá, "+pessoa.getNome()+" a Negócio Direto agradece, ");
		retorno.append(" em breve entraremos em contato");
		
		return retorno.toString();
		
		
	}
	
	
	public String  getTextoImovelCadastrado(Pessoa pessoa) {
		
		StringBuffer retorno =new StringBuffer();
		retorno.append("Olá, "+pessoa.getNome()+" \r\n" + 
				"o cadastro do seu imóvel foi realizado com sucesso. ");
		retorno.append("A partir de agora, um profissional negócio direto fará uma visita agendada em seu imóvel para registrar fotos profissionais, esclarecer dúvidas do processo negócio direto e para te ajudar com dados do mercado para você definir o preço do seu imóvel. Com esta etapa concluída, somado, a análise documental do imóvel , estaremos aptos a dar o selo de segurança negócio direto e procederemos a divulgação do seu imóvel.");
		retorno.append("Lembramos que vc pode escolher demonstrar seu imóvel ou fazer uso de um consultor negócio direto para isso ( veja condições). ");
		retorno.append("Importante: Todas as pessoas que visitarem seu imóvel tem os seus dados validados na base de dados do Serpro - Data Valid, com o intuito de reduzir fraudes de identidade.");
		retorno.append(" Mãos à obra para Negócio Direto e para você - o maior interessado na venda do imóvel");
		retorno.append("\r\nObrigado pela confiança");
		retorno.append("\r\n Diretoria Negócio Direto");
		retorno.append("Na indicação de amigos para participar dessa nova experiência de vender um imóvel, você poderá  ganhar uma dinheiro");
		retorno.append(" extra compartilhando o link: http://ndireto.com.br/clube/registro?indic="+pessoa.getToken()+" Descubra como");
		
		return retorno.toString();
		
		
	}

	public void enviarMensagemImovelCadastroCompleto(Imovel imovel) {
		// TODO Auto-generated method stub
		
		Optional<Pessoa> pessoa  = pessoaRepository.findById(imovel.getProprietario().getId());
		
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
				//Enviando mensagem para o comprador
				Mensagem mensagem = new Mensagem();
				
				mensagem.setRemetente(pessoa.get());
				mensagem.setDataInclusao(LocalDateTime.now());
				mensagem.setAssunto("Imovel Cadastrado");
				mensagem.setDestinatario(pessoa.get());
				mensagem.setObservacao(getTextoImovelCadastrado(pessoa.get()));
				mensagem.setAtivo(1);
				mensagem.setAlerta(0);
				mensagem = mensagemRepository.saveAndFlush(mensagem);
		
	}
	
	
	
	

}
