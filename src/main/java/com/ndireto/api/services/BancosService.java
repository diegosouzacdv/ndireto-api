package com.ndireto.api.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndireto.api.entity.Banco;
import com.ndireto.api.repository.BancoRepository;

@Service
public class BancosService {

	@Autowired
	private BancoRepository bancoRepository;
	
	public List<Banco> buscarTodosAtivos() {
		List<Banco> retorno  =  bancoRepository.findByAtivoOrderByNome(1);
		return retorno;
	}
	
}
