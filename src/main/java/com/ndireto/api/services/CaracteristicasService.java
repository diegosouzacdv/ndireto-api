package com.ndireto.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndireto.api.entity.Caracteristicas;
import com.ndireto.api.repository.CaracteristicasRepository;

@Service
public class CaracteristicasService {

	@Autowired
	private CaracteristicasRepository caracteristicasRepository;

	public void salvarCaracteristica(Caracteristicas caracteristica) {
		// TODO Auto-generated method stub
		
		caracteristicasRepository.saveAndFlush(caracteristica);
	}
	
	

}
