package com.ndireto.api.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.TokenFirebase;
import com.ndireto.api.repository.PessoaRepository;
import com.ndireto.api.repository.TokenFirebaseRepository;

@Service
public class TokenFirebaseService {

	@Autowired
	private TokenFirebaseRepository tokenFirebaseRepository;

	@Autowired
	private PessoaRepository pessoaRepository;

	public TokenFirebase salvarToken(String token, @AuthenticationPrincipal Object usuario) {
		TokenFirebase tk = new TokenFirebase();
		Pessoa pessoa = pessoaRepository.getOne(Integer.valueOf(usuario.toString()));
		
		List<TokenFirebase> listToken = tokenFirebaseRepository
				.findByPessoaId(pessoa.getId());
		
		boolean salvaToken = true;
		if (!token.equals("null")) {
				for (TokenFirebase tokenFirebase : listToken) {
					if (tokenFirebase.getToken().equals(token)) {
						salvaToken = false;
					}
				}
		}
		
		if(!token.equals("null") && salvaToken) {
			tk =  this.salvandoToken(token, tk, pessoa);
		}
		
		return tk;
	}

	private TokenFirebase salvandoToken(String token, TokenFirebase tk, Pessoa pessoa) {
		tk.setPessoa(pessoa);
		tk.setToken(token);
		tk.setDataCadastro(LocalDateTime.now());
		return tokenFirebaseRepository.saveAndFlush(tk);
	}

}
