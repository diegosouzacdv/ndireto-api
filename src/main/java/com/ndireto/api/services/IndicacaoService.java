package com.ndireto.api.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.mail.Mailer;
import com.ndireto.api.repository.PessoaRepository;

@Service
public class IndicacaoService {
	
	@Autowired
	private PessoaRepository pessoaRepository;

	
	@Autowired
	private Mailer mailer;
	
	public void salvarIndicacao(Pessoa pessoa, String Indicacao) {
		Optional<Pessoa> pessoaIndicadora = pessoaRepository.findByToken(Indicacao);

		if(pessoaIndicadora.isPresent()){
			
			pessoa.setPessoaIndicador(pessoaIndicadora.get());
		}
	}

	public void salvarEmailAvisoIndicacao(Pessoa pessoaIndicadora,String nomeIndicado) {
		// TODO Auto-generated method stub
		mailer.enviarAvisoIndicacao(pessoaIndicadora,nomeIndicado);
	}
}
