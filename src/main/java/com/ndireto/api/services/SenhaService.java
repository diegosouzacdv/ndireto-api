package com.ndireto.api.services;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.ndireto.api.dto.AlterarSenhaDTO;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.mail.Mailer;
import com.ndireto.api.repository.PessoaRepository;

@Service
public class SenhaService {

	
	@Autowired
	PessoaRepository pessoaRepository;
	
	@Autowired
	private Mailer mailer;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	public void salvarSenhasEnviarEmail(Pessoa pessoa) {
		String senhaAleatoria  = getSenhaAleatoria(8);
		pessoaRepository.saveAndFlush(pessoa);
		//enviarEmailBoasVindas(pessoa,senhaAleatoria);		
	}
	
	public void salvarNovaSenhasEnviarEmail(Pessoa pessoa) {
		String senhaAleatoria  = getSenhaAleatoria(8);
		pessoa.setSenha(passwordEncoder.encode(senhaAleatoria));
		pessoaRepository.saveAndFlush(pessoa);
		
		enviarEmailBoasVindas(pessoa,senhaAleatoria);		
	}
	
	public void salvarNovaSenhasAlteradaEnviarEmail(Pessoa pessoa, String senha) {
		pessoaRepository.saveAndFlush(pessoa);
		enviarEmailBoasVindas(pessoa,senha);		
	}
	
	public String getSenhaAleatoria(int tamanho) {
		char[] chart ={'0','1','2','3','4','5','6','7','8','9'};
		char[] senha= new char[tamanho];
		int chartLenght = chart.length;
		Random rdm = new Random();

		for (int i=0; i<tamanho; i++) {
			senha[i] = chart[rdm.nextInt(chartLenght)];
		}
		return new String(senha);
	}
	
	
	/**
	 * ATENÇÃO SETAR A SENHA SEM CRIPTOGRAFIA PARA ENVIAR PARA O USUARIO
	 * @param pessoa
	 */
	public void enviarEmailBoasVindas(Pessoa pessoa,String senha) {
		mailer.enviar(pessoa,senha);

	}
	
	public void enviarEmailComNovaSenha(Pessoa pessoa,String senha) {
		mailer.enviarEsqueciSenha(pessoa,senha);

	}
	
	
	
	public void atribuirErros(AlterarSenhaDTO alterarSenhaDTO, Pessoa pessoa, BindingResult result) {
		
		if(alterarSenhaDTO.getRegisterPassword()==null|| alterarSenhaDTO.getRegisterPassword().isEmpty()) {
			result.rejectValue("registerPassword","","Nova é obrigatório");
		}
		if(alterarSenhaDTO.getRegisterPasswordConfirmation()==null|| alterarSenhaDTO.getRegisterPasswordConfirmation().isEmpty()) {
			result.rejectValue("registerPasswordConfirmation","","Confirme a Nova Senha é obrigatório");
		}
	}
}
