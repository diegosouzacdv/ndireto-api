package com.ndireto.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndireto.api.entity.PessoaTipoPessoa;
import com.ndireto.api.repository.PessoaTipoPessoaRepository;

@Service
public class PessoaTipoPessoaService {
	
	@Autowired
	private PessoaTipoPessoaRepository pessoaTipoPessoaRepository;
	
	public PessoaTipoPessoa salvarPessoaTipoPessoa(PessoaTipoPessoa pessoaTipo) {
		// TODO Auto-generated method stub
		return pessoaTipoPessoaRepository.saveAndFlush(pessoaTipo);
	}
	
	
}
