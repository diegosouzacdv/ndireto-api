package com.ndireto.api.services;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ndireto.api.entity.Itens;
import com.ndireto.api.entity.TipoItens;
import com.ndireto.api.repository.ItensRepository;
import com.ndireto.api.repository.TipoItensRepository;
import com.sun.mail.imap.protocol.Item;

@Service
public class ItensService {

	@Autowired
	private TipoItensRepository tipoItensRepository;	
	
	@Autowired
	private ItensRepository itensRepository;

	public TipoItens salvarTipoItens(@Valid TipoItens tipoItens) {
		// TODO Auto-generated method stub
		tipoItens.setAtivo(1);
		return tipoItensRepository.saveAndFlush(tipoItens);
		
	}

	public Itens salvarItens(@Valid Itens itens) {
		// TODO Auto-generated method stub
		itens.setAtivo(1);
		return itensRepository.saveAndFlush(itens);
	}

	public Itens buscar(Integer id) {
		Optional<Itens> opt = itensRepository.findById(id);
		Itens retorno = new Itens();
		if(opt.isPresent()) {
			retorno = opt.get();;
		}
		return retorno;
	}

}
