package com.ndireto.api.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import com.ndireto.api.dto.AssociadoDTO;
import com.ndireto.api.dto.NovaPessoaDTO;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.PessoaTipoPessoa;
import com.ndireto.api.entity.TipoPessoa;
import com.ndireto.api.enumeration.TipoPessoaImovelEnum;
import com.ndireto.api.repository.PessoaRepository;
import com.ndireto.api.repository.PessoaTipoPessoaRepository;
import com.ndireto.api.repository.StatusPessoaRepository;
import com.ndireto.api.repository.TipoPessoaRepository;
import com.ndireto.api.services.exceptions.ObjectFoundException;

@Service
public class PessoaService {
	
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Autowired
	private SenhaService senhaService;
	
	@Autowired
	private StatusPessoaRepository statusPessoaRepository;
	
	@Autowired
	private PessoaService  pessoaService;
	
	
	@Autowired
	private PessoaTipoPessoaRepository pessoaTipoPessoaRepository;	

	@Autowired
	private TipoPessoaRepository tipoPessoaRepository;
	
	@Autowired
	private IndicacaoService indicacaoService;
	
	@Transactional
	public Pessoa insert(Pessoa obj) {
		this.isExistePorEmail(obj);
		return pessoaRepository.save(obj);
	}
	
	public Pessoa isExistePorEmail(Pessoa obj) {
		Optional<Pessoa> obj1 = pessoaRepository.findByEmail(obj.getEmail());
		if (obj1.isPresent() && obj1.get().getEmail().equals(obj.getEmail())) {
			throw new ObjectFoundException("Usuário Já existe com esse e-mail: " + obj.getEmail());
		}
		return obj;
	}
	
	public Pessoa fromDTO(NovaPessoaDTO objDTO) {
		Pessoa pessoa = new Pessoa(null, objDTO.getNome(), objDTO.getCpf(), objDTO.getEmail(), objDTO.getDataNascimento(),
				null);
		return pessoa;
	}
	
	
	/**
	 * BUSCA UMA PESSOA TENDO COMO PARAMETRO O ID
	 * @param id
	 * @return
	 */
	public Pessoa buscar(Integer id) {
		Optional<Pessoa> pessoa = pessoaRepository.findById(id);
		return pessoa.isPresent()?pessoa.get():null;
	}
	
	/**
	 * BUSCA UMA PESSOA TENDO COMO PARAMETRO O ID
	 * @param id
	 * @return
	 */
	public Pessoa buscar(String email) {
		Optional<Pessoa> pessoa = pessoaRepository.findByEmail(email);
		return pessoa.isPresent()?pessoa.get():null;
	}
	
	public Pessoa salvarPessoa(Pessoa prop) {
		// TODO Auto-generated method stub
		prop.setAtivo(1);
		return pessoaRepository.saveAndFlush(prop);
	}

	public String gerarToken(String nome){
		
		String numeroAleatorio = senhaService.getSenhaAleatoria(4);
		
		String novoToken = "";
		String[] n = nome.split(" ");
		
		for (String string : n) {
			novoToken = novoToken + string.charAt(0);
		}
		
		return novoToken+numeroAleatorio;
	}

	public Pessoa salvarAssociadoNovo(Pessoa pessoa, AssociadoDTO associadosDTO) {
		String senha =  "";
		pessoa  = new Pessoa();
		pessoa.fromAssociadoDTO(associadosDTO);
		
		pessoa.setStatusPessoa(statusPessoaRepository.findById(1));
		String tokenIndicacao = associadosDTO.getTokenIndicacao();
		
		pessoa.setUUID(UUID.randomUUID().toString());
		pessoa.setToken(pessoaService.gerarToken(pessoa.getNome()));
		pessoa.setDataCadastro(LocalDateTime.now());
		pessoa.setAtivo(1);
		pessoa.setSenha(senha);
		pessoa.setLocalImovel(associadosDTO.getEnderecoImovel());
		pessoa.setHorario(associadosDTO.getHorario());
		pessoa = pessoaRepository.save(pessoa);
			
		List<PessoaTipoPessoa> listPessoaTipoPessoa = new ArrayList<>();
			
		Optional<TipoPessoa> tipoPessoa = tipoPessoaRepository.findById(TipoPessoaImovelEnum.ASSOCIADO.getId());
			
		if(tipoPessoa.isPresent()){
			PessoaTipoPessoa pessoaTipoPessoa = new PessoaTipoPessoa();
			pessoaTipoPessoa.setPessoa(pessoa);
			pessoaTipoPessoa.setTipoPessoa(tipoPessoa.get());
			listPessoaTipoPessoa.add(pessoaTipoPessoa);
		}
		
		 pessoaTipoPessoaRepository.save(listPessoaTipoPessoa);
		
		
//		if(tokenIndicacao!=null && !StringUtils.isEmpty(tokenIndicacao) && !tokenIndicacao.trim().equals("")){
//			indicacaoService.salvarIndicacao(pessoa, tokenIndicacao);
//		}
		//enviar email para o indicador avisando que uma nova pessoa foi cadastrada
		//if(pessoa.getPessoaIndicador()!=null && pessoa.getPessoaIndicador().getId()!=0){
			//indicacaoService.salvarEmailAvisoIndicacao(pessoa.getPessoaIndicador(),pessoa.getNome());
		//}
		
		//SALVAR SENHA E ENVIAR EMAIL
		senhaService.salvarSenhasEnviarEmail(pessoa);
		
		return pessoa;
		
	}
	


}
