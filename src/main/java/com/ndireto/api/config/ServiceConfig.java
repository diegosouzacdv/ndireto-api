package com.ndireto.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ndireto.api.services.PessoaService;
import com.ndireto.api.storage.ArquivoStorage;
import com.ndireto.api.storage.FotoStorage;
import com.ndireto.api.storage.local.ArquivoStorageLocal;
import com.ndireto.api.storage.local.FotoStorageLocal;

@Configuration
@ComponentScan(basePackageClasses=PessoaService.class)
public class ServiceConfig {
	
	@Bean
	public FotoStorage fotoStorage() {
		return new FotoStorageLocal();
	}
	
	@Bean
	public ArquivoStorage arquivoStorage() {
		return new ArquivoStorageLocal();
	}
	

	
}