package com.ndireto.api.config;


import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.ndireto.api.mail.Mailer;



@Configuration
@ComponentScan(basePackageClasses = Mailer.class)
@PropertySource({ "classpath:env/mail-${ambiente:local}.properties" })
//@PropertySource(value = { "file://${HOME}/mail-homolog.properties" }, ignoreResourceNotFound = true)
public class MailConfig {
	
	
	@Autowired
	private Environment env;

	@Bean
	public JavaMailSender mailSender() {

		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("smtplw.com.br");
		mailSender.setPort(587);
		mailSender.setUsername(env.getProperty("mail.username"));
		mailSender.setPassword(env.getProperty("password"));
		
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp"); 
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.debug", true);
		props.put("mail.smtp.connectiontimeout", 10000); // miliseconds
		props.put("mail.smtp.ssl.trust", "*");

		mailSender.setJavaMailProperties(props);
		
		return mailSender;
	}
	
	

}
