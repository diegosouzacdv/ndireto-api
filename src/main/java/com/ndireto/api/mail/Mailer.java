package com.ndireto.api.mail;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.Visita;




@Component
public class Mailer {
	

	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private TemplateEngine thymeleaf;
	
	@Async
	public void enviar(Pessoa pessoa,String senha) {
		Context context = new Context(new Locale("pt", "BR"));
		
		context.setVariable("funcao", pessoa.getTipoPessoaFormatado());
		context.setVariable("pf", pessoa);
		context.setVariable("email", pessoa.getEmail());
		context.setVariable("senha", senha);
		
		try {
			String email = thymeleaf.process("mail/MessagemSenha", context);
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			helper.setFrom("contato@ndireto.com.br");
			helper.setTo(pessoa.getEmail());
			helper.setSubject("Negócio Direto - Sua conta foi criada");
			helper.setText(email, true);
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			System.err.println("Erro enviando e-mail "+e.toString());
		}
	}
	
	@Async
	public void enviarAvisoIndicacao(Pessoa pessoaIndicadora,String nomeIndicado) {
		Context context = new Context(new Locale("pt", "BR"));
		
		context.setVariable("pessoaIndicadora", pessoaIndicadora);
		context.setVariable("nomeIndicado", nomeIndicado);
		
		try {
			String email = thymeleaf.process("mail/MessagemIndicacao", context);
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			helper.setFrom("contato@ndireto.com.br");
			helper.setTo(pessoaIndicadora.getEmail());
			helper.setSubject("Nova indicação");
			helper.setText(email, true);
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			System.err.println("Erro enviando e-mail "+e.toString());
		}
	}
	
	@Async
	public void enviarMensagemVisita(Visita visita) {
		Context context = new Context(new Locale("pt", "BR"));
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		
		context.setVariable("visita", visita);
//		if(visita.getPessoaComprador().getId() == 0){
//			try {
//				String email = thymeleaf.process("mail/MessagemVisita", context);
//				
//				MimeMessage mimeMessage = mailSender.createMimeMessage();
//				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
//				helper.setFrom("contato@ndireto.com.br");
//				helper.setTo(visita.getPessoaComprador().getEmail());
//				helper.setSubject("Negócio Direto - Sua conta foi criada para acesso ao Clube NDireto");
//				helper.setText(email, true);
//				mailSender.send(mimeMessage);
//			} catch (MessagingException e) {
//				System.err.println("Erro enviando e-mail "+e.toString());
//			}
//		}
		
		//Enviando E-mail para o Fotografo/Corretor
		if(visita.getPessoaColaborador()!=null) {
			context.setVariable("pessoa", visita.getPessoaColaborador());
			try {
				String email = thymeleaf.process("mail/MessagemVisita", context);
				
				MimeMessage mimeMessage = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				helper.setFrom("contato@ndireto.com.br");
				helper.setTo(visita.getPessoaColaborador().getEmail());
				helper.setSubject("Visita Agendada para o Imóvel situado em  "+visita.getImovel().getEnderecoFormatado()+" no dia "+visita.getDataHoraVisita().format(formatter));
				helper.setText(email, true);
				mailSender.send(mimeMessage);
			} catch (MessagingException e) {
				System.err.println("Erro enviando e-mail "+e.toString());
			}
		}
		
		
		//Enviando E-mail para quem fez o agendamento da visita
		if(visita.getPessoaComprador()!=null) {
			context.setVariable("pessoa", visita.getPessoaComprador());
			try {
				String email = thymeleaf.process("mail/MessagemVisita", context);
				
				MimeMessage mimeMessage = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				helper.setFrom("contato@ndireto.com.br");
				helper.setTo(visita.getPessoaComprador().getEmail());
				helper.setSubject("Negócio Direto - Visita Agendada para o Imovel Código "+visita.getImovel().getId()+" no dia "+visita.getDataHoraVisita());
				helper.setText(email, true);
				mailSender.send(mimeMessage);
			} catch (MessagingException e) {
				System.err.println("Erro enviando e-mail "+e.toString());
			}
		}
		//Enviando E-mail para o proprietário do imovel
		if(visita.getImovel().getProprietario()!=null) {
			context.setVariable("pessoa", visita.getImovel().getProprietario());
			try {
				String email = thymeleaf.process("mail/MessagemVisita", context);
				
				MimeMessage mimeMessage = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				helper.setFrom("contato@ndireto.com.br");
				helper.setTo(visita.getImovel().getProprietario().getEmail());
				helper.setSubject("Negócio Direto - Visita Agendada para o Imovel Código "+visita.getImovel().getId()+" no dia "+visita.getDataHoraVisita());
				helper.setText(email, true);
				mailSender.send(mimeMessage);
			} catch (MessagingException e) {
				System.err.println("Erro enviando e-mail "+e.toString());
			}
		}
		
	}

	@Async
	public void enviarAvisoNovoHistoricoFinanceiroAnuidade(Pessoa pessoa) {
		Context context = new Context(new Locale("pt", "BR"));
		
		context.setVariable("pessoa", pessoa);
		
		try {
			String email = thymeleaf.process("mail/MessagemHistoricoFinanceiroAnuidade", context);
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			helper.setFrom("contato@ndireto.com.br");
			helper.setTo(pessoa.getEmail());
			helper.setSubject("Negócio direto - Clube de Associados ");
			helper.setText(email, true);
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			System.err.println("Erro enviando e-mail "+e.toString());
		}
	}
	
	@Async
	public void enviarEsqueciSenha(Pessoa pessoa, String senha) {
		Context context = new Context(new Locale("pt", "BR"));
		
		context.setVariable("pf", pessoa);
		context.setVariable("senha", senha);
		
		try {
			String email = thymeleaf.process("mail/MessagemEsqueciSenha", context);
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			helper.setFrom("contato@ndireto.com.br");
			helper.setTo(pessoa.getEmail());
			helper.setSubject("Ndireto - Senha de Acesso Sistema");
			helper.setText(email, true);
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			System.err.println("Erro enviando e-mail "+e.toString());
		}
	}
	
	@Async
	public void enviarLembreteCadastro(Pessoa pessoa) {
		Context context = new Context(new Locale("pt", "BR"));
		
		context.setVariable("pessoa", pessoa);
		
		try {
			String email = thymeleaf.process("mail/MessagemLembrete", context);
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			helper.setFrom("contato@ndireto.com.br");
			helper.setTo(pessoa.getEmail());
			helper.setSubject("Lembrete Para Conclusão de Cadastro");
			helper.setText(email, true);
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			System.err.println("Erro enviando e-mail "+e.toString());
		}
	}
}
