package com.ndireto.api.security;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.ndireto.api.entity.Pessoa;



public class UsuarioSistema  extends UsernamePasswordAuthenticationToken{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Pessoa pessoa;
	
	
	
	public UsuarioSistema( String password,
			Collection<? extends GrantedAuthority> authorities,Pessoa pessoa) {
		
		super(pessoa.getNome(), password, authorities);
		// TODO Auto-generated constructor stub
		this.pessoa = pessoa;
		//System.out.println(this.policial.getIdPessoa().getNome());
		
	}

	
	
	
	public Pessoa getPessoa() {
		return pessoa;
	}


	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	


}

