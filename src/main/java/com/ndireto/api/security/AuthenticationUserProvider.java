package com.ndireto.api.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.TipoPessoa;
import com.ndireto.api.repository.PessoaRepository;

@Component
public class AuthenticationUserProvider implements AuthenticationProvider{

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		
		Optional<Pessoa> pessoaOptional = pessoaRepository.findByEmail(authentication.getName());
		Pessoa pessoa = pessoaOptional.orElseThrow(() -> new UsernameNotFoundException("Usuário e/ou senha incorretos"));
		return new UsernamePasswordAuthenticationToken(pessoa.getId(), pessoa.getSenha(), getPermissoes(pessoa));
//		return new UsuarioSistema(pessoa.getSenha(), getPermissoes(pessoa),pessoa);
	}
	
	
	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
	

//	private Collection<? extends GrantedAuthority> getPermissoes(Pessoa pessoa) {
//		Set<SimpleGrantedAuthority> authorities = new HashSet<SimpleGrantedAuthority>();
//		pessoa.getPermissoes().forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getDescricao().toUpperCase())));
//		return authorities;
//	}

	private Collection<? extends GrantedAuthority> getPermissoes(Pessoa pessoa) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		for (TipoPessoa tipoPessoa : pessoa.getTipoPessoa()) {
			authorities.add(new SimpleGrantedAuthority(tipoPessoa.getNome().replaceAll(" ","_").toUpperCase()));
		}
		return authorities;
	}


	
	
}
