package com.ndireto.api.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.TipoPessoa;
import com.ndireto.api.repository.PessoaRepository;

@Service
public class AppUserDetailsService implements UserDetailsService{

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Override
	public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
		
		
		Optional<Pessoa> pessoaOptional = pessoaRepository.findById(Integer.valueOf(id));
		Pessoa pessoa = pessoaOptional.orElseThrow(() -> new UsernameNotFoundException("Usuário e/ou senha Invalidos"));
		return new User(pessoa.getId()+"", pessoa.getSenha(), getPermissoes(pessoa));
	}
	

	private Collection<? extends GrantedAuthority> getPermissoes(Pessoa pessoa) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		for (TipoPessoa tipoPessoa : pessoa.getTipoPessoa()) {
			authorities.add(new SimpleGrantedAuthority(tipoPessoa.getNome().replaceAll(" ","_").toUpperCase()));
		}
		return authorities;
	}

}
