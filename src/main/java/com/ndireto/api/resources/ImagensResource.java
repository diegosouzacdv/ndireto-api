package com.ndireto.api.resources;

import java.io.File;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import com.ndireto.api.dto.ArquivoDTO;
import com.ndireto.api.entity.FotoImovel;
import com.ndireto.api.entity.Imovel;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.repository.FotoImovelRepository;
import com.ndireto.api.repository.ImovelRepository;
import com.ndireto.api.repository.PessoaRepository;
import com.ndireto.api.services.AnexoService;
import com.ndireto.api.storage.ArquivoStorage;
import com.ndireto.api.storage.ArquivoStorageRunnable;


@RestController
@RequestMapping("/imagens")
public class ImagensResource {
	
	@Autowired
	private ArquivoStorage arquivoStorage;
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Autowired
	private ImovelRepository imovelRepository;
	
		
	@Autowired
	private AnexoService anexoService;
	
	
	@Autowired
	private FotoImovelRepository fotoImovelRepository;

	@GetMapping("/foto/{id}/imovel")
	public ResponseEntity<byte[]> getFotoMensagem(@PathVariable Integer id) {
		String nome = "";
		FotoImovel foto = fotoImovelRepository.findById(id);
		byte[] doc = null;

		String diretorioRaiz = "";

		if(foto!=null){
			diretorioRaiz = "anexosNdireto";
			if(anexoService.existe(diretorioRaiz+File.separator+foto.getNomeFoto())){
				nome = foto.getTipoReduzido();
				doc = arquivoStorage.recuperar(foto.getNomeFoto(),diretorioRaiz);
			}
			else{
				nome = "png";
				doc = arquivoStorage.recuperar("semfoto.png",diretorioRaiz);
			}


		}

		ResponseEntity<byte[]> retorno = anexoService.converteTipoRetorno(nome, doc);
		return retorno!=null?retorno:ResponseEntity.notFound().build();
	}
	
	
	
	@PostMapping("/pessoa/foto/{id}")
	public ResponseEntity<?>  uploadFotoPessoa(@PathVariable(name="id") Integer id,@RequestParam("file") MultipartFile[] files
			){
		
		int cont = 1;
		for (MultipartFile multipartFile : files) {
			cont++;
		}
		ArquivoDTO arquivo = gravarFotosNaPastaBanco(id, files);
		return ResponseEntity.ok(arquivo);
	}
	
	
	
	@PostMapping("/imovel/foto/{id}")
	public ResponseEntity<?>  uploadFotoImovel(
			@RequestParam("file") MultipartFile[] files,
			@PathVariable(name="id") Integer id,
			@AuthenticationPrincipal Object usuario ){
		
		
		ArquivoDTO arquivo = gravarFotosImovelNaPastaBanco(id, files);
		return ResponseEntity.ok(arquivo);
	}
	
	
	
	private ArquivoDTO gravarFotosNaPastaBanco(Integer id, MultipartFile[] files) {
		Optional<Pessoa> pessoaOpt = pessoaRepository.findById(id);
		
		
		Pessoa pessoa = new Pessoa();
		
		if(pessoaOpt.isPresent()) {
			pessoa = pessoaOpt.get();
		}
		
		ArquivoDTO arquivo = uploadArquivo(files);
		try {
			//while (arquivo.getNome()==null || !StringUtils.isEmpty(arquivo.getNome())) {
				Thread.sleep(1000);
			//}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(pessoaOpt.isPresent()) {
			pessoa.setContentType(arquivo.getContentType());
			pessoa.setNomeFoto(arquivo.getNome());
			pessoaRepository.save(pessoa);
		}
		return arquivo;
	}
	
	private ArquivoDTO gravarFotosImovelNaPastaBanco(Integer id, MultipartFile[] files) {
		Optional<Imovel> imovelOpt = imovelRepository.findById(id);
		FotoImovel fotoImovel = new FotoImovel();
		
		
		if(imovelOpt.isPresent()) {
			fotoImovel.setImovel(imovelOpt.get());
		}
		
		ArquivoDTO arquivo = uploadArquivo(files);
		try {
				Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		fotoImovel.setContentType(arquivo.getContentType());
		fotoImovel.setNomeFoto(arquivo.getNome());
		fotoImovel.setAtivo(1);
		fotoImovelRepository.save(fotoImovel);
		return arquivo;
	}
	
	
	public ArquivoDTO uploadArquivo(MultipartFile[] files) {
		DeferredResult<ArquivoDTO> resultado = new DeferredResult<>();
		Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, arquivoStorage, "anexosNdireto"));
		thread.start();

		ArquivoDTO arquivotemp = (ArquivoDTO) resultado.getResult();
		while (arquivotemp == null) {
			try {
				Thread.sleep(300);
			} catch (InterruptedException ex) {
			}

			arquivotemp = (ArquivoDTO) resultado.getResult();
		}

		ArquivoDTO arquivo = arquivotemp;
		return arquivo;
	}
	
	

}
