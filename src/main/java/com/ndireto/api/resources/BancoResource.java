package com.ndireto.api.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.entity.Banco;
import com.ndireto.api.services.BancosService;


@RestController
@RequestMapping("/bancos")
public class BancoResource {

	@Autowired
	private BancosService bancosService;
	
	@GetMapping("")
	public ResponseEntity<List<Banco>> buscarsMensagens(@AuthenticationPrincipal Object usuario) {
		List<Banco> listBancos= bancosService.buscarTodosAtivos();
		return ResponseEntity.ok(listBancos);
				
	}

	


}
