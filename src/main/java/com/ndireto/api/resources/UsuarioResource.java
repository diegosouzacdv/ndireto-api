package com.ndireto.api.resources;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.dto.HistoricoFinanceiroDTO;
import com.ndireto.api.dto.MensagemDTO;
import com.ndireto.api.dto.PessoaDTO;
import com.ndireto.api.entity.HistoricoFinanceiro;
import com.ndireto.api.entity.Mensagem;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.repository.MensagemRepository;
import com.ndireto.api.repository.PessoaRepository;
import com.ndireto.api.services.AnexoService;
import com.ndireto.api.storage.ArquivoStorage;


@RestController
@RequestMapping("/usuario")
public class UsuarioResource {


	@Autowired
	private PessoaRepository pessoaRepository;


	@Autowired
	private ArquivoStorage arquivoStorage;
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	@Autowired
	private AnexoService anexoService;
	

	@GetMapping("")
	//@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PESSOA') and #oauth2.hasScope('read')")
	public PessoaDTO buscarPeloId(@AuthenticationPrincipal Object usuario) {
		Optional<Pessoa>  optPessoa =  pessoaRepository.findById(Integer.valueOf(usuario.toString()));
		Pessoa pessoa = new Pessoa();
		if(optPessoa.isPresent()) {
			pessoa = optPessoa.get();
		}
		

		return pessoa.fromDTO();
	}
	
	
	@GetMapping("/mensagens")
	public ResponseEntity<List<MensagemDTO>> buscarsMensagens(@AuthenticationPrincipal Object usuario) {
		List<Mensagem> listMensagem = mensagemRepository.findByDestinatarioIdAndAtivoOrderByDataInclusaoDesc(Integer.valueOf(usuario.toString()), 1);
		
		List<MensagemDTO> mensagensRetorno = new ArrayList<MensagemDTO>();
		
		for (Mensagem obj : listMensagem) {
			MensagemDTO temp = new MensagemDTO();
			temp = temp.fromDto(obj);
			mensagensRetorno.add(temp);
			
		}
		
		return ResponseEntity.ok(mensagensRetorno);
				
	}
	
	@GetMapping("/indicacoes/quantidade")
	public ResponseEntity<Integer> buscarsIndicacoesQuantidade(@AuthenticationPrincipal Object usuario) {
		List<Pessoa> indicados = pessoaRepository.findByPessoaIndicadorId(Integer.valueOf(usuario.toString()));
		
		List<PessoaDTO> pessoaRetorno = new ArrayList<PessoaDTO>();
		

		return ResponseEntity.ok(indicados.size());
				
	}
	
	@GetMapping("/indicacoes")
	public ResponseEntity<List<PessoaDTO>> buscarsIndicacoes(@AuthenticationPrincipal Object usuario) {
		List<Pessoa> indicados = pessoaRepository.findByPessoaIndicadorId(Integer.valueOf(usuario.toString()));
		
		List<PessoaDTO> pessoaRetorno = new ArrayList<PessoaDTO>();
		
		for (Pessoa obj : indicados) {
			PessoaDTO temp = new PessoaDTO();
			temp = obj.fromDTO();
			pessoaRetorno.add(temp);
			
		}
		
		return ResponseEntity.ok(pessoaRetorno);
				
	}
	
	@GetMapping("/mensagens/novas")
	public ResponseEntity<List<MensagemDTO>> buscarsMensagensNovas(@AuthenticationPrincipal Object usuario) {
		List<Mensagem> listMensagem = mensagemRepository.findByDestinatarioIdAndAtivoAndDataRecebimentoIsNullOrderByDataInclusaoDesc(Integer.valueOf(usuario.toString()), 1);
		
		List<MensagemDTO> mensagensRetorno = new ArrayList<MensagemDTO>();
		
		for (Mensagem obj : listMensagem) {
			MensagemDTO temp = new MensagemDTO();
			temp = temp.fromDto(obj);
			mensagensRetorno.add(temp);
			
		}
		
		return ResponseEntity.ok(mensagensRetorno);
				
	}
	
	
	@GetMapping("/financeiro")
	public ResponseEntity<List<HistoricoFinanceiroDTO>> buscarFinacas(@AuthenticationPrincipal Object usuario) {
		Optional<Pessoa>  optPessoa =  pessoaRepository.findById(Integer.valueOf(usuario.toString()));
		List<HistoricoFinanceiro> financas = optPessoa.get().getHistoricoFinanceiro();
		
		List<HistoricoFinanceiroDTO> financasRetorno = new ArrayList<HistoricoFinanceiroDTO>();
		
		for (HistoricoFinanceiro obj : financas) {
			HistoricoFinanceiroDTO temp = new HistoricoFinanceiroDTO();
			temp = temp.fromDto(obj);
			financasRetorno.add(temp);
			
		}
		
		return financasRetorno!=null && !financasRetorno.isEmpty()?
				ResponseEntity.ok(financasRetorno):ResponseEntity.notFound().build();
				
	}
	
	
	@GetMapping("/financeiro/novos")
	public ResponseEntity<List<HistoricoFinanceiroDTO>> buscarFinacasNovas(@AuthenticationPrincipal Object usuario) {
		Optional<Pessoa>  optPessoa =  pessoaRepository.findById(Integer.valueOf(usuario.toString()));
		List<HistoricoFinanceiro> financas = optPessoa.get().getHistoricoFinanceiro();
		
		List<HistoricoFinanceiroDTO> financasRetorno = new ArrayList<HistoricoFinanceiroDTO>();
		
		for (HistoricoFinanceiro obj : financas) {
			HistoricoFinanceiroDTO temp = new HistoricoFinanceiroDTO();
			if(obj.getDataVisualizacao()==null) {
				temp = temp.fromDto(obj);
				financasRetorno.add(temp);
			}
			
		}
		
		return financasRetorno!=null && !financasRetorno.isEmpty()?
				ResponseEntity.ok(financasRetorno):ResponseEntity.notFound().build();
				
	}
	
	
	
	@SuppressWarnings("unused")
	@GetMapping("/financeiro/quantidade")
	public ResponseEntity<Integer> buscarFinacasQuantidade(@AuthenticationPrincipal Object usuario) {
		
		Integer retorno = 0;
		
		Optional<Pessoa>  optPessoa =  pessoaRepository.findById(Integer.valueOf(usuario.toString()));
		List<HistoricoFinanceiro> financas = optPessoa.get().getHistoricoFinanceiro();
		
		for (HistoricoFinanceiro obj : financas) {
			if(obj.getDataVisualizacao()==null) {
				retorno++;
			}
		}
		
		return 	ResponseEntity.ok(retorno);		
	}
	
	
	@GetMapping("/foto")
	public ResponseEntity<byte[]> getFotoUsuario(@AuthenticationPrincipal Object usuario) {
		String nome = "";
		Optional<Pessoa> optPessoa = pessoaRepository.findById(Integer.valueOf(usuario.toString()));
		byte[] doc = null;

		String diretorioRaiz = "";

		if(optPessoa.isPresent()){
			diretorioRaiz = "anexosNdireto";
			if(optPessoa.get().getNomeFoto()!=null && 
					!optPessoa.get().getNomeFoto().trim().equals("") && 
					anexoService.existe(diretorioRaiz+File.separator+optPessoa.get().getNomeFoto())){
				nome = optPessoa.get().getTipoReduzido();
				doc = arquivoStorage.recuperar(optPessoa.get().getNomeFoto(),diretorioRaiz);
			}
			else{
				nome = "png";
				doc = arquivoStorage.recuperar("semfoto.png",diretorioRaiz);
			}


		}
		
		ResponseEntity<byte[]> retorno = anexoService.converteTipoRetorno(nome, doc);

		
		return retorno!=null?retorno:ResponseEntity.notFound().build();
		
	}
	
	
	@GetMapping("/foto/thumb")
	public ResponseEntity<byte[]> getFotoUsuarioMenor(@AuthenticationPrincipal Object usuario) {
		String nome = "";
		Optional<Pessoa> optPessoa = pessoaRepository.findById(Integer.valueOf(usuario.toString()));
		byte[] doc = null;

		String diretorioRaiz = "";

		if(optPessoa.isPresent()){
			diretorioRaiz = "anexosNdireto";
			if(anexoService.existe(diretorioRaiz+File.separator+optPessoa.get().getNomeFoto())){
				nome = optPessoa.get().getTipoReduzido();
				doc = arquivoStorage.recuperar(optPessoa.get().getNomeFoto(),diretorioRaiz);
			}
			else{
				nome = "png";
				doc = arquivoStorage.recuperar("semfoto.png",diretorioRaiz);
			}


		}

	ResponseEntity<byte[]> retorno = anexoService.converteTipoRetorno(nome, doc);

	
	return retorno!=null?retorno:ResponseEntity.notFound().build();
}


}
