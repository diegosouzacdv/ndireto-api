package com.ndireto.api.resources;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.entity.TokenFirebase;
import com.ndireto.api.repository.TokenFirebaseRepository;
import com.ndireto.api.services.TokenFirebaseService;


@RestController
@RequestMapping("/token")
public class TokenFirebaseResource {

	@Autowired
	private TokenFirebaseRepository tokenFirebaseRepository;
	
	@Autowired
	private TokenFirebaseService tokenFirebaseService;
	
	@GetMapping()
	public ResponseEntity<List<TokenFirebase>> buscarTokens(@AuthenticationPrincipal Object usuario) {
		List<TokenFirebase> listToken= tokenFirebaseRepository.findByPessoaId(Integer.valueOf(usuario.toString()));
		return ResponseEntity.ok(listToken);
				
	}
	
	@PostMapping()
	public ResponseEntity<TokenFirebase> SalvarToken(@RequestBody String token,  @AuthenticationPrincipal Object usuario) {
		TokenFirebase tokenSalvo = tokenFirebaseService.salvarToken(token, usuario);
		return ResponseEntity.ok(tokenSalvo);
				
	}

	


}
