package com.ndireto.api.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.entity.Itens;
import com.ndireto.api.repository.ItensRepository;


@RestController
@RequestMapping("/itens")
public class ItensResource {



	
	@Autowired
	private ItensRepository itensRepository;
	
	
	
	@GetMapping("")
	public ResponseEntity<List<Itens>> buscarsMensagens(@AuthenticationPrincipal Object usuario) {
		List<Itens> listItens= itensRepository.findByAtivoOrderByNomeAsc(1);
		
		return ResponseEntity.ok(listItens);
				
	}

	


}
