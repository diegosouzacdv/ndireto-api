package com.ndireto.api.resources;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.dto.MensagemDTO;
import com.ndireto.api.entity.Mensagem;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.repository.MensagemRepository;
import com.ndireto.api.repository.PessoaRepository;
import com.ndireto.api.services.AnexoService;
import com.ndireto.api.services.MensagemService;
import com.ndireto.api.storage.ArquivoStorage;


@RestController
@RequestMapping("/mensagem")
public class MensagensResource {



	@Autowired
	private ArquivoStorage arquivoStorage;
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	@Autowired
	private AnexoService anexoService;
	
	@Autowired
	private MensagemService mensagemService;

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@GetMapping("/{id}/imagem")
	public ResponseEntity<byte[]> getFotoMensagem(@PathVariable Integer id) {
		String nome = "";
		Optional<Mensagem> optMensagem = mensagemRepository.findById(id);
		byte[] doc = null;

		String diretorioRaiz = "";

		if(optMensagem.isPresent()){
			diretorioRaiz = "anexosNdireto";
			if(anexoService.existe(diretorioRaiz+File.separator+optMensagem.get().getImagem())){
				nome = optMensagem.get().getTipoReduzido();
				doc = arquivoStorage.recuperar(optMensagem.get().getImagem(),diretorioRaiz);
			}
			else{
				nome = "png";
				doc = arquivoStorage.recuperar("semfoto.png",diretorioRaiz);
			}
		}

		ResponseEntity<byte[]> retorno = anexoService.converteTipoRetorno(nome, doc);
		return retorno!=null?retorno:ResponseEntity.notFound().build();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<MensagemDTO> getMensagemId(@PathVariable Integer id) {
		Optional<Mensagem> optMensagem = mensagemRepository.findById(id);
		MensagemDTO retorno = new MensagemDTO();
		retorno = retorno.fromDto(optMensagem.get());
		return ResponseEntity.ok(retorno);
	}
	
	
	@PutMapping("/ler/{id}")
	public ResponseEntity<MensagemDTO> marcarMensagemComoLida(@PathVariable Integer id) {
		Optional<Mensagem> optMensagem = mensagemRepository.findById(id);
		MensagemDTO retorno = new MensagemDTO();
		optMensagem.get().setDataRecebimento(LocalDateTime.now());
		mensagemService.marcarLida(optMensagem.get());
		retorno = retorno.fromDto(optMensagem.get());
		return ResponseEntity.ok(retorno);
	}
	
	@PutMapping("/naoler/{id}")
	public ResponseEntity<MensagemDTO> marcarMensagemNaoLida(@PathVariable Integer id) {
		Optional<Mensagem> optMensagem = mensagemRepository.findById(id);
		MensagemDTO retorno = new MensagemDTO();
		//mensagemService.marcarNaoLida(optMensagem.get());
		retorno = retorno.fromDto(optMensagem.get());
		return ResponseEntity.ok(retorno);
	}	

	@GetMapping("/whatsapp")
	public ResponseEntity<List<MensagemDTO>> buscarsMensagensZap() {
		
		List<Mensagem> listMensagem = mensagemRepository.findByAtivoAndDataEnvioWatsAppIsNullOrderByDataInclusaoDesc(1);
		
		List<MensagemDTO> mensagensRetorno = new ArrayList<MensagemDTO>();
		
		for (Mensagem obj : listMensagem) {
			MensagemDTO temp = new MensagemDTO();
			temp = temp.fromDto(obj);
			mensagensRetorno.add(temp);
			
		}
		
		return ResponseEntity.ok(mensagensRetorno);
				
	}
	
	
	@PutMapping("/whatsapp/despachar/{id}")
	public ResponseEntity<MensagemDTO> despacharMensagensZap(@PathVariable Integer id,
			@AuthenticationPrincipal Object usuario) {
		
		Optional<Mensagem> optMensagem = mensagemRepository.findById(id);
		Pessoa pessoa = pessoaRepository.findOne(Integer.valueOf(usuario.toString()));
		MensagemDTO retorno = new MensagemDTO();
		
		optMensagem.get().setDataEnvioWatsApp(LocalDateTime.now());
		optMensagem.get().setUsuarioEnvioWatsApp(pessoa);
		retorno = retorno.fromDto(optMensagem.get());
		mensagemRepository.save(optMensagem.get());
		
		return ResponseEntity.ok(retorno);
	}
	


}
