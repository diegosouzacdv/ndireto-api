package com.ndireto.api.resources;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.dto.AssociadoDTO;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.PessoaTipoPessoa;
import com.ndireto.api.entity.TokenFirebase;
import com.ndireto.api.model.PushNotificationRequest;
import com.ndireto.api.model.PushNotificationResponse;
import com.ndireto.api.repository.PessoaTipoPessoaRepository;
import com.ndireto.api.services.MensagemService;
import com.ndireto.api.services.PessoaService;
import com.ndireto.api.services.PushNotificationService;
import com.ndireto.api.services.SmsService;


@RestController
@RequestMapping()
public class RegistroResource {


	@Autowired
	private PessoaService pessoaService;
	
	@Autowired
	private PushNotificationService pushNotificationService;
	
	@Autowired
	private SmsService smsService;
	
	@Autowired
	private MensagemService mensagemService;
	
	
	@Autowired
	private PessoaTipoPessoaRepository pessoaTipoPessoaRepository;
	
	@PostMapping("/registro")
	public Integer buscarPeloId(@RequestBody AssociadoDTO associadosDTO) {
		
		Pessoa pessoa  =  new Pessoa();
		pessoa = pessoaService.buscar(associadosDTO.getEmail());
		
		if(pessoa!=null) {
			enviarNotificacaoCadastroDuplicadoCadastro(associadosDTO);
			smsService.enviarMensagemJaCadastrado(pessoa);
			mensagemService.enviarMensagemJaCadastrado(pessoa);
			return 2;
		}
		
		try {
			Pessoa pes = pessoaService.salvarAssociadoNovo(pessoa,associadosDTO);
			enviarNotificacaoCadastro(associadosDTO);
			smsService.enviarMensagemBoasVindas(pes);
			mensagemService.enviarMensagemBoasVindas(pes);
		} catch (Exception e) {
			return 0;
		}
		return 1;
	}
	
	
	private void enviarNotificacaoCadastro(AssociadoDTO associado) {
		PushNotificationRequest msg = new PushNotificationRequest();
		msg.setMessage("Nome:"+associado.getNome()+"  Contato :"+associado.getTelefone() +"  Bairro : "+associado.getEnderecoImovel() +" Horário: "+associado.getHorario());
		msg.setTitle("Cadastro");
		msg.setTopic("Cadastro");
		
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		ids.add(2);
		
		List<PessoaTipoPessoa> listPessoaTipoPessoa =    pessoaTipoPessoaRepository.findByTipoPessoaIdIn(ids);
		
		for (PessoaTipoPessoa pessoaTipoPessoa : listPessoaTipoPessoa) {
			for (TokenFirebase token : pessoaTipoPessoa.getPessoa().getListTokenFirebase()) {
				msg.setToken(token.getToken());
				sendTokenNotification(msg);
				
			}
		}
		
		sendTokenNotification(msg);
	}
	
	private void enviarNotificacaoCadastroDuplicadoCadastro(AssociadoDTO associado) {
		PushNotificationRequest msg = new PushNotificationRequest();
		msg.setMessage("Nome:"+associado.getNome()+"  Contato :"+associado.getTelefone() +"  Bairro : "+associado.getEnderecoImovel() +" Horário: "+associado.getHorario());
		msg.setTitle("Contato");
		msg.setTopic("Contato");
		
		
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		ids.add(2);
		
		List<PessoaTipoPessoa> listPessoaTipoPessoa =    pessoaTipoPessoaRepository.findByTipoPessoaIdIn(ids);
		
		for (PessoaTipoPessoa pessoaTipoPessoa : listPessoaTipoPessoa) {
			for (TokenFirebase token : pessoaTipoPessoa.getPessoa().getListTokenFirebase()) {
				msg.setToken(token.getToken());
				sendTokenNotification(msg);
			}
		}
		
		
		
	}
	
	
	
	 public ResponseEntity sendTokenNotification(PushNotificationRequest request) {
	        pushNotificationService.sendPushNotificationToToken(request);
	        return new ResponseEntity<>(new PushNotificationResponse(HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
	    }
	

}
