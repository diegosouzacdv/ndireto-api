package com.ndireto.api.resources;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.dto.AgendaDisponibilidadeDTO;
import com.ndireto.api.dto.AgendarVisitaDTO;
import com.ndireto.api.dto.DiasDTO;
import com.ndireto.api.dto.EstruturaImovelDTO;
import com.ndireto.api.dto.HorasDTO;
import com.ndireto.api.dto.ImovelDTO;
import com.ndireto.api.dto.ItensImovelDTO;
import com.ndireto.api.dto.LocalizacaoDTO;
import com.ndireto.api.dto.TerrenoImovelDTO;
import com.ndireto.api.dto.ValoresImovelDTO;
import com.ndireto.api.entity.DiasSemana;
import com.ndireto.api.entity.DisponibilidadeVisitaImovel;
import com.ndireto.api.entity.Horario;
import com.ndireto.api.entity.Imovel;
import com.ndireto.api.entity.ImovelItens;
import com.ndireto.api.entity.Itens;
import com.ndireto.api.entity.StatusImovel;
import com.ndireto.api.repository.DiasSemanaRepository;
import com.ndireto.api.repository.DisponibilidadeVisitaImovelRepository;
import com.ndireto.api.repository.HorarioRepository;
import com.ndireto.api.repository.StatusImovelRepository;
import com.ndireto.api.services.ImovelService;
import com.ndireto.api.services.VisitaService;


@RestController
@RequestMapping("/imovel")
public class ImovelResource {
	
	
	@Autowired	
	private ImovelService  imovelService;
	

	
	@Autowired
	private DiasSemanaRepository diasSemanaRepository;

	@Autowired
	private DisponibilidadeVisitaImovelRepository disponibilidadeVisitaImovelRepository;
	
	@Autowired
	private HorarioRepository horarioRepository;
	
	@Autowired
	private StatusImovelRepository statusImovelRepository;
	
	@Autowired
	private VisitaService visitaService;
	
	
	
	
	@GetMapping("/todos")
	public ResponseEntity<List<ImovelDTO>> todosImoveis() {
		List<Imovel> imovel =  imovelService.buscarTodos();
		List<ImovelDTO> dto = new ArrayList<ImovelDTO>();
		for (Imovel imo : imovel) {
			dto.add(imo.fromDTO());
		}
	 return ResponseEntity.ok(dto);
	}
	
	
	
	@GetMapping("/quantidade/todos")
	public ResponseEntity<Integer> quantidadeTodosImoveis() {
	 return ResponseEntity.ok(imovelService.buscarQuantidadeTodosImoveis());
	}
	
	
	
	@GetMapping("/visitacorretor")
	public ResponseEntity<List<ImovelDTO>> disponiveisParaVisitaImoveis() {
		List<Imovel> imovel =  imovelService.buscarDisponiveisParaVisitaCorretor();
		List<ImovelDTO> dto = new ArrayList<ImovelDTO>();
		for (Imovel imo : imovel) {
			dto.add(imo.fromDTO());
		}
	 return ResponseEntity.ok(dto);
	}
	
	
	
	@GetMapping("/quantidade/visitacorretor")
	public ResponseEntity<Integer> quantidadeDisponiveisParaVisitaImoveis() {
		return ResponseEntity.ok(imovelService.buscarQuantidadeDisponiveisParaVisitaCorretor());
	}
	
	
	
	@GetMapping("/meus")
	public ResponseEntity<List<ImovelDTO>> meusImoveis() {
		List<Imovel> imovel =  imovelService.buscarMeus();
		List<ImovelDTO> dto = new ArrayList<ImovelDTO>();
		for (Imovel imo : imovel) {
			dto.add(imo.fromDTO());
		}
	 return ResponseEntity.ok(dto);
	}
	
	
	
	@GetMapping("/quantidade/meus")
	public ResponseEntity<Integer> quantidadeMeusImoveis() {
	 return ResponseEntity.ok(imovelService.buscarQuantidadeMeusImoveis());
	}
	
	
	
	@GetMapping("/anunciados")
	public ResponseEntity<List<ImovelDTO>> anunciadosImoveis() {
		List<Imovel> imovel =  imovelService.buscarAnunciados();
		List<ImovelDTO> dto = new ArrayList<ImovelDTO>();
		for (Imovel imo : imovel) {
			dto.add(imo.fromDTO());
		}
	 return ResponseEntity.ok(dto);
	}
	
	
	
	@GetMapping("/quantidade/anunciados")
	public ResponseEntity<Integer> quantidadeanunciadosImoveis() {
	 return ResponseEntity.ok(imovelService.buscarQuantidadeAnunciadosImoveis());
	}
	
	
	@GetMapping("/fotografados")
	public ResponseEntity<List<ImovelDTO>> fotografadosImoveis() {
		List<Imovel> imovel =  imovelService.buscarFotografados();
		List<ImovelDTO> dto = new ArrayList<ImovelDTO>();
		for (Imovel imo : imovel) {
			dto.add(imo.fromDTO());
		}
	 return ResponseEntity.ok(dto);
	}
	
	
	
	@GetMapping("/quantidade/fotografados")
	public ResponseEntity<Integer> quantidadefotografadosImoveis() {
	 return ResponseEntity.ok(imovelService.buscarQuantidadeFotografadosImoveis());
	}
	
	
	@GetMapping("/visitados")
	public ResponseEntity<List<ImovelDTO>> visitadosImoveis() {
		List<Imovel> imovel =  imovelService.buscarVisitados();
		List<ImovelDTO> dto = new ArrayList<ImovelDTO>();
		for (Imovel imo : imovel) {
			dto.add(imo.fromDTO());
		}
	 return ResponseEntity.ok(dto);
	}
	
	
	
	
	
	@GetMapping("/itens/{id}")
	public ResponseEntity<List<Itens>> buscarsMensagens(@PathVariable int id) {
		Imovel imovel =  imovelService.buscar(id);
		List<Itens> listItens = new ArrayList<Itens>();
		
		for (ImovelItens obj : imovel.getImovelItens()) {
			listItens.add(obj.getItens());
		}
		
		return ResponseEntity.ok(listItens); 
				
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ImovelDTO> buscarsimovel(@PathVariable int id) {
		Imovel imovel =  imovelService.buscar(id);
		
		return ResponseEntity.ok(imovel.fromDTO()); 
				
	}
	
	
	
	@PutMapping("/concluir/cadastro/{id}")
	public ResponseEntity<ImovelDTO> salvarCadastroCompleto(@PathVariable int id) {
		Imovel imovel =  imovelService.buscar(id);
		StatusImovel status = 	statusImovelRepository.findById(12);
		imovel.setStatusImovel(status);
		imovelService.salvar(imovel);
		
	 return ResponseEntity.ok(imovel.fromDTO());
	}
	
	
	@PostMapping("/agendar/visita/fotografo")
	public ResponseEntity<AgendarVisitaDTO> agendarVisita(@RequestBody AgendarVisitaDTO  visitaDTO,
			@AuthenticationPrincipal Object usuario) {
		
		
		visitaDTO.setCadastranteCodigo(Integer.valueOf(usuario.toString()));
		visitaDTO.setColaboradorCodigo(Integer.valueOf(usuario.toString()));
		
		visitaService.salvarNovaVisitaFotografo(visitaDTO);
		
		visitaDTO.setStatusVisita("Agendada");
		return ResponseEntity.ok(visitaDTO); 
				
	}
	
	
	
	
	@GetMapping("/agenda/disponibilidade/{id}")
	public ResponseEntity<AgendaDisponibilidadeDTO> buscarsAgendaDisponibilidade(@PathVariable int id) {
		Imovel imovel =  imovelService.buscar(id);
		AgendaDisponibilidadeDTO agenda = new AgendaDisponibilidadeDTO();
		List<DiasDTO> diasHorasDTO = new ArrayList<DiasDTO>();
		
		agenda.setCodigoImovel(id);
		List<DiasSemana> dias =  diasSemanaRepository.findAll();
		List<Horario> horarios = horarioRepository.findAll();
		
		List<Horario> horariosManha = new ArrayList<Horario>();
		List<Horario> horariosTarde = new ArrayList<Horario>();
		List<Horario> horariosNoite = new ArrayList<Horario>();
		for (Horario horario : horarios) {
			
			if(horario.isManha()) {//manhã
				horariosManha.add(horario);
			}else if(horario.isTarde()) {//tarde
				horariosTarde.add(horario);
			}else if(horario.isNoite()) {//noite
				horariosNoite.add(horario);
			}
			
		}
		
		
		List<DiasSemana> diasOrganizados = new ArrayList<DiasSemana>();
		
		for (int i = 1; i < 8; i++) {
			LocalDate  diaTempo = LocalDate.now().plusDays(i);
			for (DiasSemana obj : dias) {
				if(obj.getOrdem()==diaTempo.getDayOfWeek().getValue()) {
					diasOrganizados.add(obj);
				}
				
			}
			
			
		}
		
		
		
		int i = 0;
		for (DiasSemana obj : diasOrganizados) {
			
			boolean cheioManha =  isIncluirExcluirPeriodo(horariosManha, imovel,obj);
			boolean cheioTarde =  isIncluirExcluirPeriodo(horariosTarde, imovel,obj);
			boolean cheioNoite =  isIncluirExcluirPeriodo(horariosNoite, imovel,obj);
			DiasDTO dia = new DiasDTO();
			
			dia.setCodigo(obj.getCodigo());
			dia.setNome(obj.getNome());
			dia.setSigla(obj.getSigla());
			dia.setCheioManha(cheioManha);
			dia.setCheioTarde(cheioTarde);
			dia.setCheioNoite(cheioNoite);
			
			DateTimeFormatter formatter  = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate  diaTempo = LocalDate.now().plusDays(i);
			i++;
			dia.setData(diaTempo.format(formatter));
			
			diasHorasDTO.add(dia);
			
		}
		
		
		
		List<DisponibilidadeVisitaImovel> disponibilidade = disponibilidadeVisitaImovelRepository.findByImovelId(id);
		
		
		for (DiasDTO dia : diasHorasDTO) {
			
			List<HorasDTO> horas = new ArrayList<HorasDTO>();
			dia.setHora(horas);
			for (DisponibilidadeVisitaImovel obj : disponibilidade) {
				if(obj.getDiasSemana().getCodigo()==dia.getCodigo()) {
					HorasDTO hora = new HorasDTO(obj.getHorario().getCodigo(),obj.getHorario().getHora());
					dia.getHora().add(hora);
				}
				
			}
		}
		
		agenda.setDiasSemana(diasHorasDTO);
		
		return imovel!=null && imovel.getId()!=0 ?ResponseEntity.ok(agenda):ResponseEntity.notFound().build();
		
	}
	

	/**
	 * Caso não tenha o item que vem como parametro este item será adcionado
	 * caso tenha será excluido
	 * @param itensImovelDTO
	 * @param usuario
	 * @return
	 */
	@PutMapping("/adiciona/item")
	public ResponseEntity<ItensImovelDTO> adicionaItem(
			@RequestBody ItensImovelDTO  itensImovelDTO,
			@AuthenticationPrincipal Object usuario) {
	
		
		if(!imovelService.contemItem(itensImovelDTO)) {
			imovelService.adicionaItem(itensImovelDTO);
			itensImovelDTO.setStatus("ADICIONADO");
		}	
		
		return ResponseEntity.ok(itensImovelDTO);
	}
	
	@PutMapping("/remove/item")
	public ResponseEntity<ItensImovelDTO> RemoveItem(
			@RequestBody ItensImovelDTO  itensImovelDTO,
			@AuthenticationPrincipal Object usuario) {
		
		
		if(imovelService.contemItem(itensImovelDTO)) {
			imovelService.removeItem(itensImovelDTO);
			itensImovelDTO.setStatus("REMOVIDO");
		}
		
		return ResponseEntity.ok(itensImovelDTO);
	}
	
	@PutMapping("/localizacao")
	public ResponseEntity<LocalizacaoDTO> salvarLocalizaco(@RequestBody LocalizacaoDTO  localizacaoDTO) {
		LocalizacaoDTO dto = new LocalizacaoDTO();
		dto = imovelService.localizacaoImovel(localizacaoDTO);
		return ResponseEntity.ok(dto);
	}
	
	@PutMapping("/estrutura")
	public ResponseEntity<EstruturaImovelDTO> salvarEstrutura(@RequestBody EstruturaImovelDTO  estruturaImovelDTO) {
		estruturaImovelDTO = imovelService.estruturaImovel(estruturaImovelDTO);
		return ResponseEntity.ok(estruturaImovelDTO);
	}
	
	
	
	@PutMapping("/terreno")
	public ResponseEntity<TerrenoImovelDTO> salvarTerreno(@RequestBody TerrenoImovelDTO  terrenoImovelDTO) {
		terrenoImovelDTO = imovelService.terrenoImovel(terrenoImovelDTO);
		return ResponseEntity.ok(terrenoImovelDTO);
	}
	
	@PutMapping("/valores")
	public ResponseEntity<ValoresImovelDTO> salvarValores(@RequestBody ValoresImovelDTO  valoresImovelDTO) {
		valoresImovelDTO = imovelService.valoresImovel(valoresImovelDTO);
		return ResponseEntity.ok(valoresImovelDTO);
	}
	
	
	
	
	
	/**
	 * SE RETORNAR FALSE É PORQUE PELOMENOS UM DOS HORÁRIOS NÃO ESTÁ NA AGENDA
	 * ENTÃO DEVE SER INCLUIDO  O PERIODO INTEIRO, NO CASO DE VERDADEIRO EXCLUI O PERÍODO INTEIRO
	 * @param horariosParaSalvar
	 * @param imo
	 * @return
	 */
	private boolean isIncluirExcluirPeriodo(List<Horario> horariosParaSalvar,
			Imovel imo,DiasSemana dia) {
		
			boolean temp = false;
			for (com.ndireto.api.entity.Horario horario : horariosParaSalvar) {
				temp = false;
				if(imo.getDisponibilidadeVisitaImovel()!=null) {
				for (DisponibilidadeVisitaImovel lista : imo.getDisponibilidadeVisitaImovel()) {
					if(horario.getCodigo().equals(lista.getHorario().getCodigo())
							&& dia.getCodigo().equals(lista.getDiasSemana().getCodigo())) {
						temp = true;
					}
				}
				}
				
				if(!temp) {
					break;
				}
			
		}
		
		
		return temp;
	}
	
	
	
}
