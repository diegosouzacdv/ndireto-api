package com.ndireto.api.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.dto.EnderecoPessoaDTO;
import com.ndireto.api.dto.LogradouroDTO;
import com.ndireto.api.entity.Bairro;
import com.ndireto.api.entity.Cidade;
import com.ndireto.api.entity.Logradouro;
import com.ndireto.api.entity.Ufe;
import com.ndireto.api.repository.BairroRepository;
import com.ndireto.api.repository.CidadeRepository;
import com.ndireto.api.repository.LogradouroRepository;
import com.ndireto.api.repository.UfeRepository; 

@RestController
@RequestMapping("/endereco")
public class EnderecoResource { 
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private BairroRepository bairroRepository;
	
	@Autowired
	private LogradouroRepository logradouroRepository;
	
	@Autowired
	private UfeRepository ufeRepository;
	
	@GetMapping("/buscaestados")
	public List<Ufe> buscarEstados()
	{	
		List<Ufe> listUfe = ufeRepository.findByAtivo(1);
		return listUfe;
	}
	
	@GetMapping("/buscacidade")
	public List<Cidade> buscarCidade(@RequestParam(name="id") Integer id)
	{	
		List<Cidade> listCidade = cidadeRepository.findByUfeId(id);
		return listCidade;
	}
	
	@GetMapping("/buscabairro")
	public List<Bairro> buscarBairro(@RequestParam(name="id") Integer id)
	{	
		List<Bairro> listBairro = bairroRepository.findByCidadeId(id);
		return listBairro;
	}
	
	@GetMapping("/buscalogradouro")
	public List<Logradouro> buscarLogradouro(@RequestParam(name="id") Integer id)
	{	
		List<Logradouro> listLogradouro = logradouroRepository.findByBairroId(id);
		return listLogradouro;
	}
	
	@GetMapping("/buscarcep/{cep}")
	 public EnderecoPessoaDTO BuscarCep(@PathVariable String cep) {
		EnderecoPessoaDTO enderecoDTO =  new EnderecoPessoaDTO();
		Logradouro log = logradouroRepository.findByCep(cep);
		System.err.println(log);
		LogradouroDTO logDto = log!= null ? log.fromDTO() : new LogradouroDTO();
		enderecoDTO.setLogradouro(logDto);
		return enderecoDTO;
	 }
	
}
