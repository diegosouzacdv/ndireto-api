package com.ndireto.api.resources;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ndireto.api.dto.AtualizarPessoaDTO;
import com.ndireto.api.dto.ContaDTO;
import com.ndireto.api.dto.EnderecoDTO;
import com.ndireto.api.dto.NovaPessoaDTO;
import com.ndireto.api.entity.Banco;
import com.ndireto.api.entity.Conta;
import com.ndireto.api.entity.Endereco;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.TipoConta;
import com.ndireto.api.repository.ContaRepository;
import com.ndireto.api.repository.EnderecoRepository;
import com.ndireto.api.repository.LogradouroRepository;
import com.ndireto.api.repository.PessoaRepository;
import com.ndireto.api.repository.TipoContaRepository;
import com.ndireto.api.services.PessoaService;

@RestController
@RequestMapping("/pessoas")
public class PessoaResource {
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Autowired
	private PessoaService service;
	
	@Autowired
	private LogradouroRepository logradouroRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private ContaRepository contaRepository;
	
	@Autowired
	private TipoContaRepository tipoContaRepository;

	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PESSOA') and #oauth2.hasScope('read')")
	public List<Pessoa> listar() {
		return pessoaRepository.findAll();
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PESSOA') and #oauth2.hasScope('read')")
	public Pessoa buscarPeloId(@PathVariable int id) {
		return pessoaRepository.findOne(id);
	}
	
	@GetMapping("/tipoConta")
	public ResponseEntity<List<TipoConta>> tipoCOnta() {
		return ResponseEntity.ok().body(tipoContaRepository.findAll());
	}
	
	@PostMapping
	public ResponseEntity<Pessoa> CadastrarPessoa(@RequestBody NovaPessoaDTO novaPessoaDto, HttpServletResponse response) {
		Pessoa obj = service.fromDTO(novaPessoaDto);
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}")
			.buildAndExpand(obj.getId()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		return ResponseEntity.created(uri).body(obj);
	}
	
	
	@PutMapping("/atualizar/dados")
	public AtualizarPessoaDTO atualizarCPF(@RequestBody  AtualizarPessoaDTO atualizarPessoaDTO, @AuthenticationPrincipal Object usuario) {
		
		Pessoa pessoa = service.buscar(Integer.valueOf(usuario.toString()));
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate nascimento = LocalDate.parse(atualizarPessoaDTO.getDataNascimento(),formatter);
		
		pessoa.setCpf(atualizarPessoaDTO.getCpf());;
		pessoa.setCelular(atualizarPessoaDTO.getCelular());
		pessoa.setDataNascimento(nascimento);
		
		service.salvarPessoa(pessoa);
		
		return atualizarPessoaDTO;
	}
	
	
	@PutMapping("/atualizar/endereco")
	public EnderecoDTO atualizarEndereco(@RequestBody EnderecoDTO enderecoDTO, @AuthenticationPrincipal Object usuario) {
		
		
		Pessoa pessoa = service.buscar(Integer.valueOf(usuario.toString()));
		
		Endereco endereco = new Endereco();
		
		endereco.setPessoa(pessoa);
		endereco.setNumero(enderecoDTO.getNumero());
		endereco.setComplemento(enderecoDTO.getComplemento());
		endereco.setLogradouro(logradouroRepository.findById(enderecoDTO.getLogradouroCodigo()).get());
		enderecoRepository.save(endereco);
		
		
		return enderecoDTO;
	}
	
	
	@PutMapping("/atualizar/conta")
	public ContaDTO atualizarConta(@RequestBody ContaDTO contaDTO, @AuthenticationPrincipal Object usuario) {
		Pessoa pessoa = service.buscar(Integer.valueOf(usuario.toString()));
		Conta conta = new Conta();
		Banco banco = new Banco();
		banco.setId(contaDTO.getBanco());
		TipoConta tipo = new TipoConta();
		tipo.setId(contaDTO.getTipo());
		conta.setPessoa(pessoa);
		conta.setAgencia(contaDTO.getAgencia());
		conta.setBanco(banco);
		conta.setDigito(contaDTO.getDigito());
		conta.setTipoConta(tipo);
		conta.setNumeroConta(contaDTO.getConta());
		contaRepository.save(conta);
		return contaDTO;
	}
	

}
