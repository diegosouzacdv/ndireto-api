package com.ndireto.api.resources;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.dto.HistoricoFinanceiroDTO;
import com.ndireto.api.entity.HistoricoFinanceiro;
import com.ndireto.api.repository.HistoricoFinanceiroRepository;
import com.ndireto.api.services.HistoricoFinanceiroService;


@RestController
@RequestMapping("/financeiro")
public class FinanceiroResource {

	
	@Autowired
	private HistoricoFinanceiroRepository historicoFinanceiroRepository;
	
	@Autowired
	private HistoricoFinanceiroService historicoFinanceiroService;

	
	@PutMapping("/ler/{id}")
	public ResponseEntity<HistoricoFinanceiroDTO> marcarMensagemComoLida(@PathVariable Integer id) {
		Optional<HistoricoFinanceiro> optFinanceiro = historicoFinanceiroRepository.findById(id);
		HistoricoFinanceiroDTO retorno = new HistoricoFinanceiroDTO();
		optFinanceiro.get().setDataVisualizacao(LocalDateTime.now());
		historicoFinanceiroService.marcarLida(optFinanceiro.get());
		
		retorno = retorno.fromDto(optFinanceiro.get());
		
		
		

		return ResponseEntity.ok(retorno);
	}

	


}
