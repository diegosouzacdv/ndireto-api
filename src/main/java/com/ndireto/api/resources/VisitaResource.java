package com.ndireto.api.resources;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ndireto.api.dto.ImovelDTO;
import com.ndireto.api.dto.VisitaDTO;
import com.ndireto.api.entity.Imovel;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.Visita;
import com.ndireto.api.repository.ImovelRepository;
import com.ndireto.api.repository.PessoaRepository;
import com.ndireto.api.repository.VisitaRepository;

@RestController
@RequestMapping()
public class VisitaResource {

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Autowired
	private VisitaRepository visitaRepository;
	
	@Autowired
	private ImovelRepository imovelRepository;
	
	@GetMapping("/fotografo/visita/{id}")
	public ResponseEntity<VisitaDTO> buscarVisitaId(
			@PathVariable int id, @AuthenticationPrincipal Object usuario) {	
		Visita visitas = visitaRepository.findOne(id);
		VisitaDTO visitaDto = visitas.fromDTO();
				
		return ResponseEntity.ok(visitaDto);
				
	}
	
	
	@GetMapping("/fotografo/imoveis/disponiveis")
	public ResponseEntity<List<ImovelDTO>> buscarDisponiveisParaFotografia(
			@AuthenticationPrincipal Object usuario) {	
		
		List<Imovel> imoveis = imovelRepository.findByStatusImovelIdOrderByDataCadastro(11);
		List<ImovelDTO> imoveisDto = new ArrayList<ImovelDTO>();
		
		
		for (Imovel obj : imoveis) {
			imoveisDto.add(obj.fromDTO());
		}
		
		
		return ResponseEntity.ok(imoveisDto);
				
	}
	
	
	@GetMapping("/fotografo/agendas/{id}")
	public ResponseEntity<List<VisitaDTO>> buscarImoveis(@PathVariable int id, @AuthenticationPrincipal Object usuario) {
		
		
		List<VisitaDTO> visitasDto = new ArrayList<VisitaDTO>();
		Optional<Pessoa> pessoa = pessoaRepository.findById(Integer.valueOf(usuario.toString()));
		List<Visita> visitas = new ArrayList<Visita>();
		
		LocalDateTime dia = LocalDateTime.now();
	
		
		if(id == 1) {
			visitas = visitaRepository.findByPessoaColaboradorIdAndTipoVisitaIdAndDataHoraVisitaAfter(pessoa.get().getId(), 2, dia);
			
		}
		else if(id == 2) {
			 visitas = visitaRepository.findByPessoaColaboradorIdAndTipoVisitaIdAndDataHoraVisitaBefore(pessoa.get().getId(), 2, dia);
			
		}
		
		for (Visita obj : visitas) {
			VisitaDTO visita = obj.fromDTO();
			visitasDto.add(visita);
		}
		
		if(visitas.isEmpty()) {
			return ResponseEntity.ok(new ArrayList<VisitaDTO>());
		}
		else {
			return ResponseEntity.ok(visitasDto);
		}
				
	}
	
	@GetMapping("/fotografo/quantidade/agendas/{id}")
	public ResponseEntity<Integer> buscarQuantidadeImoveis(@PathVariable int id, @AuthenticationPrincipal Object usuario) {
		
		Integer quantidade = 0;
		Optional<Pessoa> pessoa = pessoaRepository.findById(Integer.valueOf(usuario.toString()));
		List<Visita> visitas = new ArrayList<Visita>();
		LocalDateTime dia = LocalDateTime.now();
	
		if(id == 1) {
			visitas = visitaRepository.findByPessoaColaboradorIdAndTipoVisitaIdAndDataHoraVisitaAfter(pessoa.get().getId(), 2, dia);
			
		}
		else if(id == 2) {
			 visitas = visitaRepository.findByPessoaColaboradorIdAndTipoVisitaIdAndDataHoraVisitaBefore(pessoa.get().getId(), 2, dia);
			
		}
		
		for (Visita obj : visitas) {
			quantidade++;
		}
		return ResponseEntity.ok(quantidade);
				
	}
	
	
}
