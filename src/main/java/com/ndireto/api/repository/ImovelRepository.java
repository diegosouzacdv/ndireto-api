package com.ndireto.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Imovel;

@Repository
public interface ImovelRepository extends JpaRepository<Imovel, Integer>{

	Optional<Imovel> findById(int id);

	List<Imovel> findByPessoaCadastranteIdOrderByDataCadastroDesc(int id);

	List<Imovel> findByPessoaCadastranteId(int id);

	List<Imovel> findByPessoaCadastranteIdOrderByDataCadastroAsc(int id);

	List<Imovel> findAllByOrderByDataCadastroDesc();

	List<Imovel> findByProprietarioIdOrderByDataCadastroDesc(int id);

	List<Imovel> findTop5ByOrderByDataCadastroDesc();

	List<Imovel> findByStatusImovelIdOrderByDataCadastro(int status);

	List<Imovel> findByVisitasPessoaColaboradorIdAndVisitasTipoVisitaIdOrderByDataCadastro(int i, int id);

	

	List<Imovel> findTop5ByStatusImovelIdOrderByDataCadastroDesc(int i);


	Long countByAtivo(int i);

	List<Imovel> findTop15ByStatusImovelIdOrderByDataCadastroDesc(int i);
	
	List<Imovel> findByStatusImovelIdOrderByDataCadastroDesc(int i);

	Long countByAtivoAndStatusImovelId(int i,int status);
	
}
