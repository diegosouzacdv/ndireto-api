package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.TipoItens;

@Repository
public interface TipoItensRepository extends JpaRepository<TipoItens, Integer>{

	List<TipoItens> findByAtivo(int ativo);
	
	TipoItens findById(TipoItens item);

}
