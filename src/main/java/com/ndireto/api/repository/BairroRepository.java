package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Bairro;

@Repository
public interface BairroRepository extends JpaRepository<Bairro, Integer>{

	List<Bairro> findByCidadeId(Integer cidade);

}
