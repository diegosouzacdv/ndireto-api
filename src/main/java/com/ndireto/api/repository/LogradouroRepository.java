package com.ndireto.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Logradouro;

@Repository
public interface LogradouroRepository extends JpaRepository<Logradouro, Integer>{

	List<Logradouro> findByBairroId(Integer id);

	Logradouro findByCep(String cep);

	Optional<Logradouro> findById(Integer logradouroCodigo);

}
