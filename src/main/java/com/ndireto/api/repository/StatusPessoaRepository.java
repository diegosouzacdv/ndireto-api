package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.StatusPessoa;

@Repository
public interface StatusPessoaRepository extends JpaRepository<StatusPessoa, Integer>{
	StatusPessoa findById(int i);
}
