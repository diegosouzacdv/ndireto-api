package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.StatusVisita;

@Repository
public interface StatusVisitaRepository extends JpaRepository<StatusVisita, Integer>{

}
