package com.ndireto.api.repository.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ndireto.api.dto.PessoaPesquisaDTO;


public class PessoaRepositoryImpl implements PessoaRepositoryQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public List<PessoaPesquisaDTO> buscarPessoaNomeCPF(String nomeCpf) {
		
		String jpql = "SELECT com.ndireto.admin.dto.PessoaPesquisaDTO(pes.id, pes.nome, pes.cpf, pes.email, pes.celular) "
				+ "FROM Pessoa pes where lower(pes.nome) like lower(:nomeCpf) ";
		List<PessoaPesquisaDTO> pessoaFiltro = manager.createQuery(jpql, PessoaPesquisaDTO.class)
				.setParameter("nomeCpf", "%" + nomeCpf + "%")
				.setMaxResults(10)
				.getResultList();
		
		return pessoaFiltro;
	}
	
}
