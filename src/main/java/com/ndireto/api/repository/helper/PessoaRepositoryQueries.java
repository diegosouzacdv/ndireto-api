package com.ndireto.api.repository.helper;

import java.util.List;

import com.ndireto.api.dto.PessoaPesquisaDTO;

public interface PessoaRepositoryQueries {

	List<PessoaPesquisaDTO> buscarPessoaNomeCPF(String nomeCpf);

}
