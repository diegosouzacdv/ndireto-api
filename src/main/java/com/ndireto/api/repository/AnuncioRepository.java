package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Anuncio;

@Repository
public interface AnuncioRepository extends JpaRepository<Anuncio, Integer>{

	List<Anuncio> findByImovelId(int id);

}
