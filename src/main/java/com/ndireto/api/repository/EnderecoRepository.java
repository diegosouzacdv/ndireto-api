package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Endereco;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Integer>{

	List<Endereco> findByPessoaId(Integer id);

	void deleteById(Integer id);


}
