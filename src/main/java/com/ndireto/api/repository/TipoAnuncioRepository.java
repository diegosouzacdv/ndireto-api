package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.TipoAnuncio;

@Repository
public interface TipoAnuncioRepository extends JpaRepository<TipoAnuncio, Integer>{

}
