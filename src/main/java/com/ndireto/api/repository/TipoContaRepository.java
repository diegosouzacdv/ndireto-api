package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.TipoConta;

@Repository
public interface TipoContaRepository extends JpaRepository<TipoConta, Integer>{

}
