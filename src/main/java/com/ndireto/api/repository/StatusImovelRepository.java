package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.StatusImovel;

@Repository
public interface StatusImovelRepository extends JpaRepository<StatusImovel, Integer>{
	StatusImovel findById(int i);

	List<StatusImovel> findByAtivo(int i);
}
