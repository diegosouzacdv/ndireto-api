package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Ufe;

@Repository
public interface UfeRepository extends JpaRepository<Ufe, Integer>{

	List<Ufe> findByAtivo(int ativo);

}
