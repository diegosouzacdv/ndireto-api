package com.ndireto.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ndireto.api.entity.ImovelItens;

public interface ImovelItensRepository extends JpaRepository<ImovelItens, Integer> {

	Optional<ImovelItens> findByImovelIdAndItensId(Integer imovel, Integer item);

}
