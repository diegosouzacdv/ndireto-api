package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.ValidacaoPessoa;

@Repository
public interface ValidacaoPessoaRepository extends JpaRepository<ValidacaoPessoa, Integer>{


}
