package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.MotivoVenda;

@Repository
public interface AceitaNegociarRepository extends JpaRepository<MotivoVenda, Integer>{

}
