package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Cidade;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Integer>{

	List<Cidade> findByUfe(Integer id);
	
	List<Cidade> findByUfeId(Integer id);
	
	Cidade findByCep(String cep);

}
