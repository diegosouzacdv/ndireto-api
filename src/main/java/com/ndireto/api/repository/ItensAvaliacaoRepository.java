package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.ItensAvaliacao;

@Repository
public interface ItensAvaliacaoRepository extends JpaRepository<ItensAvaliacao, Integer>{

	List<ItensAvaliacao> findByAtivo(int i);

}
