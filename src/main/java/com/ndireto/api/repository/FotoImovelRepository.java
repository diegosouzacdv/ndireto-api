package com.ndireto.api.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.FotoImovel;

@Repository
public interface FotoImovelRepository extends JpaRepository<FotoImovel,Integer>{

	FotoImovel findById(Integer id);



}

