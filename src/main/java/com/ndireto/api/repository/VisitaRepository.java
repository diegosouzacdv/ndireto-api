package com.ndireto.api.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Visita;

@Repository
public interface VisitaRepository extends JpaRepository<Visita, Integer>{

	List<Visita> findByImovelId(int id);

	List<Visita> findByImovelIdAndStatusVisitaId(int id, int id2);

	List<Visita> findByPessoaColaboradorIdAndTipoVisitaId(int id, int i);

	List<Visita> findByPessoaColaboradorIdAndTipoVisitaIdAndDataHoraVisitaBefore(int id, int i, LocalDateTime dia);

	List<Visita> findByPessoaColaboradorIdAndTipoVisitaIdAndDataHoraVisitaAfter(int id, int i, LocalDateTime dia);

}
