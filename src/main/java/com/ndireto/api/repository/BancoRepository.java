package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Banco;

@Repository
public interface BancoRepository extends JpaRepository<Banco, Integer>{

	List<Banco> findByOrderByCodigoReal();

	List<Banco> findByOrderByNome();

	List<Banco> findByAtivoOrderByNome(int i);

}
