package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.TipoHistoricoFinanceiro;

@Repository
public interface TipoHistoricoFinanceiroRepository extends JpaRepository<TipoHistoricoFinanceiro, Integer>{
	TipoHistoricoFinanceiro findById(int i);

	TipoHistoricoFinanceiro findByNome(String string);
}
