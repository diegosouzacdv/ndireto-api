package com.ndireto.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Itens;

@Repository
public interface ItensRepository extends JpaRepository<Itens, Integer>{

	List<Itens> findByAtivo(int ativo);
	
	Optional<Itens> findById(int id);

	List<Itens> findByAtivoOrderByNomeDesc(int i);

	List<Itens> findByAtivoOrderByNomeAsc(int i);

}
