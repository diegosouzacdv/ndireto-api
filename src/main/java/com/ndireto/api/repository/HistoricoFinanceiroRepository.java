package com.ndireto.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.HistoricoFinanceiro;

@Repository
public interface HistoricoFinanceiroRepository extends JpaRepository<HistoricoFinanceiro, Integer>{

	Optional<HistoricoFinanceiro> findById(Integer id);
}
