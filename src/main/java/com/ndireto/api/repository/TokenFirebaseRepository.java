package com.ndireto.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.TokenFirebase;

@Repository
public interface TokenFirebaseRepository extends JpaRepository<TokenFirebase, Integer>{

	List<TokenFirebase> findByPessoaId(int id);

}
