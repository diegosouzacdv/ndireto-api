package com.ndireto.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Mensagem;

@Repository
public interface MensagemRepository extends JpaRepository<Mensagem, Integer>{


	List<Mensagem> findByDestinatarioIdAndAtivoOrderByDataInclusaoDesc(int id, int i);

	List<Mensagem> findByDestinatarioIdAndAtivoAndAlertaOrderByDataInclusaoDesc(int id, int i, int j);

	List<Mensagem> findByDestinatarioIdAndDataRecebimentoIsNullAndAtivoOrderByDataInclusaoDesc(int id, int i);
	
	List<Mensagem> findByDestinatarioIdAndDataRecebimentoIsNullAndAtivo(int id, int i);
	
	List<Mensagem> findByDestinatarioIdAndDataRecebimentoIsNull(int id);
	
	Optional<Mensagem> findById(int idmsg);

	List<Mensagem> findByDestinatarioIdAndAtivoAndDataRecebimentoIsNullOrderByDataInclusaoDesc(Integer valueOf, int i);

	List<Mensagem> findByAtivoAndDataEnvioWatsAppIsNullOrderByDataInclusaoDesc(int i); 

}
