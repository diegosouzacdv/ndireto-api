package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.TipoVisita;

@Repository
public interface TipoVisitaRepository extends JpaRepository<TipoVisita, Integer>{
	
	List<TipoVisita> findByAtivo(int i);

}
