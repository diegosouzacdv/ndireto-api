package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.DiasSemana;

@Repository
public interface DiasSemanaRepository extends JpaRepository<DiasSemana, Integer>{


}
