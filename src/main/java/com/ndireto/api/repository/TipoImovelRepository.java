package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.TipoImovel;

@Repository
public interface TipoImovelRepository extends JpaRepository<TipoImovel, Integer>{

	List<TipoImovel> findByAtivo(int ativo);

}
