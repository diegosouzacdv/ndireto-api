package com.ndireto.api.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Foto;

@Repository
public interface FotoRepository extends JpaRepository<Foto,Integer>{


	Foto findById(int valueOf);

	List<Foto> findByPessoaId(int valueOf); 

}

