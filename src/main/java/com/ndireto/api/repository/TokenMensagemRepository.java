package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ndireto.api.entity.TokenMensagem;

public interface TokenMensagemRepository extends JpaRepository<TokenMensagem, Integer>{

}
