package com.ndireto.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.FormaPagamento;


@Repository
public interface FormaPagamentoRepository extends JpaRepository<FormaPagamento, Integer>{

}
