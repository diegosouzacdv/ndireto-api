package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.MotivoVenda;

@Repository
public interface MotivoVendaRepository extends JpaRepository<MotivoVenda, Integer>{

	List<MotivoVenda> findByAtivo(int i);

}
