package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.TipoNegociacao;

@Repository
public interface TipoNegociacaoRepository extends JpaRepository<TipoNegociacao, Integer>{

	List<TipoNegociacao> findByAtivo(int ativo);

}
