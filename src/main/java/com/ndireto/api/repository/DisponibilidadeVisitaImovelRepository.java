package com.ndireto.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ndireto.api.entity.DisponibilidadeVisitaImovel;

public interface DisponibilidadeVisitaImovelRepository extends JpaRepository<DisponibilidadeVisitaImovel, Integer> {


	Optional<DisponibilidadeVisitaImovel> findByDiasSemanaCodigoAndHorarioCodigoAndImovelId(Integer dia, Integer hora,
			Integer imovel);

	Optional<DisponibilidadeVisitaImovel> findByDiasSemanaCodigoAndImovelId(Integer dia, Integer imovel);

	List<DisponibilidadeVisitaImovel> findByImovelId(Integer id);

}
