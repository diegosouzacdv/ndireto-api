package com.ndireto.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.Pessoa;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Integer>{
	
//	Pessoa findById(int id);
//
//	Optional<Pessoa> findByCpf(String cpf);
//	
//	Pessoa findByCpfOrderByCpfAsc(String cpf);
//	
	Optional<Pessoa> findByEmail(String email);

	Optional<Pessoa> findById(Integer valueOf);

	Optional<Pessoa> findByToken(String indicacao);

	List<Pessoa> findByPessoaIndicadorId(Integer valueOf);

//	@Query(value = "SELECT * FROM pessoa P WHERE exists(select PES_CODIGO from PESSOA_TIPOPESSOA TP where TP.PES_CODIGO = P.PES_CODIGO AND TP.TPS_CODIGO = ?1)", nativeQuery = true)
//	List<Pessoa> findByTipoPessoaNative(Integer tipoPessoa);

	
//	Optional<Pessoa> findByEmailAndDataNascimento(String email, LocalDate dataNascimento);


}
