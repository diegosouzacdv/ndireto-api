package com.ndireto.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ndireto.api.entity.PessoaTipoPessoa;

@Repository
public interface PessoaTipoPessoaRepository extends JpaRepository<PessoaTipoPessoa, Integer>{

//	List<PessoaTipoPessoa> findByIdPessoaId(Integer id);

	List<PessoaTipoPessoa> findByPessoaId(Integer id);

	List<PessoaTipoPessoa> findByTipoPessoaId(int i);

	List<PessoaTipoPessoa> findByTipoPessoaIdIn(List<Integer> ids);

}
