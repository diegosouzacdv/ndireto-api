package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="STATUSIMOVEL")
public class StatusImovel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="STI_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="STI_NOME")
	private String nome;
	
	@Column(name = "STI_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Transient
	private String nomeFormatado;
	
	
	public String getNome() {
		return nome;
	}
	

	public String getNomeFormatado() {
		
		String retorno = "<span class=\"label label-success\" >"+nome+"</span>";
		
		if(this.id==1) {
			retorno = "<span class=\"label label-danger\" >"+nome+"</span>";
		}
		else if(this.id==2) {
			retorno = "<span class=\"label label-info\" >"+nome+"</span>";
		}
		else {
			retorno = "<span class=\"label label-success\" >"+nome+"</span>";
		}
		
		return retorno;
	}

	public void setNomeFormatado(String nomeFormatado) {
		this.nomeFormatado = nomeFormatado;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusImovel other = (StatusImovel) obj;
		if (id != other.id)
			return false;
		return true;
	}


}