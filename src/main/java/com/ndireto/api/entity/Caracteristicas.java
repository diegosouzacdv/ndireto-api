package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ndireto.api.dto.CaracteristicaImovelDTO;

@Entity
@Table(name="CARACTERISTICAS")
public class Caracteristicas implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CAR_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="CAR_NEGOCIAVEL")
	private int aceitaNegociar;
	
	@Column(name="CAR_IDADEIMOVEL")
	private int idadeImovel;
	
	@Column(name="CAR_PERMUTA")
	private int permuta;
	
	
	@Column(name="CAR_PROPRIETARIO_VENDEDOR")
	private int proprietarioVendedor;
	
	@Column(name="CAR_PONTOS_NEGATIVOS")
	private String pontosNegativos;
	
	@Column(name="CAR_PONTOS_POSITIVOS")
	private String pontosPositivos;
	
	@Column(name = "CAR_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private int ativo;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="IMO_CODIGO")
	private Imovel imovel;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAceitaNegociar() {
		return aceitaNegociar;
	}

	public void setAceitaNegociar(int aceitaNegociar) {
		this.aceitaNegociar = aceitaNegociar;
	}

	public String getPontosNegativos() {
		return pontosNegativos;
	}

	public void setPontosNegativos(String pontosNegativos) {
		this.pontosNegativos = pontosNegativos;
	}

	public String getPontosPositivos() {
		return pontosPositivos;
	}

	public void setPontosPositivos(String pontosPositivos) {
		this.pontosPositivos = pontosPositivos;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}
	
	public int getIdadeImovel() {
		return idadeImovel;
	}

	public void setIdadeImovel(int idadeImovel) {
		this.idadeImovel = idadeImovel;
	}
	
	public int getPermuta() {
		return permuta;
	}

	public void setPermuta(int permuta) {
		this.permuta = permuta;
	}
	

	public int getProprietarioVendedor() {
		return proprietarioVendedor;
	}

	public void setProprietarioVendedor(int proprietarioVendedor) {
		this.proprietarioVendedor = proprietarioVendedor;
	}

	
	
	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Caracteristicas other = (Caracteristicas) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Caracteristicas [id=" + id + ", aceitaNegociar=" + aceitaNegociar + ", idadeImovel=" + idadeImovel
				+ ", permuta=" + permuta + ", proprietarioVendedor=" + proprietarioVendedor + ", "
				+ (pontosNegativos != null ? "pontosNegativos=" + pontosNegativos + ", " : "")
				+ (pontosPositivos != null ? "pontosPositivos=" + pontosPositivos + ", " : "") + "ativo=" + ativo + ", "
				+ "]";
	}



}
