package com.ndireto.api.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;
import org.thymeleaf.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the pessoafisica_unidade database table.
 * 
 */
@Entity
@Table(name="MENSAGEM")
//@NamedQuery(name="pessoaFisica_unidade.findAll", query="SELECT p FROM pessoaFisica_unidade p")
public class Mensagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MEN_CODIGO",unique=true, nullable=false)
	private int id;
	
	//bi-directional many-to-one association to PessoafisicaUnidade
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="PES_CODIGOREMETENTE")
	private Pessoa remetente;
	
	//bi-directional many-to-one association to PessoafisicaResponsavel
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="PES_CODIGODESTINATARIO")
	private Pessoa destinatario;
	
	//bi-directional many-to-one association to Condominio
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="IMO_CODIGO")
	private Imovel imovel;

	@Column(name="MEN_DTINCLUSAO")
	@DateTimeFormat(pattern="dd/MM/yyyy HH:mm")
	private LocalDateTime dataInclusao;

	@Column(name="MEN_DTRECEBIMENTO")
	@DateTimeFormat(pattern="dd/MM/yyyy HH:mm")
	private LocalDateTime dataRecebimento;
	
	@Column(name="MEN_OBSERVACAO")
	private String observacao;
	
	@Column(name="MEN_ASSUNTO")
	private String assunto;
	
	
	@Column(name="MEN_OBSERVACAO_ENCODE")
	private String observacaoencode;
	
	@Column(name="MEN_ACAO")
	private String acao;
	
	@Column(name = "MEN_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private int ativo;
	
	@Column(name = "MEN_ALERTA", nullable = false,  columnDefinition = "int default 0", insertable = false, updatable = true)
	private int alerta;
	
	@Column(name="MEN_DTHORAENVIOWHATSAPP")
	@DateTimeFormat(pattern="dd/MM/yyyy HH:mm")
	private LocalDateTime dataEnvioWatsApp;
	
	
	@ManyToOne
	@JoinColumn(name="PES_CODIGOENVIOWHATSAPP")
	private Pessoa usuarioEnvioWatsApp;
	
	@Transient
	private String imagem;

	
	public void setObservacaoencode(String observacaoencode) {
		this.observacaoencode = observacaoencode;
	}
	
	public String getObservacaoencode() {
		return observacaoencode;
	}
	
	public int getId() {
		return id;
	}
	
	
	
	public LocalDateTime getDataEnvioWatsApp() {
		return dataEnvioWatsApp;
	}




	public void setDataEnvioWatsApp(LocalDateTime dataEnvioWatsApp) {
		this.dataEnvioWatsApp = dataEnvioWatsApp;
	}




	public Pessoa getUsuarioEnvioWatsApp() {
		return usuarioEnvioWatsApp;
	}




	public void setUsuarioEnvioWatsApp(Pessoa usuarioEnvioWatsApp) {
		this.usuarioEnvioWatsApp = usuarioEnvioWatsApp;
	}




	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	public Pessoa getRemetente() {
		return remetente;
	}

	public void setRemetente(Pessoa remetente) {
		this.remetente = remetente;
	}

	public Pessoa getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(Pessoa destinatario) {
		this.destinatario = destinatario;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	

	public LocalDateTime getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(LocalDateTime dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public LocalDateTime getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(LocalDateTime dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}
	
	public int getAlerta() {
		return alerta;
	}

	public void setAlerta(int alerta) {
		this.alerta = alerta;
	}
	
	
	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	
	public String getObservacaoReduzida(int tamanho) {

		String retorno = "";
		String original = this.observacao != null ? this.observacao.replaceAll("\\<[^>]*>","") : "";

		if(original.length()>tamanho) {
			retorno = original.substring(0, tamanho)+"...";
		}
		else {
			retorno = original;
		}


		return retorno;
	}
	
	
	public String getAssuntoReduzida(int tamanho) {

		String retorno = "";
		String original = this.assunto.replaceAll("\\<[^>]*>","");

		if(original.length()>tamanho) {
			retorno = original.substring(0, tamanho)+"...";
		}
		else {
			retorno = original;
		}


		return retorno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mensagem other = (Mensagem) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@JsonIgnore
	public String getTipoReduzido(){
		
		String retorno ="png";
		
		if(this.imagem == null || StringUtils.isEmpty(this.imagem)) {
			String arrayNome[] = this.imagem.split("\\."); 
			retorno =  arrayNome[1];
		}
		
		return retorno;

	}
	
	
}