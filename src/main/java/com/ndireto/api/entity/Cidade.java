package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ndireto.api.dto.CidadeDTO;
import com.ndireto.api.dto.MunicipioDTO;


/**
 * The persistent class for the cidade database table.
 * 
 */
@Entity
@Table(name="CIDADE")
public class Cidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CID_CODIGO",unique=true, nullable=false) 
	private int id;

	@Column(name="CID_ATIVO")
	private int ativo;

	@Column(name="CID_LATITUDE")
	private String latitude;
	
	@Column(name="CID_LONGITUDE")
	private String longitude;

	@Column(name="CID_NOME")
	private String nome;
	
	@Column(name="CID_CEP")
	private String cep;

	//bi-directional many-to-one association to Ufe
	@ManyToOne
	@JoinColumn(name="UFE_CODIGO")
	private Ufe ufe;
	
	@Transient
	private int isCidade;
    
    public int getIsCidade() {
		return isCidade;
	}

	public void setIsCidade(int isCidade) {
		this.isCidade = isCidade;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Ufe getUfe() {
		return ufe;
	}

	public void setUfe(Ufe ufe) {
		this.ufe = ufe;
	}

	public int getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	
	public CidadeDTO fromDTO() {
		CidadeDTO dto = new CidadeDTO();
		dto.setCodigo(this.getId());
		dto.setNome(this.getNome());
		dto.setUf(this.getUfe().fromDTO());
		return dto;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cidade other = (Cidade) obj;
		if (id != other.id)
			return false;
		return true;
	}



}