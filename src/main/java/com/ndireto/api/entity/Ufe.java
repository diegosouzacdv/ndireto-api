package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ndireto.api.dto.CidadeDTO;
import com.ndireto.api.dto.UfDto;


/**
 * The persistent class for the ufe database table.
 * 
 */
@Entity
@Table(name="UFE")
public class Ufe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="UFE_CODIGO",unique=true, nullable=false)
	private Integer id;

	@Column(name="UFE_ATIVO")
	private int ativo;

	@Column(name="UFE_NOME")
	private String nome;

	@Column(name="UFE_SIGLA")
	private String sigla;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ufe other = (Ufe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public UfDto fromDTO() {
		   UfDto dto = new UfDto();
			dto.setCodigo(this.getId());
			dto.setNome(this.getNome());
			return dto;
	}

	

	

}