package com.ndireto.api.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="TIPOPESSOA")
public class TipoPessoa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TPS_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="TPS_NOME")
	private String nome;
	
	@Column(name = "TPS_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	
	@JsonIgnore
	@OneToMany(mappedBy="tipoPessoa")
	private List<PessoaTipoPessoa> pessoasTipos;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	public List<Pessoa> getPessoas() {
		List<Pessoa> retorno = new ArrayList<Pessoa>();
		
		for (PessoaTipoPessoa pessoaTipoPessoa : pessoasTipos) {
			retorno.add(pessoaTipoPessoa.getPessoa());
		}
		
		return retorno;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoPessoa other = (TipoPessoa) obj;
		if (id != other.id)
			return false;
		return true;
	}

	

	
	

}