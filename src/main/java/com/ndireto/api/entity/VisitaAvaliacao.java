package com.ndireto.api.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="VISITAAVALIACAO")
public class VisitaAvaliacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="AVA_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="AVA_PONTUACAO")
	private int pontuacao;
	
	@ManyToOne
	@JoinColumn(name="IAV_CODIGO")
	private ItensAvaliacao itemAvaliacao;

	@ManyToOne
	@JoinColumn(name="pes_codigo_demonstrador")
	private Pessoa demonstrador;
	
	@ManyToOne
	@JoinColumn(name="pes_codigo_visitante")
	private Pessoa visitante;
	
	@ManyToOne
	@JoinColumn(name="VIS_CODIGO")
	private Visita visita;
	
	@Column(name="OBSERVACAO")
	private String observacao;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public ItensAvaliacao getItemAvaliacao() {
		return itemAvaliacao;
	}

	public void setItemAvaliacao(ItensAvaliacao itemAvaliacao) {
		this.itemAvaliacao = itemAvaliacao;
	}

	public Pessoa getDemonstrador() {
		return demonstrador;
	}

	public void setDemonstrador(Pessoa demonstrador) {
		this.demonstrador = demonstrador;
	}

	public Pessoa getVisitante() {
		return visitante;
	}

	public void setVisitante(Pessoa visitante) {
		this.visitante = visitante;
	}

	public Visita getVisita() {
		return visita;
	}

	public void setVisita(Visita visita) {
		this.visita = visita;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisitaAvaliacao other = (VisitaAvaliacao) obj;
		if (id != other.id)
			return false;
		return true;
	}

}