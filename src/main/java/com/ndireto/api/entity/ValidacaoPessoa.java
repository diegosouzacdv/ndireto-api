package com.ndireto.api.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ndireto.api.dto.datavalid.retorno.AnswerRetorno;


/**
 * The persistent class for the VALIDACAO_PESSOA database table.
 * 
 */
@Entity
@Table(name="VALIDACAO_PESSOA")
public class ValidacaoPessoa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="VAL_CODIGO",unique=true, nullable=false)
	private Integer codigo;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="PES_CODIGO")
	private Pessoa pessoa;

	@Column(name="VAL_CNH_CATEGORIA_VALIDO")
	private boolean cnhCategoriaValido;

	@Column(name="VAL_CNH_DATA_PRIMEIRA_VALIDO")
	private boolean cnhDataPrimeiraValido;

	@Column(name="VAL_CNH_DATA_ULTIMA_EMISSAO_VALIDO")
	private boolean cnhDataUltimaEmissaoValido;

	@Column(name="VAL_CNH_DATA_VALIDADE_VALIDO")
	private boolean cnhDataValidadeValido;

	@Column(name="VAL_CNH_ESTRANGEIRO_VALIDO")
	private boolean cnhEstrangeiroValido;

	@Column(name="VAL_CNH_NOME_SIMILARIDADE")
	private float cnhNomeSimilaridade;

	@Column(name="VAL_CNH_NOME_VALIDO")
	private boolean cnhNomeValido;

	@Column(name="VAL_CNH_NUMERO_REGISTRO_VALIDO")
	private boolean cnhNumeroRegistroValido;

	@Column(name="VAL_CNH_SITUACAO_VALIDO")
	private boolean cnhSituacaoValido;

	@Column(name="VAL_CPF_DISPONIVEL")
	private boolean cpfDisponivel;

	@Column(name="VAL_CPF_SITUACAO")
	private boolean cpfSituacao;

	@Column(name="VAL_DATA_VALIDACAO")
	private LocalDateTime dataValidacao;

	@Column(name="VAL_ENDERECO_BAIRRO_SIMILARIDADE")
	private float enderecoBairroSimilaridade;

	@Column(name="VAL_ENDERECO_BAIRRO_VALIDO")
	private boolean enderecoBairroValido;

	@Column(name="VAL_ENDERECO_CEP_VALIDO")
	private boolean enderecoCepValido;

	@Column(name="VAL_ENDERECO_COMPLEMENTO_SIMILARIDADE")
	private float enderecoComplementoSimilaridade;

	@Column(name="VAL_ENDERECO_COMPLEMENTO_VALIDO")
	private boolean enderecoComplementoValido;

	@Column(name="VAL_ENDERECO_LOGRADOURO_SIMILARIDADE")
	private float enderecoLogradouroSimilaridade;

	@Column(name="VAL_ENDERECO_LOGRADOURO_VALIDO")
	private boolean enderecoLogradouroValido;

	@Column(name="VAL_ENDERECO_MUNICIPIO_SIMILARIDADE")
	private float valEnderecoMunicipioSimilaridade;

	@Column(name="VAL_ENDERECO_MUNICIPIO_VALIDO")
	private boolean enderecoMunicipioValido;

	@Column(name="VAL_ENDERECO_NUMERO_SIMILARIDADE")
	private float enderecoNumeroSimilaridade;

	@Column(name="VAL_ENDERECO_NUMERO_VALIDO")
	private int enderecoNumeroValido;

	@Column(name="VAL_ENDERECO_UF_VALIDO")
	private boolean enderecoUfValido;

	@Column(name="VAL_NACIONALIDADE_VALIDO")
	private boolean nacionalidadeValido;

	@Column(name="VAL_NOME_MAE_SIMILARIDADE")
	private float nomeMaeSimilaridade;

	@Column(name="VAL_NOME_MAE_VALIDO")
	private boolean nomeMaeValido;

	@Column(name="VAL_NOME_PAI_SIMILARIDADE")
	private float valNomePaiSimilaridade;

	@Column(name="VAL_NOME_PAI_VALIDO")
	private boolean nomePaiValido;

	@Column(name="VAL_NOME_SIMILARIDADE")
	private float nomeSimilaridade;

	@Column(name="VAL_NOME_VALIDO")
	private boolean nomeValido;

	@Column(name="VAL_PES_CPF_SIMILARIDADE")
	private float valPesCpfSimilaridade;

	@Column(name="VAL_PES_CPF_VALIDO")
	private boolean pesCpfValido;

	@Column(name="VAL_PES_DTNASCIMENTO_VALIDO")
	private int pesDtnascimentoValido;

	@Column(name="VAL_PES_ORGAO_EXPEDIDOR_RG_VALIDO")
	private boolean pesOrgaoExpedidorRgValido;

	@Column(name="VAL_PES_RG_SIMILARIDADE")
	private float pesRgSimilaridade;

	@Column(name="VAL_PES_RG_VALIDO")
	private boolean valPesRgValido;

	@Column(name="VAL_PES_UFE_ORGAO_EXPEDIDOR_RG_VALIDO")
	private boolean pesUfeOrgaoExpedidorRgValido;

	@Column(name="VAL_RESULTADO")
	private String resultado;

	@Column(name="VAL_SEXO_VALIDO")
	private boolean sexoValido;
	
	@Column(name="VAL_ATUAL")
	private boolean atual;
	
	@Column(name="VAL_BIOMETRIA_FACIAL_PROBABILIDADE")
	private String biometria_facial_probabilidade;
	
	@Column(name="VAL_BIOMETRIA_FACIAL_DISPONIVEL")
	private boolean biometria_facial_disponivel;
	
	@Column(name="VAL_BIOMETRIA_FACIAL_SIMILARIDADE")
	private float biometria_facial_similaridade;

	public ValidacaoPessoa() {
	}
	

	public Integer getCodigo() {
		return codigo;
	}




	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}




	public String getBiometria_facial_probabilidade() {
		return biometria_facial_probabilidade;
	}


	public void setBiometria_facial_probabilidade(String biometria_facial_probabilidade) {
		this.biometria_facial_probabilidade = biometria_facial_probabilidade;
	}


	public boolean isBiometria_facial_disponivel() {
		return biometria_facial_disponivel;
	}


	public void setBiometria_facial_disponivel(boolean biometria_facial_disponivel) {
		this.biometria_facial_disponivel = biometria_facial_disponivel;
	}



	public float getBiometria_facial_similaridade() {
		return biometria_facial_similaridade;
	}


	public void setBiometria_facial_similaridade(float biometria_facial_similaridade) {
		this.biometria_facial_similaridade = biometria_facial_similaridade;
	}

	
	public boolean isFaceValida() {
		return this.biometria_facial_similaridade >= 0.92;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}


	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}


	public boolean isCnhCategoriaValido() {
		return cnhCategoriaValido;
	}


	public void setCnhCategoriaValido(boolean cnhCategoriaValido) {
		this.cnhCategoriaValido = cnhCategoriaValido;
	}


	public boolean isCnhDataPrimeiraValido() {
		return cnhDataPrimeiraValido;
	}


	public void setCnhDataPrimeiraValido(boolean cnhDataPrimeiraValido) {
		this.cnhDataPrimeiraValido = cnhDataPrimeiraValido;
	}


	public boolean isCnhDataUltimaEmissaoValido() {
		return cnhDataUltimaEmissaoValido;
	}


	public void setCnhDataUltimaEmissaoValido(boolean cnhDataUltimaEmissaoValido) {
		this.cnhDataUltimaEmissaoValido = cnhDataUltimaEmissaoValido;
	}


	public boolean isCnhDataValidadeValido() {
		return cnhDataValidadeValido;
	}


	public void setCnhDataValidadeValido(boolean cnhDataValidadeValido) {
		this.cnhDataValidadeValido = cnhDataValidadeValido;
	}


	public boolean isCnhEstrangeiroValido() {
		return cnhEstrangeiroValido;
	}


	public void setCnhEstrangeiroValido(boolean cnhEstrangeiroValido) {
		this.cnhEstrangeiroValido = cnhEstrangeiroValido;
	}


	public float getCnhNomeSimilaridade() {
		return cnhNomeSimilaridade;
	}


	public void setCnhNomeSimilaridade(float cnhNomeSimilaridade) {
		this.cnhNomeSimilaridade = cnhNomeSimilaridade;
	}


	public boolean isCnhNomeValido() {
		return cnhNomeValido;
	}


	public void setCnhNomeValido(boolean cnhNomeValido) {
		this.cnhNomeValido = cnhNomeValido;
	}


	public boolean isCnhNumeroRegistroValido() {
		return cnhNumeroRegistroValido;
	}


	public void setCnhNumeroRegistroValido(boolean cnhNumeroRegistroValido) {
		this.cnhNumeroRegistroValido = cnhNumeroRegistroValido;
	}


	public boolean isCnhSituacaoValido() {
		return cnhSituacaoValido;
	}


	public void setCnhSituacaoValido(boolean cnhSituacaoValido) {
		this.cnhSituacaoValido = cnhSituacaoValido;
	}


	public boolean isCpfDisponivel() {
		return cpfDisponivel;
	}


	public void setCpfDisponivel(boolean cpfDisponivel) {
		this.cpfDisponivel = cpfDisponivel;
	}


	public boolean isCpfSituacao() {
		return cpfSituacao;
	}


	public void setCpfSituacao(boolean cpfSituacao) {
		this.cpfSituacao = cpfSituacao;
	}


	public LocalDateTime getDataValidacao() {
		return dataValidacao;
	}


	public void setDataValidacao(LocalDateTime dataValidacao) {
		this.dataValidacao = dataValidacao;
	}


	public float getEnderecoBairroSimilaridade() {
		return enderecoBairroSimilaridade;
	}


	public void setEnderecoBairroSimilaridade(float enderecoBairroSimilaridade) {
		this.enderecoBairroSimilaridade = enderecoBairroSimilaridade;
	}


	public boolean isEnderecoBairroValido() {
		return enderecoBairroValido;
	}


	public void setEnderecoBairroValido(boolean enderecoBairroValido) {
		this.enderecoBairroValido = enderecoBairroValido;
	}


	public boolean isEnderecoCepValido() {
		return enderecoCepValido;
	}


	public void setEnderecoCepValido(boolean enderecoCepValido) {
		this.enderecoCepValido = enderecoCepValido;
	}


	public float getEnderecoComplementoSimilaridade() {
		return enderecoComplementoSimilaridade;
	}


	public void setEnderecoComplementoSimilaridade(float enderecoComplementoSimilaridade) {
		this.enderecoComplementoSimilaridade = enderecoComplementoSimilaridade;
	}


	public boolean isEnderecoComplementoValido() {
		return enderecoComplementoValido;
	}


	public void setEnderecoComplementoValido(boolean enderecoComplementoValido) {
		this.enderecoComplementoValido = enderecoComplementoValido;
	}


	public float getEnderecoLogradouroSimilaridade() {
		return enderecoLogradouroSimilaridade;
	}


	public void setEnderecoLogradouroSimilaridade(float enderecoLogradouroSimilaridade) {
		this.enderecoLogradouroSimilaridade = enderecoLogradouroSimilaridade;
	}


	public boolean isEnderecoLogradouroValido() {
		return enderecoLogradouroValido;
	}


	public void setEnderecoLogradouroValido(boolean enderecoLogradouroValido) {
		this.enderecoLogradouroValido = enderecoLogradouroValido;
	}


	public float getValEnderecoMunicipioSimilaridade() {
		return valEnderecoMunicipioSimilaridade;
	}


	public void setValEnderecoMunicipioSimilaridade(float valEnderecoMunicipioSimilaridade) {
		this.valEnderecoMunicipioSimilaridade = valEnderecoMunicipioSimilaridade;
	}


	public boolean isEnderecoMunicipioValido() {
		return enderecoMunicipioValido;
	}


	public void setEnderecoMunicipioValido(boolean enderecoMunicipioValido) {
		this.enderecoMunicipioValido = enderecoMunicipioValido;
	}


	public float getEnderecoNumeroSimilaridade() {
		return enderecoNumeroSimilaridade;
	}


	public void setEnderecoNumeroSimilaridade(float enderecoNumeroSimilaridade) {
		this.enderecoNumeroSimilaridade = enderecoNumeroSimilaridade;
	}


	public int getEnderecoNumeroValido() {
		return enderecoNumeroValido;
	}


	public void setEnderecoNumeroValido(int enderecoNumeroValido) {
		this.enderecoNumeroValido = enderecoNumeroValido;
	}


	public boolean isEnderecoUfValido() {
		return enderecoUfValido;
	}


	public void setEnderecoUfValido(boolean enderecoUfValido) {
		this.enderecoUfValido = enderecoUfValido;
	}


	public boolean isNacionalidadeValido() {
		return nacionalidadeValido;
	}


	public void setNacionalidadeValido(boolean nacionalidadeValido) {
		this.nacionalidadeValido = nacionalidadeValido;
	}


	public float getNomeMaeSimilaridade() {
		return nomeMaeSimilaridade;
	}


	public void setNomeMaeSimilaridade(float nomeMaeSimilaridade) {
		this.nomeMaeSimilaridade = nomeMaeSimilaridade;
	}


	public boolean isNomeMaeValido() {
		return nomeMaeValido;
	}


	public void setNomeMaeValido(boolean nomeMaeValido) {
		this.nomeMaeValido = nomeMaeValido;
	}


	public float getValNomePaiSimilaridade() {
		return valNomePaiSimilaridade;
	}


	public void setValNomePaiSimilaridade(float valNomePaiSimilaridade) {
		this.valNomePaiSimilaridade = valNomePaiSimilaridade;
	}


	public boolean isNomePaiValido() {
		return nomePaiValido;
	}


	public void setNomePaiValido(boolean nomePaiValido) {
		this.nomePaiValido = nomePaiValido;
	}


	public float getNomeSimilaridade() {
		return nomeSimilaridade;
	}


	public void setNomeSimilaridade(float nomeSimilaridade) {
		this.nomeSimilaridade = nomeSimilaridade;
	}


	public boolean isNomeValido() {
		return nomeValido;
	}


	public void setNomeValido(boolean nomeValido) {
		this.nomeValido = nomeValido;
	}


	public float getValPesCpfSimilaridade() {
		return valPesCpfSimilaridade;
	}


	public void setValPesCpfSimilaridade(float valPesCpfSimilaridade) {
		this.valPesCpfSimilaridade = valPesCpfSimilaridade;
	}


	public boolean isPesCpfValido() {
		return pesCpfValido;
	}


	public void setPesCpfValido(boolean pesCpfValido) {
		this.pesCpfValido = pesCpfValido;
	}


	public int getPesDtnascimentoValido() {
		return pesDtnascimentoValido;
	}


	public void setPesDtnascimentoValido(int pesDtnascimentoValido) {
		this.pesDtnascimentoValido = pesDtnascimentoValido;
	}


	public boolean isPesOrgaoExpedidorRgValido() {
		return pesOrgaoExpedidorRgValido;
	}


	public void setPesOrgaoExpedidorRgValido(boolean pesOrgaoExpedidorRgValido) {
		this.pesOrgaoExpedidorRgValido = pesOrgaoExpedidorRgValido;
	}


	public float getPesRgSimilaridade() {
		return pesRgSimilaridade;
	}


	public void setPesRgSimilaridade(float pesRgSimilaridade) {
		this.pesRgSimilaridade = pesRgSimilaridade;
	}


	public boolean isValPesRgValido() {
		return valPesRgValido;
	}


	public void setValPesRgValido(boolean valPesRgValido) {
		this.valPesRgValido = valPesRgValido;
	}


	public boolean isPesUfeOrgaoExpedidorRgValido() {
		return pesUfeOrgaoExpedidorRgValido;
	}


	public void setPesUfeOrgaoExpedidorRgValido(boolean pesUfeOrgaoExpedidorRgValido) {
		this.pesUfeOrgaoExpedidorRgValido = pesUfeOrgaoExpedidorRgValido;
	}


	public String getResultado() {
		return resultado;
	}


	public void setResultado(String resultado) {
		this.resultado = resultado;
	}


	public boolean isSexoValido() {
		return sexoValido;
	}


	public void setSexoValido(boolean sexoValido) {
		this.sexoValido = sexoValido;
	}


	public boolean isAtual() {
		return atual;
	}


	public void setAtual(boolean atual) {
		this.atual = atual;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValidacaoPessoa other = (ValidacaoPessoa) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}


	public void converterRetorno(AnswerRetorno ret) {
		this.atual = true;
		this.nomeValido = ret.isNome();
		this.sexoValido = ret.isSexo();
		this.nomeMaeValido = ret.getFiliacao().isNome_mae();
		this.cnhCategoriaValido = ret.getCnh().isCategoria();
		this.cnhDataPrimeiraValido = ret.getCnh().isData_primeira_habilitacao();
		this.cnhDataUltimaEmissaoValido= ret.getCnh().isData_ultima_emissao();
		this.cnhDataValidadeValido= ret.getCnh().isData_validade();
		this.cnhSituacaoValido= ret.getCnh().isDescricao_situacao();
		this.cnhNomeSimilaridade= ret.getCnh().getNome_similiridade();
		this.biometria_facial_disponivel = ret.getBiometria_face().isDisponivel();
		this.biometria_facial_probabilidade = ret.getBiometria_face().getProbabilidade(); 
		this.biometria_facial_similaridade = ret.getBiometria_face().getSimilaridade();
		this.nomeSimilaridade = ret.getNomeSimilaridade();
		this.dataValidacao = LocalDateTime.now();
		this.enderecoCepValido =ret.getEndereco().isCep();
		this.sexoValido = ret.isSexo();
		this.cpfDisponivel = ret.isSituacao_cpf();
		this.cpfSituacao = ret.isSituacao_cpf();
		
		
	}

	

}