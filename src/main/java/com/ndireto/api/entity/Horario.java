package com.ndireto.api.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="horarios")
public class Horario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="hor_codigo")
	private Integer codigo;

	@Column(name="hor_hora")
	private String hora;


	@Transient
	private boolean manha;
	
	@Transient
	private boolean tarde;
	
	@Transient
	private boolean noite;
	
	
	/**
	 * VERIFICADO DE ACORDO COM CODIGO DA HORA NO BANCO
	 * CUIDADO COM ALTERAÇÃO DO BANCO
	 * @return
	 */
	public boolean isManha() {
		return this.codigo<=12;
	}

	public void setManha(boolean manha) {
		this.manha = manha;
	}

	/**
	 * VERIFICADO DE ACORDO COM CODIGO DA HORA NO BANCO
	 * CUIDADO COM ALTERAÇÃO DO BANCO
	 * @return
	 */
	public boolean isTarde() {
		return  (this.codigo>12 && this.codigo<=25) ;
	}

	public void setTarde(boolean tarde) {
		this.tarde = tarde;
	}

	/**
	 * VERIFICADO DE ACORDO COM CODIGO DA HORA NO BANCO
	 * CUIDADO COM ALTERAÇÃO DO BANCO
	 * @return
	 */
	public boolean isNoite() {
		return this.codigo>25;
	}

	public void setNoite(boolean noite) {
		this.noite = noite;
	}
	
	
	public Horario() {
	}
	
	
	public Integer getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Horario other = (Horario) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Horario [codigo=" + codigo + ", hora=" + hora + ", manha=" + manha + ", tarde=" + tarde + ", noite="
				+ noite + "]";
	}

	


}