package com.ndireto.api.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="HISTORICOFINANCEIRO")
public class HistoricoFinanceiro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="HIF_CODIGO",unique=true, nullable=false)
	private int id;
	
	@JsonIgnore
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="PES_CODIGO")
	private Pessoa pessoa;

	
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="THF_CODIGO")
	private TipoHistoricoFinanceiro tipoHistoricoFinanceiro;
	
	@JsonIgnore
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="PES_CODIGOUSUARIO")
	private Pessoa codigoUsuario;
	
	@JsonIgnore
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="VIS_CODIGO")
	private Visita visita;
	
	@Column(name="HIF_VALOR")
	private Float valor;
	
	@Column(name="HIF_DTMOVIMENTACAO")
	private LocalDateTime dataMovimentacao;
	
	
	@Column(name="HFI_OBSERVACAO")
	private String observacao;
	
	@Column(name="HIF_DATACONCILIACAO")
	private LocalDateTime dataConciliacao;
	
	
	@Column(name="HIF_DATAVISUALIZACAO")
	private LocalDateTime dataVisualizacao;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="PES_CODIGOUSUARIO_CONCILIACAO")
	private Pessoa codigoUsuarioConciliacao;
	
	
	
	
	public LocalDateTime getDataConciliacao() {
		return dataConciliacao;
	}

	public void setDataConciliacao(LocalDateTime dataConciliacao) {
		this.dataConciliacao = dataConciliacao;
	}

	public LocalDateTime getDataVisualizacao() {
		return dataVisualizacao;
	}

	public void setDataVisualizacao(LocalDateTime dataVisualizacao) {
		this.dataVisualizacao = dataVisualizacao;
	}

	public Pessoa getCodigoUsuarioConciliacao() {
		return codigoUsuarioConciliacao;
	}

	public void setCodigoUsuarioConciliacao(Pessoa codigoUsuarioConciliacao) {
		this.codigoUsuarioConciliacao = codigoUsuarioConciliacao;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public TipoHistoricoFinanceiro getTipoHistoricoFinanceiro() {
		return tipoHistoricoFinanceiro;
	}

	public void setTipoHistoricoFinanceiro(TipoHistoricoFinanceiro tipoHistoricoFinanceiro) {
		this.tipoHistoricoFinanceiro = tipoHistoricoFinanceiro;
	}

	public Pessoa getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(Pessoa codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	
	

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Visita getVisita() {
		return visita;
	}

	public void setVisita(Visita visita) {
		this.visita = visita;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}
	
	


	public LocalDateTime getDataMovimentacao() {
		return dataMovimentacao;
	}

	public void setDataMovimentacao(LocalDateTime dataMovimentacao) {
		this.dataMovimentacao = dataMovimentacao;
	}
	
	
	public String getSinal() {
		
		String retorno = "X";
		
		if(tipoHistoricoFinanceiro.getNome().trim().equals("Débito")) {
			retorno = "<strong style='color:red; font-size:16px;'>-</strong>";
		}
		else if(tipoHistoricoFinanceiro.getNome().trim().equals("Crédito")) {
			retorno = "<strong style='color:green; font-size:16px;'>+</strong>";
		}
		
		return retorno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoFinanceiro other = (HistoricoFinanceiro) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	
}
