package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.ndireto.api.dto.BairroDTO;

/**
 * @author Cyd
 *
 *
 */
@Entity
@Table(name = "BAIRRO")
public class Bairro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BAI_CODIGO")
    private int id;
    
    @Size(max = 255)
    @Column(name = "BAI_NOME")
    private String nome;
    
    @ManyToOne
	@JoinColumn(name="CID_CODIGO")
    @NotNull(message = "Cidade é obrigatória")
    private Cidade cidade;
    

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public boolean isNovo(){
		return this.id ==0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bairro other = (Bairro) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public BairroDTO fromDTO() {
		BairroDTO dto = new BairroDTO();
		
		dto.setCodigo(this.getId());
		dto.setNome(this.getNome());
		dto.setCidade(this.getCidade().fromDTO());
		
		
		return dto;
	}

	
	
	
}
