package com.ndireto.api.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="disponibilidade_visita_imovel")
public class DisponibilidadeVisitaImovel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="dhi_codigo")
	private int codigo;

	//bi-directional many-to-one association to DiasSemana
	@ManyToOne
	@JoinColumn(name="dse_Codigo")
	private DiasSemana diasSemana;

	//bi-directional many-to-one association to Horario
	@ManyToOne
	@JoinColumn(name="hor_codigo")
	private Horario horario;
	
	//bi-directional many-to-one association to Ufe
	@ManyToOne
	@JoinColumn(name="IMO_CODIGO")
	private Imovel imovel;

	public DisponibilidadeVisitaImovel() {
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public DiasSemana getDiasSemana() {
		return this.diasSemana;
	}

	public void setDiasSemana(DiasSemana diasSemana) {
		this.diasSemana = diasSemana;
	}

	public Horario getHorario() {
		return this.horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}
	
	

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DisponibilidadeVisitaImovel other = (DisponibilidadeVisitaImovel) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}
	

}