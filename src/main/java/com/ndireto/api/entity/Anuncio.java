package com.ndireto.api.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ANUNCIO")
public class Anuncio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ANU_CODIGO",unique=true, nullable=false)
	private int id;

	@ManyToOne
	@JoinColumn(name="PES_CODIGO_CADASTRANTE")
	private Pessoa pessoaCadastrante;
	
	@Column(name="ANU_DTCADASTRO")
	private LocalDateTime dataCadastro;
	
	@Column(name="ANU_DTANUNCIO")
	private LocalDateTime dataAnuncio;
	
	@Column(name="ANU_DTEXPIRACAO")
	private LocalDateTime dataExpiracao;
	
	@Column(name="ANU_LINK")
	private String link;
	
	@ManyToOne
	@JoinColumn(name="IMO_CODIGO")
	private Imovel imovel;
	
	@ManyToOne
	@JoinColumn(name="TAN_CODIGO")
	private TipoAnuncio tipoAnuncio;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pessoa getPessoaCadastrante() {
		return pessoaCadastrante;
	}

	public void setPessoaCadastrante(Pessoa pessoaCadastrante) {
		this.pessoaCadastrante = pessoaCadastrante;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalDateTime getDataAnuncio() {
		return dataAnuncio;
	}

	public void setDataAnuncio(LocalDateTime dataAnuncio) {
		this.dataAnuncio = dataAnuncio;
	}

	public LocalDateTime getDataExpiracao() {
		return dataExpiracao;
	}

	public void setDataExpiracao(LocalDateTime dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	public TipoAnuncio getTipoAnuncio() {
		return tipoAnuncio;
	}

	public void setTipoAnuncio(TipoAnuncio tipoAnuncio) {
		this.tipoAnuncio = tipoAnuncio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Anuncio other = (Anuncio) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
