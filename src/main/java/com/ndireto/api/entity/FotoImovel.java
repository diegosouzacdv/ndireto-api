package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.thymeleaf.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="FOTOIMOVEL")
public class FotoImovel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="FIM_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="FIM_ATIVO")
	private int ativo;

	@Column(name="FIM_CONTENT_TYPE")
	private String contentType;

	@Column(name="FIM_NOMEFOTO")
	private String nomeFoto;
	
	
	@Column(name="FIM_DESCRICAO")
	private String descricao;

	//bi-directional many-to-one association to Pessoa
	@ManyToOne
	@JoinColumn(name="IMO_CODIGO")
	@JsonIgnore
	private Imovel imovel;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getNomeFoto() {
		return nomeFoto;
	}

	public void setNomeFoto(String nomeFoto) {
		this.nomeFoto = nomeFoto;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}
	
	
	

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@JsonIgnore
	public String getTipoReduzido(){
		if(this.nomeFoto == null || StringUtils.isEmpty(this.nomeFoto)) {
			return "jpg";
		}
		String arrayNome[] = this.nomeFoto.split("\\."); 
		return arrayNome[1];

	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FotoImovel other = (FotoImovel) obj;
		if (id != other.id)
			return false;
		return true;
	}

	

}