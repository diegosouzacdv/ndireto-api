
package com.ndireto.api.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;
import org.thymeleaf.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ndireto.api.dto.AssociadoDTO;
import com.ndireto.api.dto.EnderecoPessoaDTO;
import com.ndireto.api.dto.PessoaDTO;
import com.ndireto.api.enumeration.TipoPessoaImovelEnum;

@Entity
@Table(name="PESSOA")
public class Pessoa implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PES_CODIGO",unique=true, nullable=false)
	private int id;

//	@NotBlank(message="Nome é obrigatório")
	@Column(name="PES_NOME")
	private String nome;
	
	@Column(name="PES_NOME_MAE")
	private String nomeMae;
	
	@Column(name="PES_NOME_PAI")
	private String nomePai;
	
	@Column(name="PES_SENHA")
	private String senha;
	
	@Column(name="PES_LOCALIMOVEL")
	private String localImovel;
	
	@Column(name="PES_HORARIO")
	private String horario;
	
//	@ManyToOne
//	@JoinColumn(name="TIP_CODIGO")
//	private TipoPessoa tipoPessoa;
	
	@Column(name="PES_CPF")
	private String cpf;
	
	@Column(name = "PES_DTNASCIMENTO")
//	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private LocalDate dataNascimento;
	
	@Column(name="PES_EMAIL")
	private String email;
	
	@Column(name="PES_SEXO")
	private String sexo;
	
	
	@Column(name="PES_CELULAR")
	private String celular;
	
	@Column(name="PES_FIXO")
	private String fixo;
	
	@Column(name = "PES_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	@Column(name="PES_CRECI")
	private String creci;
	
	@Column(name="PES_TOKEN")
	private String token;
	
	@Column(name="PES_UUID")
	private String UUID;
	
	@Column(name="PES_RG")
	private String rg;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="PES_UFE_ORGAO_EXPEDIDOR_RG")
	private Ufe rgUfExpedicao;
	
	
	@Column(name="PES_ORGAO_EXPEDIDOR_RG")
	private String rgOrgaoExpedidor;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="UFE_CODIGOCRECI")
	private Ufe ufeCreci;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="STP_CODIGO")
	private StatusPessoa statusPessoa;
	
	@JsonIgnore
	@OneToMany( mappedBy = "proprietario", fetch = FetchType.LAZY)
	private List<Imovel> imoveis;
	
	@JsonIgnore
	@OneToMany( mappedBy = "pessoa", fetch = FetchType.LAZY)
	private List<HistoricoFinanceiro> historicoFinanceiro;

	
	@JsonIgnore
	@OneToMany(mappedBy="pessoa")
	private List<PessoaTipoPessoa> listPessoasTipos;
	
	@JsonIgnore
	@OneToMany(mappedBy="pessoa")
	private List<Endereco> enderecos;
	
	@JsonIgnore
	@OneToMany(mappedBy="pessoa")
	private List<Conta> contas;
	
	
	@JsonIgnore
	@OneToMany(mappedBy="pessoa")
	private List<ValidacaoPessoa> validacoes;
	
	
	@Column(name="PES_NOMEFOTO")
	private String nomeFoto;
	
	@Column(name="PES_CONTENT_TYPE")
	private String contentType;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="PES_CODIGO_INDICADOR")
	private Pessoa pessoaIndicador;
	
	
	@Column(name="PES_DATACADASTRO")
	private LocalDateTime dataCadastro;
	
	
	@Column(name="PES_DATAACEITACAO_CONTRATO")
	private LocalDateTime dataAceitacaoContrato;
	
	@Column(name="PES_OPORTUNIDADES")
	private Integer oportunidade;
	
	
	@JsonIgnore
	@OneToMany(mappedBy="pessoa")
	private List<TokenFirebase> listTokenFirebase;
	
	@Transient
	private Endereco endereco;
	
	@Transient
	private boolean gestor = false;
	
	@Transient
	private boolean funcionarioAdministrativo = false;
	
	@Transient
	private boolean fotografo = false;
	
	@Transient
	private boolean corretor = false;
	
	@Transient
	private boolean comprador = false;
	
	@Transient
	private boolean associado = false;
	
	
	public Pessoa() {
		super();
	}

	
	public List<TokenFirebase> getListTokenFirebase() {
		return listTokenFirebase;
	}

	public void setListTokenFirebase(List<TokenFirebase> listTokenFirebase) {
		this.listTokenFirebase = listTokenFirebase;
	}
	public String getHorario() {
		return horario;
	}
	
	public String getLocalImovel() {
		return localImovel;
	}
	
	public void setLocalImovel(String localImovel) {
		this.localImovel = localImovel;
	}
	
	public void setHorario(String horario) {
		this.horario = horario;
	}
	
	public Integer getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}
	
	public Pessoa(Integer id, String nome, String cpf, String email, LocalDate dataNascimento, String senha) {
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.dataNascimento = dataNascimento;
		this.senha = senha;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
	public List<Conta> getContas() {
		return contas;
	}

	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}

	public String getNomeFoto() {
		return nomeFoto;
	}

	public void setNomeFoto(String nomeFoto) {
		this.nomeFoto = nomeFoto;
	}

	public String getContentType() {
		return contentType;
	}

	
	public LocalDateTime getDataAceitacaoContrato() {
		return dataAceitacaoContrato;
	}

	public void setDataAceitacaoContrato(LocalDateTime dataAceitacaoContrato) {
		this.dataAceitacaoContrato = dataAceitacaoContrato;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfSemMascara(){
		return  this.cpf!=null?this.cpf.trim().replaceAll("\\.|-|/", ""):null;
	}	
	
	
	@PrePersist @PreUpdate
	private void prePersistPreUpdate() {
		this.cpf = (this.cpf!=null?this.cpf.trim().replaceAll("\\.|-|/", ""):null);
	}
	
	
	@PostLoad
	private void postLoad() {
		this.cpf = (this.cpf!=null?this.cpf.replaceAll("(\\d{3})(\\d{3})(\\d{3})", "$1.$2.$3-").trim():null); 
		
		for (PessoaTipoPessoa pessoaTipoPessoa : this.getListPessoasTipos()) {
			
			if(pessoaTipoPessoa.getTipoPessoa()!=null) {
			if(pessoaTipoPessoa.getTipoPessoa().getId() == TipoPessoaImovelEnum.CORRETOR.getId()){
				corretor = true;
			}
			
			if(pessoaTipoPessoa.getTipoPessoa().getId() == TipoPessoaImovelEnum.GESTOR.getId()){
				gestor = true;
			}
			
			if(pessoaTipoPessoa.getTipoPessoa().getId() == TipoPessoaImovelEnum.FUNCIONARIOADMINISTRATIVO.getId()){
				funcionarioAdministrativo = true;
			}
			
			if(pessoaTipoPessoa.getTipoPessoa().getId() == TipoPessoaImovelEnum.FOTOGRAFO.getId()){
				fotografo = true;
			}
			
			if(pessoaTipoPessoa.getTipoPessoa().getId() == TipoPessoaImovelEnum.ASSOCIADO.getId()){
				associado = true;
			}
			
			if(pessoaTipoPessoa.getTipoPessoa().getId() == TipoPessoaImovelEnum.COMPRADOR.getId()){
				comprador = true;
			}
			}
		}
	}

	
	public boolean isNova() {
		
		return this.id==0;
		
	}
	
	public boolean isGestor() {
		return gestor;
	}

	public void setGestor(boolean gestor) {
		this.gestor = gestor;
	}

	public boolean isFuncionarioAdministrativo() {
		return funcionarioAdministrativo;
	}

	public void setFuncionarioAdministrativo(boolean funcionarioAdministrativo) {
		this.funcionarioAdministrativo = funcionarioAdministrativo;
	}

	public boolean isFotografo() {
		return fotografo;
	}

	public void setFotografo(boolean fotografo) {
		this.fotografo = fotografo;
	}

	public boolean isCorretor() {
		return corretor;
	}

	public void setCorretor(boolean corretor) {
		this.corretor = corretor;
	}

	public boolean isComprador() {
		return comprador;
	}

	public void setComprador(boolean comprador) {
		this.comprador = comprador;
	}

	public boolean isAssociado() {
		return associado;
	}

	public void setAssociado(boolean associado) {
		this.associado = associado;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	public String getEndereco() {
//		return endereco;
//	}
//
//	public void setEndereco(String endereco) {
//		this.endereco = endereco;
//	}

	public StatusPessoa getStatusPessoa() {
		return statusPessoa;
	}

	public void setStatusPessoa(StatusPessoa statusPessoa) {
		this.statusPessoa = statusPessoa;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}


	public String getFixo() {
		return fixo;
	}

	public void setFixo(String fixo) {
		this.fixo = fixo;
	}
	


	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
		
	public String getTelefones() { 
		
		return ((this.fixo==null?"":this.fixo)+(this.celular==null?"":" / "+this.celular));
	}
	
	public String getCreci() {
		return creci;
	}

	public void setCreci(String creci) {
		this.creci = creci;
	}
	
	
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Ufe getUfeCreci() {
		return ufeCreci;
	}

	public void setUfeCreci(Ufe ufeCreci) {
		this.ufeCreci = ufeCreci;
	}
	
	
	
	
	public List<PessoaTipoPessoa> getListPessoasTipos() {
		return listPessoasTipos;
	}

	public void setListPessoasTipos(List<PessoaTipoPessoa> listPessoasTipos) {
		this.listPessoasTipos = listPessoasTipos;
	}
	
	public Integer getOportunidade() {
		return oportunidade;
	}
	
	public void setOportunidade(Integer oportunidade) {
		this.oportunidade = oportunidade;
	}
	
	public Pessoa getPessoaIndicador() {
		return pessoaIndicador;
	}

	public void setPessoaIndicador(Pessoa pessoaIndicador) {
		this.pessoaIndicador = pessoaIndicador;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	
	
	public List<Imovel> getImoveis() {
		return imoveis;
	}

	public void setImoveis(List<Imovel> imoveis) {
		this.imoveis = imoveis;
	}


	public List<HistoricoFinanceiro> getHistoricoFinanceiro() {
		return historicoFinanceiro;
	}

	public void setHistoricoFinanceiro(List<HistoricoFinanceiro> historicoFinanceiro) {
		this.historicoFinanceiro = historicoFinanceiro;
	}
	

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getNomePai() {
		return nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Ufe getRgUfExpedicao() {
		return rgUfExpedicao;
	}

	public void setRgUfExpedicao(Ufe rgUfExpedicao) {
		this.rgUfExpedicao = rgUfExpedicao;
	}

	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}

	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}
			
			
			

	public List<ValidacaoPessoa> getValidacoes() {
		return validacoes;
	}

	public void setValidacoes(List<ValidacaoPessoa> validacoes) {
		this.validacoes = validacoes;
	}
	

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}
	
		public String getTelefoneZap() { 
		
		return (this.celular==null?"":this.celular).replace(" ","").replaceAll("-","").replace("(", "").replace(")", "");
	}

	@JsonIgnore
	public Conta getConta() {
		Conta retorno = new Conta();
		
		if(contas!=null && !contas.isEmpty()) {
			for (Conta obj : contas) {
				retorno = obj;
			}
		}
		
		return retorno;
	}
	
	@JsonIgnore
	public Endereco getEndereco() {
		Endereco retorno = new Endereco();
		
		if(enderecos!=null && !enderecos.isEmpty()) {
			for (Endereco obj : enderecos) {
				retorno = obj;
			}
		}
		
		return retorno;
	}
	
	@JsonIgnore
	public int getNegociosFechados(){
		int qtde = 0;
		
		for (Imovel obj : this.getImoveis()) {
			if(obj.getStatusImovel().getId() == 6 || obj.getStatusImovel().getId() == 7){
				qtde++;
			}
		}
		
		return qtde;
	}
	
	
	@JsonIgnore
	public List<Visita> getCompromissos(){
		
		List<Visita> retorno = new ArrayList<Visita>();
		for (Imovel obj : this.getImoveis()) {
			
			retorno.addAll(obj.getVisitas());
			
		}
		
		return retorno;
	}

	@JsonIgnore
	public String getTipoPessoaFormatado(){
		String retorno = "";
		if(listPessoasTipos!=null){
			for (PessoaTipoPessoa pessoaTipoPessoa : listPessoasTipos) {
				retorno += pessoaTipoPessoa.getTipoPessoa().getNome()+"/ ";
			}
		}
		return retorno;
	}
	
	@JsonIgnore
	public List<TipoPessoa> getTipoPessoa(){
		List<TipoPessoa> listTipoPessoa = new ArrayList<TipoPessoa>();
		
		if(listPessoasTipos!=null){
			for (PessoaTipoPessoa pessoaTipoPessoa : listPessoasTipos) {
				listTipoPessoa.add(pessoaTipoPessoa.getTipoPessoa());
			}
		}
		
		return listTipoPessoa;
	}
	
	@JsonIgnore
	public String getTipoReduzido(){
		
		String retorno ="png";
		if(this.nomeFoto != null || !StringUtils.isEmpty(this.nomeFoto.trim())) {
			String arrayNome[] = this.nomeFoto.split("\\."); 
			if(arrayNome.length>1) {
				retorno =  arrayNome[1];
			}
		}
		
		return retorno;

	}
	
	@JsonIgnore
	public PessoaDTO fromDTO() {
		System.err.println(this.getNome());
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
		PessoaDTO dto = new PessoaDTO(this.id, this.getNome(), this.getCpf(), this.getDataNascimento()==null?null:this.getDataNascimento().format(formatter), this.email, this.getCelular(),this.getEndereco().getId()==0?new EnderecoPessoaDTO():this.getEndereco().fromDTO(), this.getToken(), this.getConta());//		dto.setId(this.id);
//		dto.setNome(this.getNome());
//		dto.setDataNascimento(this.getDataNascimento()==null?null:this.getDataNascimento().format(formatter));
//		dto.setNome(this.getNomeMae());
//		dto.setCelular(this.getCelular());
//		dto.setNomePai(this.getNomePai());
//		dto.setCpf(this.getCpf());
//		dto.setEmail(this.email);
//		dto.setEndereco(this.endereco==null?new EnderecoPessoaDTO():this.endereco.fromDTO());
		
		
		return dto;
	}
	
	
	public boolean isValido() {
		boolean retorno = false;
		if(validacoes!=null) {
			for (ValidacaoPessoa obj : validacoes) {
				if(obj.isAtual()) {
					if((obj.getBiometria_facial_similaridade()>0.92) && (obj.isCpfSituacao()) && (obj.isNomeValido())) {
						retorno = true;
					}
				}
			}
		}
		
		return retorno;
		
	}
	
	public ValidacaoPessoa getDadosValidacao() {
		ValidacaoPessoa retorno = new ValidacaoPessoa();
		if(validacoes!=null) {
			for (ValidacaoPessoa obj : validacoes) {
				if(obj.isAtual()) {
						retorno = obj;
				}
			}
		}
		
		return retorno;
		
	}
	
	public void fromAssociadoDTO(AssociadoDTO associadosDTO) {
		this.setNome(associadosDTO.getNome());
		this.setEmail(associadosDTO.getEmail());
		this.setCelular(associadosDTO.getTelefone());
		//this.setOportunidade(associadosDTO.isInformacoes()?1:0);
		
	}

	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (id != other.id)
			return false;
		return true;
	}


	

	
	
}
