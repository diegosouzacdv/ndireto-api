package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the imovel database table.
 * 
 */
@Entity
@Table(name="CONTA")
public class Conta implements Serializable { 
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COT_CODIGO",unique=true, nullable=false) 
	private int id;
	
	@Column(name="COT_NR")
	private String numeroConta;
	
	@Column(name="COT_AGENCIA")
	private String agencia;
	
	@Column(name="COT_DIGITO")
	private String digito;
	
	//bi-directional many-to-one association to TipoImovel
	@ManyToOne
	@JoinColumn(name="BAN_CODIGO")
	private Banco banco;
	
	//bi-directional many-to-one association to TipoImovel
	@ManyToOne
	@JoinColumn(name="TIC_CODIGO")
	private TipoConta tipoConta;
	
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="PES_CODIGO")
	@JsonIgnore
	private Pessoa pessoa;

	public boolean isNova() {
		return this.id==0;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNumeroConta() {
		return numeroConta;
	}
	

	public String getDigito() {
		return digito;
	}

	public void setDigito(String digito) {
		this.digito = digito;
	}

	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}


	public Banco getBanco() {
		return banco;
	}


	public void setBanco(Banco banco) {
		this.banco = banco;
	}


	public TipoConta getTipoConta() {
		return tipoConta;
	}
	

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}


	public Pessoa getPessoa() {
		return pessoa;
	}


	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conta other = (Conta) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}