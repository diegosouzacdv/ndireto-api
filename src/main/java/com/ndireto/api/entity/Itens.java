package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ndireto.api.dto.CaracteristicaImovelDTO;


@Entity
@Table(name="ITENS")
public class Itens implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ITE_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="ITE_NOME")
	private String nome;
	
	//bi-directional many-to-one association to TipoImovel
	@ManyToOne
	@JoinColumn(name="TPI_CODIGO")
	private TipoItens tipoItens;
	
	@Column(name = "ITE_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	
	public boolean isNovo() {  
  		return id == 0;
  	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}
	
	public TipoItens getTipoItens() {
		return tipoItens;
	}

	public void setTipoItens(TipoItens tipoItens) {
		this.tipoItens = tipoItens;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Itens other = (Itens) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public CaracteristicaImovelDTO fromDTO() {
			CaracteristicaImovelDTO dto = new CaracteristicaImovelDTO();
			dto.setCodigoItem(this.getId());
			dto.setNome(this.getNome());
			dto.setStatus(this.isNovo());
			return dto;
	}

}