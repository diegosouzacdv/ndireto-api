package com.ndireto.api.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.NumberFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ndireto.api.dto.CaracteristicaImovelDTO;
import com.ndireto.api.dto.EnderecoImovelDTO;
import com.ndireto.api.dto.EstruturaImovelDTO;
import com.ndireto.api.dto.ImovelDTO;
import com.ndireto.api.dto.LocalizacaoDTO;


/**
 * The persistent class for the imovel database table.
 * 
 */
@Entity
@Table(name="IMOVEL")
public class Imovel implements Serializable { 
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IMO_CODIGO",unique=true, nullable=false) 
	private int id;
	
	//bi-directional many-to-one association to TipoImovel
	@ManyToOne
	@JoinColumn(name="TIM_CODIGO")
	private TipoImovel tipoImovel;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="PES_CODIGOCADASTRANTE")
	private Pessoa pessoaCadastrante;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="PES_CODIGO_PROPRIETARIO")
	private Pessoa proprietario;
	
	@ManyToOne
	@JoinColumn(name="TNE_CODIGO")
	private TipoNegociacao tipoNegociacao;
	
	@ManyToOne
	@JoinColumn(name="MOT_CODIGO")
	private MotivoVenda motivoVenda;
	
	@Transient
	private Caracteristicas caracteristica;
	
	@JsonIgnore
	@OneToMany(mappedBy="imovel")
	private List<Caracteristicas> caracteristicas;
	
	@JsonIgnore
	@OneToMany(mappedBy="imovel")
	private List<DisponibilidadeVisitaImovel> disponibilidadeVisitaImovel;
	
	@JsonIgnore
	@OneToMany(mappedBy="imovel")
	private List<FotoImovel> fotos;
	
	@JsonIgnore
	@OneToMany(mappedBy="imovel")
	private List<ImovelItens> imovelItens;
	
	
	@JsonIgnore
	@OneToMany(mappedBy="imovel")
	private List<Visita> visitas;
	
	
	@ManyToOne
	@JoinColumn(name="STI_CODIGO")
	private StatusImovel statusImovel;
	
	
	
	@Column(name="IMO_LATITUDE")
	private String latitude;
	
	@Column(name="IMO_LONGITUDE")
	private String longitude;
	
	@Column(name="IMO_DTCADASTRO")
	private LocalDateTime dataCadastro;
	
	@Column(name="IMO_QTDBANHEIROS")
	private int qtdBanheiros;
	
	@Column(name="IMO_QTDSUITES")
	private int qtdSuites;
	
	@Column(name="IMO_QTDQUARTOS")
	private int qtdQuartos;
	
	@Column(name="IMO_PAGINA_ATUAL")
	private int pagina;
	
	@NumberFormat(pattern = "#,###,###,###.##")
	@Column(name="IMO_VALOR")
	private BigDecimal valor;
	
	@NumberFormat(pattern = "#,###,###,###.##")
	@Column(name="IMO_VALORCONDOMINIO")
	private BigDecimal valorCondominio;
	
	@NumberFormat(pattern = "#,###,###,###.##")
	@Column(name="IMO_VALORIPTU")
	private BigDecimal valorIPTU;
	
	@Column(name="IMO_AREAUTIL")
	private String areautil;
	
	@Column(name="IMO_AREATOTAL")
	private String areatotal;
	
	@Column(name="IMO_VAGAS")
	private int vagas;
	
	@Column(name="IMO_POSICAO")
	private int posicao;

	@Column(name="IMO_ATIVO")
	private int ativo;
	
	@Column(name="IMO_TELEFONE_1")
	private String telefone1;
	
	@Column(name="IMO_TELEFONE_2")
	private String telefone2;
	
	@Column(name="IMO_PONTOS_POSITIVOS")
	private String pontosPositivos;
	
	@Column(name="IMO_PONTOS_NEGATIVOS")
	private String pontosNegativos;
	
	@Column(name="IMO_DESOCUPADO")
	private int desocupado;
	
	@Column(name="IMO_FOTO_CAPA")
	private String fotoCapa;
	
	@JsonIgnore
	@OneToMany(mappedBy = "imovel", fetch = FetchType.LAZY)
	private List<Endereco> listEndereco;
	
	
	@Transient
	private Endereco endereco;
	
	
	
	@Transient
	private FotoImovel fotoImovel;
	
	@Transient
	private String uuid;
	
	
	@Transient
	private boolean disponivelManha;
	
	@Transient
	private boolean disponivelTarde;
	
	@Transient
	private boolean disponivelNoite;
	
	
	@Transient
	public boolean isNovo() {  
  		return id == 0;
  	}
	
	public int getId() {
		return id;
	}
	
	public List<Visita> getVisitas() {
		return visitas;
	}

	public void setVisitas(List<Visita> visitas) {
		this.visitas = visitas;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TipoImovel getTipoImovel() {
		return tipoImovel;
	}

	public void setTipoImovel(TipoImovel tipoImovel) {
		this.tipoImovel = tipoImovel;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public BigDecimal getValor() {
		
		valor = (valor==null?BigDecimal.ZERO:valor);
		
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public List<ImovelItens> getImovelItens() {
		return imovelItens;
	}

	public void setImovelItens(List<ImovelItens> imovelItens) {
		this.imovelItens = imovelItens;
	}

	@JsonIgnore
	public BigDecimal getValorTradicional(){
		float retorno = 0F;
		this.valor = (this.valor==null?BigDecimal.ZERO:this.valor);
		retorno = (5*this.valor.floatValue())/100; 
		
		return BigDecimal.valueOf(retorno);
	}
	
	
	public BigDecimal getValorTradicionalAluguel(){
		float retorno = 0F;
		this.valor = (this.valor==null?BigDecimal.ZERO:this.valor);
		retorno = (10*this.valor.floatValue())/100; 
		
		return BigDecimal.valueOf(retorno);
	}
	
	public BigDecimal getValorCondominio() {
		return valorCondominio;
	}

	public void setValorCondominio(BigDecimal valorCondominio) {
		this.valorCondominio = valorCondominio;
	}

	public BigDecimal getValorIPTU() {
		return valorIPTU;
	}

	public void setValorIPTU(BigDecimal valorIPTU) {
		this.valorIPTU = valorIPTU;
	}

	public int getQtdBanheiros() {
		return qtdBanheiros;
	}

	public void setQtdBanheiros(int qtdBanheiros) {
		this.qtdBanheiros = qtdBanheiros;
	}

	public int getQtdSuites() {
		return qtdSuites;
	}

	public void setQtdSuites(int qtdSuites) {
		this.qtdSuites = qtdSuites;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	public String getAreautil() {
		return areautil;
	}

	public void setAreautil(String areautil) {
		this.areautil = areautil;
	}

	public String getAreatotal() {
		return areatotal;
	}

	public void setAreatotal(String areatotal) {
		this.areatotal = areatotal;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Pessoa getPessoaCadastrante() {
		return pessoaCadastrante;
	}

	public void setPessoaCadastrante(Pessoa pessoaCadastrante) {
		this.pessoaCadastrante = pessoaCadastrante;
	}

	public TipoNegociacao getTipoNegociacao() {
		return tipoNegociacao;
	}

	public void setTipoNegociacao(TipoNegociacao tipoNegociacao) {
		this.tipoNegociacao = tipoNegociacao;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public int getQtdQuartos() {
		return qtdQuartos;
	}

	public void setQtdQuartos(int qtdQuartos) {
		this.qtdQuartos = qtdQuartos;
	}

	public int getVagas() {
		return vagas;
	}

	public void setVagas(int vagas) {
		this.vagas = vagas;
	}
	
	public int getPosicao() {
		return posicao;
	}

	public void setPosicao(int posicao) {
		this.posicao = posicao;
	}
	
	public List<Endereco> getListEndereco() {
		return listEndereco;
	}

	public void setListEndereco(List<Endereco> listEndereco) {
		this.listEndereco = listEndereco;
	}

	public int getDesocupado() {
		return desocupado;
	}

	public void setDesocupado(int desocupado) {
		this.desocupado = desocupado;
	}

	public String getEnderecoFormatado() {
		String retorno = "SEM ENDEREÇO";
		
		if(listEndereco!=null && !listEndereco.isEmpty()) {
			for (Endereco endereco : listEndereco) {
				retorno = endereco.getLogradouro().getNome()+" "+endereco.getComplemento() +" - "+endereco.getLogradouro().getBairro().getNome()+" - "+endereco.getLogradouro().getBairro().getCidade().getNome()+" - "+endereco.getLogradouro().getBairro().getCidade().getUfe().getSigla();
				
			}
		}
		
		return retorno;
	}
	
	public String getEnderecoBasicoFormatado() {
		String retorno = "SEM ENDEREÇO";
		
		if(listEndereco!=null && !listEndereco.isEmpty()) {
			for (Endereco endereco : listEndereco) {
				retorno = endereco.getLogradouro().getBairro().getNome()+" - "+endereco.getLogradouro().getBairro().getCidade().getUfe().getSigla();
				
			}
		}
		
		return retorno;
	}
	
	
//	public FotoImovel getFotoCapa() {
//		
//		
//		FotoImovel  retorno = new FotoImovel();
//		
//		if(fotos!=null && !fotos.isEmpty()) {
//			for (FotoImovel foto : fotos) {
//				retorno = foto;
//				break;
//			}
//		}
//		
//		return retorno;
//	}
	
	
	

	public Pessoa getProprietario() {
		return proprietario;
	}

	public void setProprietario(Pessoa proprietario) {
		this.proprietario = proprietario;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	

	public MotivoVenda getMotivoVenda() {
		return motivoVenda;
	}

	public void setMotivoVenda(MotivoVenda motivoVenda) {
		this.motivoVenda = motivoVenda;
	}

	public String getPontosPositivos() {
		return pontosPositivos;
	}

	public void setPontosPositivos(String pontosPositivos) {
		this.pontosPositivos = pontosPositivos;
	}

	public String getPontosNegativos() {
		return pontosNegativos;
	}

	public void setPontosNegativos(String pontosNegativos) {
		this.pontosNegativos = pontosNegativos;
	}
	
	
	public Caracteristicas getCaracteristica() {
		
		
		if(caracteristicas!=null && !caracteristicas.isEmpty()) {
			for (Caracteristicas obj : caracteristicas) {
				this.caracteristica = obj;
			}
		}
		
		return this.caracteristica;
	}
	
	

	public void setCaracteristica(Caracteristicas caracteristica) {
		this.caracteristica = caracteristica;
	}

	public List<Caracteristicas> getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(List<Caracteristicas> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public StatusImovel getStatusImovel() {
		return statusImovel;
	}

	public void setStatusImovel(StatusImovel statusImovel) {
		this.statusImovel = statusImovel;
	}

	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	//@JsonIgnore
	public Endereco getEndereco() {
		Endereco retorno = new Endereco();
		
		if(listEndereco!=null && !listEndereco.isEmpty()) {
			for (Endereco obj : listEndereco) {
				retorno = obj;
			}
		}
		
		return retorno.getId()==0?this.endereco:retorno;
	}

	public int getPagina() {
		return pagina;
	}

	public void setPagina(int pagina) {
		this.pagina = pagina;
	}
	
	

	public List<FotoImovel> getFotos() {
		return fotos;
	}

	public void setFotoCapa(String fotoCapa) {
		this.fotoCapa = fotoCapa;
	}
	

	public String getFotoCapa() {
		return fotoCapa;
	}

	public void setFotos(List<FotoImovel> fotos) {
		this.fotos = fotos;
	}
	
	
	public FotoImovel getFotoImovel() {
		return fotoImovel;
	}

	public void setFotoImovel(FotoImovel fotoImovel) {
		this.fotoImovel = fotoImovel;
	}

	
	
	

	public List<DisponibilidadeVisitaImovel> getDisponibilidadeVisitaImovel() {
		return disponibilidadeVisitaImovel;
	}

	public void setDisponibilidadeVisitaImovel(List<DisponibilidadeVisitaImovel> disponibilidadeVisitaImovel) {
		this.disponibilidadeVisitaImovel = disponibilidadeVisitaImovel;
	}
	
	public boolean isDisponivel(Integer dia,Integer hora) {
		
		if(this.disponibilidadeVisitaImovel!=null) {
		for (DisponibilidadeVisitaImovel obj : this.disponibilidadeVisitaImovel) {
			
			if(obj.getDiasSemana().getCodigo().equals(dia) 
					&& obj.getHorario().getCodigo().equals(hora)) {
				return true;
				
			}
		}
		}
		return false;
		
	}
	

	public boolean isDisponivelManha() {
		return disponivelManha;
	}

	public void setDisponivelManha(boolean disponivelManha) {
		this.disponivelManha = disponivelManha;
	}

	public boolean isDisponivelTarde() {
		return disponivelTarde;
	}

	public void setDisponivelTarde(boolean disponivelTarde) {
		this.disponivelTarde = disponivelTarde;
	}

	public boolean isDisponivelNoite() {
		return disponivelNoite;
	}

	public void setDisponivelNoite(boolean disponivelNoite) {
		this.disponivelNoite = disponivelNoite;
	}

	@JsonIgnore
	public ImovelDTO fromDTO() {
		
		
		ImovelDTO dto = new ImovelDTO();
		
		LocalizacaoDTO localizacao = new LocalizacaoDTO(this.getId(), this.latitude, this.longitude);
		
		List<CaracteristicaImovelDTO> caracteristicas = this.getCaracteristicasDTO();
		EstruturaImovelDTO estrutura = new EstruturaImovelDTO();
		
		estrutura.setCodigoImovel(this.getId());
		estrutura.setQtdBanheiros(this.getQtdBanheiros());
		estrutura.setQtdQuartos(this.getQtdQuartos());
		estrutura.setQtdSuites(this.getQtdSuites());
		
		List<String> fotos = new ArrayList<String>();
		
		if(this.fotos!=null && !this.fotos.isEmpty()) {
			for (FotoImovel obj : this.fotos) {
				fotos.add("/imagens/foto/"+obj.getId()+"/imovel");
			}
		}
		
		dto.setFotoCapa(this.getFotoCapa());
		dto.setId(this.getId());
		dto.setEndereco(this.getEndereco()==null?new EnderecoImovelDTO():this.getEndereco().fromImovelDTO());
		dto.setLocalizacao(localizacao);
		dto.setCaracteristicas(caracteristicas);
		dto.setEstrutura(estrutura);
		dto.setValor(valor);
		dto.setTipoImovel(tipoImovel == null ? " " : tipoImovel.getNome());
		dto.setTipoNegociacao(tipoNegociacao.getNome());
		dto.setStatusImovel(statusImovel == null ? " " : statusImovel.getNome());
		dto.setValorCondominio(valorCondominio);
		dto.setValorIPTU(valorIPTU);
		dto.setQtdBanheiros(qtdBanheiros);
		dto.setQtdQuartos(qtdQuartos);
		dto.setQtdSuites(qtdSuites);
		dto.setVagas(vagas);
		dto.setAreatotal(areatotal);
		dto.setAreautil(areautil);
		dto.setFotos(fotos);
		return dto;
	}
	
	
	private List<CaracteristicaImovelDTO> getCaracteristicasDTO() {
		List<CaracteristicaImovelDTO> dtos = new ArrayList<CaracteristicaImovelDTO>();
		
		for ( ImovelItens obj : this.getImovelItens()) {
			CaracteristicaImovelDTO dto = new CaracteristicaImovelDTO();
			dto.setCodigoItem(obj.getItens().getId());
			dto.setNome(obj.getItens().getNome());
			
			dtos.add(dto);
		}
		
		return dtos;
	}

	@JsonIgnore
	public List<Integer> getItensMarcados(){
		
		List<Integer> retorno = new ArrayList<Integer>();
		if(imovelItens!=null) {
			for(ImovelItens obj:  imovelItens) {
				retorno.add(obj.getItens().getId());
			}
		}
		
		return retorno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Imovel other = (Imovel) obj;
		if (id != other.id)
			return false;
		return true;
	}

	

	

	
	
	
}