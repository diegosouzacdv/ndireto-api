package com.ndireto.api.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ndireto.api.dto.LocalizacaoDTO;
import com.ndireto.api.dto.PessoaDTO;
import com.ndireto.api.dto.VisitaDTO;

@Entity
@Table(name="VISITA")
public class Visita implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="VIS_CODIGO",unique=true, nullable=false)
	private int id;

	@ManyToOne
	@JoinColumn(name="PES_CODIGOCOLABORADOR")
	private Pessoa pessoaColaborador;
	
	@ManyToOne
	@JoinColumn(name="PES_CODIGOCOMPRADOR")
	private Pessoa pessoaComprador; 
	
	@ManyToOne
	@JoinColumn(name="PES_CODIGO_CADASTRANTE")
	private Pessoa pessoaCadastrante;
	
	@ManyToOne
	@JoinColumn(name="IMO_CODIGO")
	private Imovel imovel;
	
	@Column(name="VIS_DTHORAVISITA")
	private LocalDateTime dataHoraVisita;
	
	
	@Column(name="VIS_DATA_CADASTRO")
	private LocalDateTime dataHoraCadastro;
	
	
	@ManyToOne
	@JoinColumn(name="TPV_CODIGO")
	private TipoVisita tipoVisita;
	
	@Column(name="VIS_AVALIACAO")
	private Integer avaliacao;
	
	@Column(name="VIS_OBSERVACAO")
	private String observacao;
	
	@ManyToOne
	@JoinColumn(name="STA_AVALIACAO")
	private StatusVisita statusVisita; 
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pessoa getPessoaColaborador() {
		return pessoaColaborador;
	}

	public void setPessoaColaborador(Pessoa pessoaColaborador) {
		this.pessoaColaborador = pessoaColaborador;
	}

	public Pessoa getPessoaComprador() {
		return pessoaComprador;
	}
	
	public Integer getAvaliacao() {
		return avaliacao;
	}

	public void setAvaliacao(Integer avaliacao) {
		this.avaliacao = avaliacao;
	}

	public StatusVisita getStatusVisita() {
		return statusVisita;
	}

	public void setStatusVisita(StatusVisita statusVisita) {
		this.statusVisita = statusVisita;
	}

	public void setPessoaComprador(Pessoa pessoaComprador) {
		this.pessoaComprador = pessoaComprador;
	}
	
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	public LocalDateTime getDataHoraVisita() {
		return dataHoraVisita;
	}

	public void setDataHoraVisita(LocalDateTime dataHoraVisita) {
		this.dataHoraVisita = dataHoraVisita;
	}


	public TipoVisita getTipoVisita() {
		return tipoVisita;
	}

	public void setTipoVisita(TipoVisita tipoVisita) {
		this.tipoVisita = tipoVisita;
	}

	public Pessoa getPessoaCadastrante() {
		return pessoaCadastrante;
	}

	public void setPessoaCadastrante(Pessoa pessoaCadastrante) {
		this.pessoaCadastrante = pessoaCadastrante;
	}
	

	public LocalDateTime getDataHoraCadastro() {
		return dataHoraCadastro;
	}

	public void setDataHoraCadastro(LocalDateTime dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}
	
	@JsonIgnore
	public VisitaDTO fromDTO() {
		VisitaDTO dto = new VisitaDTO();
		DateTimeFormatter formatterDia = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		DateTimeFormatter formatterHora = DateTimeFormatter.ofPattern("HH:mm");
		dto.setCodigo(this.getId());
		dto.setData(this.getDataHoraVisita().format(formatterDia));
		dto.setHorario(this.getDataHoraVisita().format(formatterHora));
		dto.setColaborador(this.getPessoaColaborador()== null?new PessoaDTO(): this.getPessoaColaborador().fromDTO());
		
		dto.setComprador(this.getPessoaComprador()== null?new PessoaDTO():this.getPessoaComprador().fromDTO());
		dto.setProprietario(this.getImovel().getProprietario().fromDTO());
		dto.setTipoCodigo(this.getTipoVisita().getId());
		dto.setTipoNome(this.getTipoVisita().getNome());
		dto.setImovel(this.getImovel().fromDTO());
		dto.setStatusVisita(statusVisita.getNome());
		String localizacaoLougradouro = this.getImovel() != null && this.getImovel().getEndereco() != null ? 
				this.getImovel().getEndereco().fromImovelDTO().getLatitude() + "," + this.getImovel().getEndereco().fromImovelDTO().getLongitude() :
					"";
		dto.setLocalizacaoLogradouro(localizacaoLougradouro);
		LocalizacaoDTO localizacao = new LocalizacaoDTO(this.getId(), this.getImovel().getLatitude(), this.getImovel().getLongitude());
		String localizacaoNova = localizacao.getLatitude() + "," + localizacao.getLongitude();
		dto.setLocalizacaoNova(localizacaoNova);
		return dto;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Visita other = (Visita) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Visita [id=" + id + ", pessoaColaborador=" + pessoaColaborador + ", pessoaComprador=" + pessoaComprador
				+ ", pessoaCadastrante=" + pessoaCadastrante + ", imovel=" + imovel + ", dataHoraVisita="
				+ dataHoraVisita + ", dataHoraCadastro=" + dataHoraCadastro + ", tipoVisita=" + tipoVisita
				+ ", avaliacao=" + avaliacao + ", observacao=" + observacao + ", statusVisita=" + statusVisita + "]";
	}

	

	
	
}
