package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ndireto.api.dto.LogradouroDTO;
/**
 * The persistent class for the logradouro database table.
 * 
 */
@Entity
@Table(name="LOGRADOURO")
public class Logradouro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="LGG_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="LGG_LATITUDE")
	private String latitude;
	
	@Column(name="LGG_LONGITUDE")
	private String longitude;

	@Column(name="LGG_NOME")
	private String nome;

	@Column(name="LGG_CEP")
	private String cep;
	
	//bi-directional many-to-one association to Bairro
	@ManyToOne
	@JoinColumn(name="BAI_CODIGO")
	private Bairro bairro;
	
//	//bi-directional many-to-one association to logradouro
//	@JsonIgnore
//	@OneToMany(mappedBy="logradouro")
//	private List<Endereco> listEnderecos;
	
	@Transient
	private int isCidade;
	
	public int getIsCidade() {
		return isCidade;
	}

	public void setIsCidade(int isCidade) {
		this.isCidade = isCidade;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}
	
//	public List<Endereco> getListEnderecos() {
//		return listEnderecos;
//	}
//
//	public void setListEnderecos(List<Endereco> listEnderecos) {
//		this.listEnderecos = listEnderecos;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Logradouro other = (Logradouro) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public LogradouroDTO fromDTO() {
		
		LogradouroDTO dto = new LogradouroDTO();
		
		dto.setCodigo(this.getId());
		dto.setNome(this.nome);
		dto.setCep(this.cep);
		dto.setLatitude(this.latitude);
		dto.setLongitude(this.longitude);
		dto.setBairro(this.bairro.fromDTO());
		
		return dto;
	}

	
	
	
}
