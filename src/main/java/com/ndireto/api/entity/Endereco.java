	package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ndireto.api.dto.EnderecoImovelDTO;
import com.ndireto.api.dto.EnderecoPessoaDTO;

/**
 * The persistent class for the email database table.
 * 
 */
@Entity
@Table(name="ENDERECO")
public class Endereco implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="END_CODIGO",unique=true, nullable=false)
	private int id;
	
	@Column(name="END_COMPLEMENTO")
	private String complemento;
	
	@Column(name="END_NUMERO")
	private String numero;
	
	//bi-directional many-to-one association to Pessoa
	@ManyToOne  
	@JoinColumn(name="LGG_CODIGO")
	private Logradouro logradouro;

	//bi-directional many-to-one association to Pessoa
	@JsonIgnore
	@ManyToOne  
	@JoinColumn(name="PES_CODIGO")
	private Pessoa pessoa;

	//bi-directional many-to-one association to Imovel
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="IMO_CODIGO")
	private Imovel imovel;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}


	public Logradouro getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(Logradouro logradouro) {
		this.logradouro = logradouro;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}
	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	@JsonIgnore
	public EnderecoPessoaDTO fromDTO() {
		EnderecoPessoaDTO dto = new EnderecoPessoaDTO();
		dto.setComplemento(this.getComplemento());
		dto.setNumero(this.getNumero());
		dto.setLatitude(this.logradouro.getLatitude());
		dto.setLongitude(this.logradouro.getLongitude());
		dto.setLogradouro(this.logradouro.fromDTO());
		dto.setEnderecoCompleto(this.getLogradouro().getNome()+" "+this.getComplemento()+" "
		+this.getNumero()+" "+this.getLogradouro().getBairro().getNome()+" "+
		this.getLogradouro().getBairro().getCidade().getNome()+" - "+
		this.getLogradouro().getBairro().getCidade().getUfe().getSigla()+" - cep:"+this.getLogradouro().getCep());
		
		return dto;
	}	
	
	@JsonIgnore
	public EnderecoImovelDTO fromImovelDTO() {
		EnderecoImovelDTO dto = new EnderecoImovelDTO();
		
		dto.setCep(this.logradouro.getCep());
		dto.setComplemento(this.getComplemento());
		dto.setNumero(this.getNumero());
		dto.setLatitude(this.getLogradouro().getLatitude());
		dto.setLongitude(this.logradouro.getLongitude());
		dto.setLogradouro(this.logradouro.fromDTO());
		dto.setEnderecoCompleto(this.getLogradouro().getNome()+" "+this.getComplemento()+" "
				+this.getNumero()+" "+this.getLogradouro().getBairro().getNome()+" "+
				this.getLogradouro().getBairro().getCidade().getNome()+" - "+
				this.getLogradouro().getBairro().getCidade().getUfe().getSigla()+" - cep:"+this.getLogradouro().getCep());
		return dto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	
	


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Endereco other = (Endereco) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Endereco [id=" + id + ", complemento=" + complemento + ", numero=" + numero + ", logradouro="
				+ logradouro + ", pessoa=" + pessoa + ", imovel=" + imovel + "]";
	}



	
	
			
}