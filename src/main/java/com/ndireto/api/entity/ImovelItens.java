package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="IMOVEL_ITENS")
public class ImovelItens implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IMI_CODIGO",unique=true, nullable=false)
	private int id;

	@ManyToOne()
	@JoinColumn(name="IMO_CODIGO")
	private Imovel imovel; 
	
	@ManyToOne()
	@JoinColumn(name="ITE_CODIGO")
	private Itens itens;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	public Itens getItens() {
		return itens;
	}

	public void setItens(Itens itens) {
		this.itens = itens;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImovelItens other = (ImovelItens) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ImovelItens [id=" + id + ", " + (imovel != null ? "imovel=" + imovel.getId()+ ", " : "")
				+ (itens != null ? "itens=" + itens.getId() : "") + "]";
	}
	
	
	

}
