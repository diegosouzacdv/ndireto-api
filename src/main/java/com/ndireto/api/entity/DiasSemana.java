package com.ndireto.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="dias_semana")
public class DiasSemana implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="dse_codigo")
	private Integer codigo;

	@Column(name="dse_nome")
	private String nome;

	@Column(name="dse_sigla")
	private String sigla;

	@Column(name="dse_ordem")
	private int ordem;
	
	@Transient
	private boolean cheioManha;
	
	@Transient
	private boolean cheioTarde;
	
	@Transient
	private boolean cheioNoite;

	public DiasSemana() {
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return this.sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public DiasSemana(Integer codigo) {
		super();
		this.codigo = codigo;
	}

	public boolean isCheioManha() {
		return cheioManha;
	}

	public void setCheioManha(boolean cheioManha) {
		this.cheioManha = cheioManha;
	}

	public boolean isCheioTarde() {
		return cheioTarde;
	}

	public void setCheioTarde(boolean cheioTarde) {
		this.cheioTarde = cheioTarde;
	}

	public boolean isCheioNoite() {
		return cheioNoite;
	}

	public void setCheioNoite(boolean cheioNoite) {
		this.cheioNoite = cheioNoite;
	}

	@Override
	public String toString() {
		return "DiasSemana [" + (codigo != null ? "codigo=" + codigo + ", " : "")
				+ (nome != null ? "nome=" + nome + ", " : "") + (sigla != null ? "sigla=" + sigla + ", " : "")
				+ "ordem=" + ordem + ", cheioManha=" + cheioManha + ", cheioTarde=" + cheioTarde + ", cheioNoite="
				+ cheioNoite + "]";
	}
	
	
	
	

}