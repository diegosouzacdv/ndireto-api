package com.ndireto.api.entity;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
@Transactional
@Scope(value="session",proxyMode=ScopedProxyMode.TARGET_CLASS)
public class UsuarioSistema  extends UsernamePasswordAuthenticationToken {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Pessoa pessoa;
	
	
	private String mensagens = "0";
	
	
	public UsuarioSistema(Pessoa pessoa, String password,
			Collection<? extends GrantedAuthority> authorities) {
		
		super(pessoa.getNome(), password, authorities);
		// TODO Auto-generated constructor stub
		this.pessoa = pessoa;
		//System.out.println(this.policial.getIdPessoa().getNome());
		
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public String getMensagens() {
		return mensagens;
	}

	public void setMensagens(String mensagens) {
		this.mensagens = mensagens;
	}

}
