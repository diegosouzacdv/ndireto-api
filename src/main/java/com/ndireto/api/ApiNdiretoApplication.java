package com.ndireto.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.ndireto.api.config.property.NdiretoApiProperty;


@SpringBootApplication
@EnableConfigurationProperties(NdiretoApiProperty.class)
public class ApiNdiretoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiNdiretoApplication.class, args);
	}

}
