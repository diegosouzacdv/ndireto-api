package com.ndireto.api.enumeration;

public enum TipoPessoaImovelEnum {
	

	GESTOR(1, "GESTOR"),
	FUNCIONARIOADMINISTRATIVO(2, "FUNCIONARIO ADMINISTRATIVO"),
	CORRETOR(3, "CORRETOR"),
	FOTOGRAFO(4, "FOTOGRAFO"),
	COMPRADOR(5, "COMPRADOR"),
	ASSOCIADO(6, "ASSOCIADO");
	
	private int id;
	private String descricao;
	
	private TipoPessoaImovelEnum(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}  
	
	
}
