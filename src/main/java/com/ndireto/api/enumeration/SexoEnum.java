package com.ndireto.api.enumeration;

public enum SexoEnum {
	

	MASCULINO(1, "M"),
	FEMININO(2, "F");
	
	private int id;
	private String descricao;
	
	private SexoEnum(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}  
	
	
}
