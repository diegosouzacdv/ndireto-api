package com.ndireto.api.dto;

public class BairroDTO {

	private Integer codigo;
	private String nome;
	private CidadeDTO cidade;
	

	public BairroDTO() {
		// TODO Auto-generated constructor stub
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public CidadeDTO getCidade() {
		return cidade;
	}
	public void setCidade(CidadeDTO cidade) {
		this.cidade = cidade;
	}
	
	
}
