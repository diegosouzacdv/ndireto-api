package com.ndireto.api.dto;

public class LogradouroDTO {

	private Integer codigo;
	private String nome;
	private BairroDTO bairro;
	private String longitude;
	private String latitude;
	private String cep;

	
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public LogradouroDTO() {
		// TODO Auto-generated constructor stub
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public BairroDTO getBairro() {
		return bairro;
	}
	public void setBairro(BairroDTO bairro) {
		this.bairro = bairro;
	}
	
	
	
}
