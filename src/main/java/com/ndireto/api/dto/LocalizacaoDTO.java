package com.ndireto.api.dto;

public class LocalizacaoDTO {

	private Integer codigoImovel;
	private String latitude;
	private String longitude;
	
	
	public LocalizacaoDTO() {
		super();
	}

	public LocalizacaoDTO(Integer codigoImovel, String latitude, String longitude) {
		super();
		this.codigoImovel = codigoImovel;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Integer getCodigoImovel() {
		return codigoImovel;
	}
	public void setCodigoImovel(Integer codigoImovel) {
		this.codigoImovel = codigoImovel;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	
	
	
}
