package com.ndireto.api.dto;

public class AssociadoDTO {

	private String nome;
	private String telefone;
	private String email;
	private boolean informacoes;
	private boolean termo;
	private Integer tipo;
	private String tokenIndicacao;
	private String enderecoImovel;
	private String horario;
	
	
	public String getEnderecoImovel() {
		return enderecoImovel;
	}
	public void setEnderecoImovel(String enderecoImovel) {
		this.enderecoImovel = enderecoImovel;
	}
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isInformacoes() {
		return informacoes;
	}
	public void setInformacoes(boolean informacoes) {
		this.informacoes = informacoes;
	}
	public boolean isTermo() {
		return termo;
	}
	public void setTermo(boolean termo) {
		this.termo = termo;
	}
	
	
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	
	
	public String getTokenIndicacao() {
		return tokenIndicacao;
	}
	public void setTokenIndicacao(String tokenIndicacao) {
		this.tokenIndicacao = tokenIndicacao;
	}
	@Override
	public String toString() {
		return "AssociadoDTO [" + (nome != null ? "nome=" + nome + ", " : "")
				+ (telefone != null ? "telefone=" + telefone + ", " : "")
				+ (email != null ? "email=" + email + ", " : "") + "informacoes=" + informacoes + ", termo=" + termo
				+ "]";
	}
	
	
	
	
	
	
}
