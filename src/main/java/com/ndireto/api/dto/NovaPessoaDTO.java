package com.ndireto.api.dto;

import java.io.Serializable;
import java.time.LocalDate;

import org.hibernate.validator.constraints.NotEmpty;

import com.ndireto.api.entity.Pessoa;

public class NovaPessoaDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message="Nome é obrigatório")
	private String nome;
	@NotEmpty(message="CPF é obrigatório")
	private String cpf;
	@NotEmpty(message="E-mail é obrigatório")
	private String email;
	@NotEmpty(message="Data de Nascimento é obrigatório")
	private LocalDate dataNascimento;
	@NotEmpty(message="Senha é obrigatório")
	private String senha;
	
	public NovaPessoaDTO() {
	}
	
	public NovaPessoaDTO(Pessoa obj) {
		nome = obj.getNome();
		cpf = obj.getCpf();
		email = obj.getEmail();
		dataNascimento = obj.getDataNascimento();
		senha = obj.getSenha();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
