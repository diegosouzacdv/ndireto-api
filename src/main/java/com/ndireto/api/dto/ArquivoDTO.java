package com.ndireto.api.dto;


public class ArquivoDTO {

	private String nome;
	private String contentType;
	private String diretorio;	
	
	public ArquivoDTO(String nome, String contentType,String diretorio) {
		this.nome = nome;
		this.contentType = contentType;
		this.diretorio = diretorio;
		
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getDiretorio() {
		return diretorio;
	}

	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}

	@Override
	public String toString() {
		return "ArquivoDTO [" + (nome != null ? "nome=" + nome + ", " : "")
				+ (contentType != null ? "contentType=" + contentType + ", " : "")
				+ (diretorio != null ? "diretorio=" + diretorio : "") + "]";
	}
	

	
}
