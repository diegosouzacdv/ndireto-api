package com.ndireto.api.dto;

import java.time.LocalDateTime;

public class CompromissoDTO {
	
	private int id;
	private String descricao;
	private String titulo;
	private LocalDateTime dataHora;
	private String cor;
	private String visitante;
	private String demonstrador;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	
	public LocalDateTime getDataHora() {
		return dataHora;
	}
	public void setDataHora(LocalDateTime dataHora) {
		this.dataHora = dataHora;
	}
	
	
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public String getVisitante() {
		return visitante;
	}
	public void setVisitante(String visitante) {
		this.visitante = visitante;
	}
	public String getDemonstrador() {
		return demonstrador;
	}
	public void setDemonstrador(String demonstrador) {
		this.demonstrador = demonstrador;
	}
	@Override
	public String toString() {
		return "CompromissoDTO [id=" + id + ", " + (descricao != null ? "descricao=" + descricao + ", " : "")
				+ (titulo != null ? "titulo=" + titulo : "") + "]";
	}
	
}
