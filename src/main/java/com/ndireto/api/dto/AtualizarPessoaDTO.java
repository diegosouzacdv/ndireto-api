package com.ndireto.api.dto;

import java.time.format.DateTimeFormatter;

import com.ndireto.api.entity.Pessoa;

public class AtualizarPessoaDTO {
	
	private Integer id;
	private String cpf;
	private EnderecoDTO enderecoDTO;
	private String sexo;
	private String nomeFoto;
	private ContaDTO contaDTO;
	private String celular;
	private String telefoneFixo;
	private String dataNascimento;
	
	public String getDataNascimento() {
		return dataNascimento;
	}
	
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public AtualizarPessoaDTO() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public EnderecoDTO getEnderecoDTO() {
		return enderecoDTO;
	}
	public void setEnderecoDTO(EnderecoDTO enderecoDTO) {
		this.enderecoDTO = enderecoDTO;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getNomeFoto() {
		return nomeFoto;
	}
	public void setNomeFoto(String nomeFoto) {
		this.nomeFoto = nomeFoto;
	}
	public ContaDTO getContaDTO() {
		return contaDTO;
	}
	public void setContaDTO(ContaDTO contaDTO) {
		this.contaDTO = contaDTO;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getTelefoneFixo() {
		return telefoneFixo;
	}
	public void setTelefoneFixo(String telefoneFixo) {
		this.telefoneFixo = telefoneFixo;
	}
	public void fromDTO(Pessoa pessoa) {
		
		EnderecoDTO enderecoDTO =  new EnderecoDTO();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
		if(!pessoa.getEnderecos().isEmpty()) {
			enderecoDTO.setComplemento(pessoa.getEndereco().getComplemento());
			enderecoDTO.setLogradouroCodigo(pessoa.getEndereco().getLogradouro().getId());
			enderecoDTO.setNumero(pessoa.getEndereco().getNumero());
		}
		
		this.id = pessoa.getId();
		this.celular = pessoa.getCelular();
		this.cpf = pessoa.getCpfSemMascara();
		this.nomeFoto = pessoa.getNomeFoto();
		this.sexo = pessoa.getSexo();
		this.enderecoDTO = enderecoDTO;
		this.dataNascimento = (pessoa.getDataNascimento()!=null?pessoa.getDataNascimento().format(formatter):null);
		
	}

	@Override
	public String toString() {
		return "AtualizarPessoaDTO [id=" + id + ", cpf=" + cpf + ", enderecoDTO=" + enderecoDTO + ", sexo=" + sexo
				+ ", nomeFoto=" + nomeFoto + ", contaDTO=" + contaDTO + ", celular=" + celular + ", telefoneFixo="
				+ telefoneFixo + ", dataNascimento=" + dataNascimento + "]";
	}
	
	
	
	
	
	
}
