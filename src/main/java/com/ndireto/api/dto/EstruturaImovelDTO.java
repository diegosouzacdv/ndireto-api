package com.ndireto.api.dto;

public class EstruturaImovelDTO {

	private int qtdBanheiros;
	private int qtdSuites;
	private int qtdQuartos;
	private int vagas;
	private Integer codigoImovel;
	
	
	public EstruturaImovelDTO() {
		super();
	}
	
	public EstruturaImovelDTO(int qtdBanheiros, int qtdSuites, int qtdQuartos, int vagas, Integer codigoImovel) {
		super();
		this.qtdBanheiros = qtdBanheiros;
		this.qtdSuites = qtdSuites;
		this.qtdQuartos = qtdQuartos;
		this.vagas = vagas;
		this.codigoImovel = codigoImovel;
	}

	public int getQtdBanheiros() {
		return qtdBanheiros;
	}
	public void setQtdBanheiros(int qtdBanheiros) {
		this.qtdBanheiros = qtdBanheiros;
	}
	public int getQtdSuites() {
		return qtdSuites;
	}
	public void setQtdSuites(int qtdSuites) {
		this.qtdSuites = qtdSuites;
	}
	public int getQtdQuartos() {
		return qtdQuartos;
	}
	public void setQtdQuartos(int qtdQuartos) {
		this.qtdQuartos = qtdQuartos;
	}
	public int getVagas() {
		return vagas;
	}
	public void setVagas(int vagas) {
		this.vagas = vagas;
	}
	public Integer getCodigoImovel() {
		return codigoImovel;
	}
	public void setCodigoImovel(Integer codigoImovel) {
		this.codigoImovel = codigoImovel;
	}
	@Override
	public String toString() {
		return "EstruturaImovelDTO [qtdBanheiros=" + qtdBanheiros + ", qtdSuites=" + qtdSuites + ", qtdQuartos="
				+ qtdQuartos + ", vagas=" + vagas + ", codigoImovel=" + codigoImovel + "]";
	}
}
