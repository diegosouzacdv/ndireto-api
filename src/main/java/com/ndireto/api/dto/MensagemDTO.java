package com.ndireto.api.dto;

import java.time.format.DateTimeFormatter;

import com.ndireto.api.entity.Mensagem;

public class MensagemDTO {
	
	private int id;
	private String assunto;
	private String texto;
	private String textoEncode;
	
	private String assuntoReduzido;
	private String textoReduzido;
	
	
	private String remetente;
	private String acao;
	private String dataEnvio;
	private String dataLeitura;
	
	
	private String dataEnvioZap;
	private Integer pesCodigoEnvioZap;
	
	private String urlImagen;
	
	private String telefoneDestinatario;
	
	
	public MensagemDTO() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getRemetente() {
		return remetente;
	}
	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}
	public String getAcao() {
		return acao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public String getDataEnvio() {
		return dataEnvio;
	}
	public void setDataEnvio(String dataEnvio) {
		this.dataEnvio = dataEnvio;
	}
	public String getDataLeitura() {
		return dataLeitura;
	}
	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}
	public String getUrlImagen() {
		return urlImagen;
	}
	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}
	
	
	public String getTextoEncode() {
		return textoEncode;
	}
	public void setTextoEncode(String textoEncode) {
		this.textoEncode = textoEncode;
	}
	public String getAssuntoReduzido() {
		return assuntoReduzido;
	}
	public void setAssuntoReduzido(String assuntoReduzido) {
		this.assuntoReduzido = assuntoReduzido;
	}
	public String getTextoReduzido() {
		return textoReduzido;
	}
	public void setTextoReduzido(String textoReduzido) {
		this.textoReduzido = textoReduzido;
	}
	
	public String getDataEnvioZap() {
		return dataEnvioZap;
	}
	public void setDataEnvioZap(String dataEnvioZap) {
		this.dataEnvioZap = dataEnvioZap;
	}
	
	
	public Integer getPesCodigoEnvioZap() {
		return pesCodigoEnvioZap;
	}
	public void setPesCodigoEnvioZap(Integer pesCodigoEnvioZap) {
		this.pesCodigoEnvioZap = pesCodigoEnvioZap;
	}
	
	
	public String getTelefoneDestinatario() {
		return telefoneDestinatario;
	}
	public void setTelefoneDestinatario(String telefoneDestinatario) {
		this.telefoneDestinatario = telefoneDestinatario;
	}

		
	
	public MensagemDTO fromDto(Mensagem obj) {
		 
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		
		this.id = obj.getId();
		this.dataEnvio = obj.getDataInclusao( ) != null ? obj.getDataInclusao().format(formatter) : "";
		this.dataLeitura = (obj.getDataRecebimento()!=null?obj.getDataRecebimento().format(formatter):null);
		this.textoEncode = obj.getObservacaoencode();
		this.dataEnvioZap = obj.getDataEnvioWatsApp() != null ? obj.getDataInclusao().format(formatter) : "";
		this.pesCodigoEnvioZap = (obj.getUsuarioEnvioWatsApp()!=null?obj.getUsuarioEnvioWatsApp().getId():null);
		
		this.assunto = obj.getAssunto();
		this.texto = obj.getObservacao();
		this.telefoneDestinatario = obj.getDestinatario().getTelefoneZap();
		this.assuntoReduzido = obj.getAssuntoReduzida(30);
		this.textoReduzido = obj.getObservacaoReduzida(100);
		this.remetente = obj.getRemetente().getNome();
		this.urlImagen = (obj.getImagem()!=null?"/mensagem/"+obj.getId()+"/imagem":null);
		
		return this;
	}
	

}
