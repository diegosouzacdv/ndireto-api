package com.ndireto.api.dto;

public class HorasDTO {
	
	private int codigo;
	private String horario;
	
	
	public HorasDTO(int codigo, String horario) {
		super();
		this.codigo = codigo;
		this.horario = horario;
	}
	public HorasDTO() {
		super();
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	
	
	

}
