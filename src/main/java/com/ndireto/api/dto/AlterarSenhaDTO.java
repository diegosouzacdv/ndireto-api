package com.ndireto.api.dto;

public class AlterarSenhaDTO {
	
	private String passwordOld;
	private String registerPassword;
	private String registerPasswordConfirmation;
	
	public String getPasswordOld() {
		return passwordOld;
	}
	public void setPasswordOld(String passwordOld) {
		this.passwordOld = passwordOld;
	}
	public String getRegisterPassword() {
		return registerPassword;
	}
	public void setRegisterPassword(String registerPassword) {
		this.registerPassword = registerPassword;
	}
	public String getRegisterPasswordConfirmation() {
		return registerPasswordConfirmation;
	}
	public void setRegisterPasswordConfirmation(String registerPasswordConfirmation) {
		this.registerPasswordConfirmation = registerPasswordConfirmation;
	}
	
}
