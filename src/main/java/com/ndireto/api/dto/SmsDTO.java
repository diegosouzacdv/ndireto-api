package com.ndireto.api.dto;

//
//Sender
//string
//Este campo pode ser utilizado no método de relatóio posteriormente para buscar detalhes individualmente de cada sms, então pode ser passado um Id interno ou qualquer número de controle de requisição. O conteúdo não será enviado aos destinatários e caso não seja preciso controle individual de requisição pode ser informado qualquer valor, o padrão é o username de acesso ao sistema.
//
//Receivers
//string
//Destinatários que irão receber a mensagem. Para dois ou mais destinatários é necessário separá-los por “,”, possuindo o seguinte formato: DDD + Número.
//
//Content
//string
//Conteúdo da mensagem a ser enviada. IMPORTANTE: Em casos que o conteúdo do SMS for superior a 160 caracteres, será tarifado mais de um crédito a cada 153 caracteres. Algumas operadoras como a Oi e Sercomtel não suportam concatenação de mensagens.


public class SmsDTO {

	String Sender =  "BoasVindas";
	String Content = "teste reste";
	String Receivers = "";
	
	
	public String getSender() {
		return Sender;
	}
	public void setSender(String sender) {
		Sender = sender;
	}
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	public String getReceivers() {
		return Receivers;
	}
	public void setReceivers(String receivers) {
		Receivers = receivers;
	}

	
	
	
}
