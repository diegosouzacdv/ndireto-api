package com.ndireto.api.dto;

public class EnderecoImovelDTO {
	
	private String  numero;
	private String  longitude;
	private String  latitude;
	private String  complemento ;
	private String  cep ;
	private Integer imovel;
	private String enderecoCompleto;
	private LogradouroDTO logradouro;
	
	
	
	
	public String getEnderecoCompleto() {
		return enderecoCompleto;
	}


	public void setEnderecoCompleto(String enderecoCompleto) {
		this.enderecoCompleto = enderecoCompleto;
	}


	public LogradouroDTO getLogradouro() {
		return logradouro;
	}


	public void setLogradouro(LogradouroDTO logradouro) {
		this.logradouro = logradouro;
	}


	public EnderecoImovelDTO() {
		// TODO Auto-generated constructor stub
	}


	public Integer getImovel() {
		return imovel;
	}



	public void setImovel(Integer imovel) {
		this.imovel = imovel;
	}



	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	
	
}
