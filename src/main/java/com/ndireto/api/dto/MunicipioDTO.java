package com.ndireto.api.dto;

import com.ndireto.api.entity.Ufe;

public class MunicipioDTO {

	private Integer codigo;
	private String nome;
	private Ufe estado;
	
	
	
	public MunicipioDTO() {
		super();
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Ufe getEstado() {
		return estado;
	}
	public void setEstado(Ufe estado) {
		this.estado = estado;
	}
	
	
	
	
	
	
}
