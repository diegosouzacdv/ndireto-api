package com.ndireto.api.dto;

public class CidadeDTO {

	private Integer codigo;
	private String nome;
	private UfDto uf;
	
	
	public CidadeDTO(Integer codigo, String nome, UfDto uf) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.uf = uf;
	}
	public CidadeDTO() {
		// TODO Auto-generated constructor stub
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public UfDto getUf() {
		return uf;
	}
	public void setUf(UfDto uf) {
		this.uf = uf;
	}
	
	
}
