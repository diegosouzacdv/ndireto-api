package com.ndireto.api.dto;

public class TerrenoImovelDTO {

	private String areautil;
	private String areatotal;
	private Integer codigoImovel;
	
	
	public TerrenoImovelDTO() {
		super();
	}


	public TerrenoImovelDTO(String areautil, String areatotal, Integer codigoImovel) {
		super();
		this.areautil = areautil;
		this.areatotal = areatotal;
		this.codigoImovel = codigoImovel;
	}


	public String getAreautil() {
		return areautil;
	}


	public void setAreautil(String areautil) {
		this.areautil = areautil;
	}


	public String getAreatotal() {
		return areatotal;
	}


	public void setAreatotal(String areatotal) {
		this.areatotal = areatotal;
	}


	public Integer getCodigoImovel() {
		return codigoImovel;
	}


	public void setCodigoImovel(Integer codigoImovel) {
		this.codigoImovel = codigoImovel;
	}


	@Override
	public String toString() {
		return "TerrenoImovelDTO [areautil=" + areautil + ", areatotal=" + areatotal + ", codigoImovel=" + codigoImovel
				+ "]";
	}
	
	
}
