package com.ndireto.api.dto;

import java.util.List;

import com.ndireto.api.entity.Conta;
import com.ndireto.api.entity.Endereco;
import com.ndireto.api.entity.Pessoa;

public class PessoaDTO {
	
	private int id;

	private String nome;
	
	private String nomeMae;
	
	private String nomePai;
	
	private String cpf;
	
	private String dataNascimento;
	
	private String email;
	
	private String sexo;
	
	
	private String celular;
	
	private String fixo;
	
	
	private String creci;
	
	private String token;
	
	private String UUID;
	
	private String rg;
	
	private String rgUfExpedicao;
	
	private String rgOrgaoExpedidor;
	
	private String ufeCreci;
	
	private String statusPessoa;
	
	private List<String> tipoPessoa;
	
	private String pessoaIndicador;
	
	private String dataCadastro;
	
	
	private EnderecoPessoaDTO endereco;

	private Conta conta;
	
	public PessoaDTO(int id, String nome, String cpf, String dataNascimento, String email, String celular, EnderecoPessoaDTO endereco, String token, Conta conta) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
		this.email = email;
		this.celular = celular;
		this.endereco = endereco;
		this.token = token;
		this.conta = conta;
	}


	public PessoaDTO() {
		super();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getNomeMae() {
		return nomeMae;
	}


	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}


	public String getNomePai() {
		return nomePai;
	}


	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}


	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public String getDataNascimento() {
		return dataNascimento;
	}


	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getSexo() {
		return sexo;
	}


	public void setSexo(String sexo) {
		this.sexo = sexo;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public String getFixo() {
		return fixo;
	}


	public void setFixo(String fixo) {
		this.fixo = fixo;
	}


	public String getCreci() {
		return creci;
	}


	public void setCreci(String creci) {
		this.creci = creci;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public String getUUID() {
		return UUID;
	}


	public void setUUID(String uUID) {
		UUID = uUID;
	}


	public String getRg() {
		return rg;
	}


	public void setRg(String rg) {
		this.rg = rg;
	}


	public String getRgUfExpedicao() {
		return rgUfExpedicao;
	}


	public void setRgUfExpedicao(String rgUfExpedicao) {
		this.rgUfExpedicao = rgUfExpedicao;
	}


	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}


	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}


	public String getUfeCreci() {
		return ufeCreci;
	}


	public void setUfeCreci(String ufeCreci) {
		this.ufeCreci = ufeCreci;
	}


	public String getStatusPessoa() {
		return statusPessoa;
	}


	public void setStatusPessoa(String statusPessoa) {
		this.statusPessoa = statusPessoa;
	}


	public List<String> getTipoPessoa() {
		return tipoPessoa;
	}


	public void setTipoPessoa(List<String> tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}


	public String getPessoaIndicador() {
		return pessoaIndicador;
	}


	public void setPessoaIndicador(String pessoaIndicador) {
		this.pessoaIndicador = pessoaIndicador;
	}


	public String getDataCadastro() {
		return dataCadastro;
	}


	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}


	public EnderecoPessoaDTO getEndereco() {
		return endereco;
	}


	public void setEndereco(EnderecoPessoaDTO endereco) {
		this.endereco = endereco;
	}


	public Conta getConta() {
		return conta;
	}


	public void setConta(Conta conta) {
		this.conta = conta;
	}

	
	


}
