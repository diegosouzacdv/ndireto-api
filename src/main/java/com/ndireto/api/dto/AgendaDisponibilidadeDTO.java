package com.ndireto.api.dto;

import java.util.List;

public class AgendaDisponibilidadeDTO {
	
	private int codigoImovel;
	private List<DiasDTO> diasSemana;
	
	
	public AgendaDisponibilidadeDTO(int codigoImovel, List<DiasDTO> diasSemana) {
		super();
		this.codigoImovel = codigoImovel;
		this.diasSemana = diasSemana;
	}
	public AgendaDisponibilidadeDTO() {
		// TODO Auto-generated constructor stub
	}
	public int getCodigoImovel() {
		return codigoImovel;
	}
	public void setCodigoImovel(int codigoImovel) {
		this.codigoImovel = codigoImovel;
	}
	public List<DiasDTO> getDiasSemana() {
		return diasSemana;
	}
	public void setDiasSemana(List<DiasDTO> diasSemana) {
		this.diasSemana = diasSemana;
	}
	
	
	

}
