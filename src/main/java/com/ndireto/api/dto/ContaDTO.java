package com.ndireto.api.dto;

public class ContaDTO {

	private int banco;
	private int tipo;
	private String agencia;
	private String conta;
	private String digito;
	
	public String getDigito() {
		return digito;
	}
	public int getTipo() {
		return tipo;
	}
	
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public void setDigito(String digito) {
		this.digito = digito;
	}
	
	public ContaDTO() {
		super();
	}
	public int getBanco() {
		return banco;
	}
	public void setBanco(int banco) {
		this.banco = banco;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getConta() {
		return conta;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	
	
	
	
	
}
