package com.ndireto.api.dto;

public class EnderecoDTO {
	
	private Integer logradouroCodigo;
	private String complemento;
	private String numero;
	
	
	public EnderecoDTO() {
		super();
	}
	public Integer getLogradouroCodigo() {
		return logradouroCodigo;
	}
	public void setLogradouroCodigo(Integer logradouroCodigo) {
		this.logradouroCodigo = logradouroCodigo;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	
	
	@Override
	public String toString() {
		return "EnderecoDTO [" + (logradouroCodigo != null ? "logradouroCodigo=" + logradouroCodigo + ", " : "")
				+ (complemento != null ? "complemento=" + complemento + ", " : "")
				+ (numero != null ? "numero=" + numero : "") + "]";
	}
	

}
