package com.ndireto.api.dto;

public class ItensImovelDTO {

	private Integer codigoItem;
	private Integer codigoImovel;
	private String status;
	
	
	public ItensImovelDTO() {
		super();
	}
	public ItensImovelDTO(Integer codigoItem, Integer codigoImovel, String status) {
		super();
		this.codigoItem = codigoItem;
		this.codigoImovel = codigoImovel;
		this.status = status;
	}
	public Integer getCodigoItem() {
		return codigoItem;
	}
	public void setCodigoItem(Integer codigoItem) {
		this.codigoItem = codigoItem;
	}
	public Integer getCodigoImovel() {
		return codigoImovel;
	}
	public void setCodigoImovel(Integer codigoImovel) {
		this.codigoImovel = codigoImovel;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ItensImovelDTO [codigoItem=" + codigoItem + ", codigoImovel=" + codigoImovel + ", status=" + status
				+ "]";
	}
	
	
}
