package com.ndireto.api.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.ndireto.api.entity.Imovel;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.StatusVisita;
import com.ndireto.api.entity.Visita;

public class VisitaDTO {
	
	private int codigo;
	private String data;
	private String horario;
	private PessoaDTO colaborador;
	private PessoaDTO comprador;
	private PessoaDTO proprietario;
	
	private int colaboradorCodigo;
	private int compradorCodigo;
	private int proprietarioCodigo;
	
	private int cadastranteCodigo;
	
	private int tipoCodigo;
	private String tipoNome;
	private ImovelDTO imovel;
	private int codigoImovel;
	private String statusVisita; 
	private String observacao; 
	private String localizacaoLogradouro;
	private String localizacaoNova;
	
	public VisitaDTO(int codigo, String data, String horario, int colaboradorCodigo, int compradorCodigo,
			int proprietarioCodigo, int cadastranteCodigo, int codigoImovel, String statusVisita, String observacao) {
		super();
		this.codigo = codigo;
		this.data = data;
		this.horario = horario;
		this.colaboradorCodigo = colaboradorCodigo;
		this.compradorCodigo = compradorCodigo;
		this.proprietarioCodigo = proprietarioCodigo;
		this.cadastranteCodigo = cadastranteCodigo;
		this.codigoImovel = codigoImovel;
		this.statusVisita = statusVisita;
		this.observacao = observacao;
	}


	public VisitaDTO() {
	}
	
	
	
	
	public int getCodigoImovel() {
		return codigoImovel;
	}


	public void setCodigoImovel(int codigoImovel) {
		this.codigoImovel = codigoImovel;
	}

	
	

	public int getCadastranteCodigo() {
		return cadastranteCodigo;
	}


	public void setCadastranteCodigo(int cadastranteCodigo) {
		this.cadastranteCodigo = cadastranteCodigo;
	}


	public int getColaboradorCodigo() {
		return colaboradorCodigo;
	}


	public void setColaboradorCodigo(int colaboradorCodigo) {
		this.colaboradorCodigo = colaboradorCodigo;
	}


	public int getCompradorCodigo() {
		return compradorCodigo;
	}


	public void setCompradorCodigo(int compradorCodigo) {
		this.compradorCodigo = compradorCodigo;
	}


	public int getProprietarioCodigo() {
		return proprietarioCodigo;
	}


	public void setProprietarioCodigo(int proprietarioCodigo) {
		this.proprietarioCodigo = proprietarioCodigo;
	}


	public String getHorario() {
		return horario;
	}


	public void setHorario(String horario) {
		this.horario = horario;
	}


	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public PessoaDTO getColaborador() {
		return colaborador;
	}
	public void setColaborador(PessoaDTO colaborador) {
		this.colaborador = colaborador;
	}
	public PessoaDTO getComprador() {
		return comprador;
	}
	public void setComprador(PessoaDTO comprador) {
		this.comprador = comprador;
	}
	public int getTipoCodigo() {
		return tipoCodigo;
	}
	public void setTipoCodigo(int tipoCodigo) {
		this.tipoCodigo = tipoCodigo;
	}
	public String getTipoNome() {
		return tipoNome;
	}
	public void setTipoNome(String tipoNome) {
		this.tipoNome = tipoNome;
	}
	public ImovelDTO getImovel() {
		return imovel;
	}
	public void setImovel(ImovelDTO imovel) {
		this.imovel = imovel;
	}

	public PessoaDTO getProprietario() {
		return proprietario;
	}

	public void setProprietario(PessoaDTO proprietario) {
		this.proprietario = proprietario;
	}
	
	public String getStatusVisita() {
		return statusVisita;
	}

	public void setStatusVisita(String statusVisita) {
		this.statusVisita = statusVisita;
	}


	
	
	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	/**
	 * @return the localizacaoLogradouro
	 */
	public String getLocalizacaoLogradouro() {
		return localizacaoLogradouro;
	}


	/**
	 * @param localizacaoLogradouro the localizacaoLogradouro to set
	 */
	public void setLocalizacaoLogradouro(String localizacaoLogradouro) {
		this.localizacaoLogradouro = localizacaoLogradouro;
	}


	/**
	 * @return the localizacaoNova
	 */
	public String getLocalizacaoNova() {
		return localizacaoNova;
	}


	/**
	 * @param localizacaoNova the localizacaoNova to set
	 */
	public void setLocalizacaoNova(String localizacaoNova) {
		this.localizacaoNova = localizacaoNova;
	}
}
