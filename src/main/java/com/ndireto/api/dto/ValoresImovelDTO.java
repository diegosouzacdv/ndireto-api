package com.ndireto.api.dto;

import java.math.BigDecimal;

public class ValoresImovelDTO {

	private BigDecimal valor;
	private BigDecimal valorCondominio;
	private BigDecimal valorIPTU;
	private Integer codigoImovel;
	
	
	public ValoresImovelDTO() {
		super();
	}


	public ValoresImovelDTO(BigDecimal valor, BigDecimal valorCondominio, BigDecimal valorIPTU, Integer codigoImovel) {
		super();
		this.valor = valor;
		this.valorCondominio = valorCondominio;
		this.valorIPTU = valorIPTU;
		this.codigoImovel = codigoImovel;
	}


	public BigDecimal getValor() {
		return valor;
	}


	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}


	public BigDecimal getValorCondominio() {
		return valorCondominio;
	}


	public void setValorCondominio(BigDecimal valorCondominio) {
		this.valorCondominio = valorCondominio;
	}


	public BigDecimal getValorIPTU() {
		return valorIPTU;
	}


	public void setValorIPTU(BigDecimal valorIPTU) {
		this.valorIPTU = valorIPTU;
	}


	public Integer getCodigoImovel() {
		return codigoImovel;
	}


	public void setCodigoImovel(Integer codigoImovel) {
		this.codigoImovel = codigoImovel;
	}


	@Override
	public String toString() {
		return "ValoresImovelDTO [valor=" + valor + ", valorCondominio=" + valorCondominio + ", valorIPTU=" + valorIPTU
				+ ", codigoImovel=" + codigoImovel + "]";
	}



	
}
