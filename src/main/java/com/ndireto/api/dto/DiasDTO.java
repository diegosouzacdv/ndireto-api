package com.ndireto.api.dto;

import java.util.List;

public class DiasDTO {
	
	private Integer codigo;
	private String nome;
	private String sigla;
	private String data;
	private boolean cheioManha;
	private boolean cheioTarde;
	private boolean cheioNoite;
	private List<HorasDTO> hora;
	
	public DiasDTO() {
	}
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public boolean isCheioManha() {
		return cheioManha;
	}


	public void setCheioManha(boolean cheioManha) {
		this.cheioManha = cheioManha;
	}


	public boolean isCheioTarde() {
		return cheioTarde;
	}


	public void setCheioTarde(boolean cheioTarde) {
		this.cheioTarde = cheioTarde;
	}


	public boolean isCheioNoite() {
		return cheioNoite;
	}


	public void setCheioNoite(boolean cheioNoite) {
		this.cheioNoite = cheioNoite;
	}


	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public List<HorasDTO> getHora() {
		return hora;
	}
	public void setHora(List<HorasDTO> hora) {
		this.hora = hora;
	}


	public String getData() {
		return data;
	}


	public void setData(String data) {
		this.data = data;
	}

	
	
	
}
