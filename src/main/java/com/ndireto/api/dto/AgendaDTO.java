package com.ndireto.api.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AgendaDTO {
	
	private int id;
	private LocalDateTime data;
	private List<CompromissoDTO> compromissos = new ArrayList<CompromissoDTO>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LocalDateTime getData() {
		return data;
	}
	public void setData(LocalDateTime data) {
		this.data = data;
	}
	public void addCompromisso(CompromissoDTO compromisso) {
		this.compromissos.add(compromisso);
	}
	public List<CompromissoDTO> getCompromissos() {
		return compromissos;
	}
	public void setCompromissos(List<CompromissoDTO> compromissos) {
		this.compromissos = compromissos;
	}
	@Override
	public String toString() {
		return "AgendaDTO [id=" + id + ", " + (data != null ? "data=" + data + ", " : "")
				+ (compromissos != null ? "compromissos=" + compromissos : "") + "]";
	}
	
	
}
