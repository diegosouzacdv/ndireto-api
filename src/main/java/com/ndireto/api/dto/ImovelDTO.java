package com.ndireto.api.dto;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;

import com.ndireto.api.entity.StatusImovel;
import com.ndireto.api.entity.TipoNegociacao;

public class ImovelDTO {
	
	private int id;
	private EnderecoImovelDTO endereco;
	private String fotoCapa;
	private BigDecimal valor;
	private BigDecimal valorCondominio;
	private BigDecimal valorIPTU;
	private LocalizacaoDTO localizacao;
	private List<CaracteristicaImovelDTO> caracteristicas;
	private List<String> fotos;
	private EstruturaImovelDTO estrutura;
	private String tipoImovel;
	private String tipoNegociacao;
	private String statusImovel;
	private int qtdBanheiros;
	private int qtdSuites;
	private int qtdQuartos;
	private int vagas;
	private String areautil;
	private String areatotal;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public EnderecoImovelDTO getEndereco() {
		return endereco;
	}
	public void setEndereco(EnderecoImovelDTO endereco) {
		this.endereco = endereco;
	}
	public String getFotoCapa() {
		return fotoCapa;
	}
	public void setFotoCapa(String fotoCapa) {
		this.fotoCapa = fotoCapa;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public LocalizacaoDTO getLocalizacao() {
		return localizacao;
	}
	public void setLocalizacao(LocalizacaoDTO localizacao) {
		this.localizacao = localizacao;
	}
	
	public List<CaracteristicaImovelDTO> getCaracteristicas() {
		return caracteristicas;
	}
	public void setCaracteristicas(List<CaracteristicaImovelDTO> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}
	public EstruturaImovelDTO getEstrutura() {
		return estrutura;
	}
	public void setEstrutura(EstruturaImovelDTO estrutura) {
		this.estrutura = estrutura;
	}
	public String getTipoImovel() {
		return tipoImovel;
	}
	public void setTipoImovel(String tipoImovel) {
		this.tipoImovel = tipoImovel;
	}
	public String getTipoNegociacao() {
		return tipoNegociacao;
	}
	public void setTipoNegociacao(String tipoNegociacao) {
		this.tipoNegociacao = tipoNegociacao;
	}
	public String getStatusImovel() {
		return statusImovel;
	}
	public void setStatusImovel(String statusImovel) {
		this.statusImovel = statusImovel;
	}
	public BigDecimal getValorCondominio() {
		return valorCondominio;
	}
	public void setValorCondominio(BigDecimal valorCondominio) {
		this.valorCondominio = valorCondominio;
	}
	public BigDecimal getValorIPTU() {
		return valorIPTU;
	}
	public void setValorIPTU(BigDecimal valorIPTU) {
		this.valorIPTU = valorIPTU;
	}
	public int getQtdBanheiros() {
		return qtdBanheiros;
	}
	public void setQtdBanheiros(int qtdBanheiros) {
		this.qtdBanheiros = qtdBanheiros;
	}
	public int getQtdSuites() {
		return qtdSuites;
	}
	public void setQtdSuites(int qtdSuites) {
		this.qtdSuites = qtdSuites;
	}
	public int getQtdQuartos() {
		return qtdQuartos;
	}
	public void setQtdQuartos(int qtdQuartos) {
		this.qtdQuartos = qtdQuartos;
	}
	public int getVagas() {
		return vagas;
	}
	public void setVagas(int vagas) {
		this.vagas = vagas;
	}
	public String getAreautil() {
		return areautil;
	}
	public void setAreautil(String areautil) {
		this.areautil = areautil;
	}
	public String getAreatotal() {
		return areatotal;
	}
	public void setAreatotal(String areatotal) {
		this.areatotal = areatotal;
	}
	public List<String> getFotos() {
		return fotos;
	}
	public void setFotos(List<String> fotos) {
		this.fotos = fotos;
	}
	
	
	
}
