package com.ndireto.api.dto;

public class UfDto {

	private Integer codigo;
	private String nome;
	
	
	
	public UfDto(Integer codigo, String nome) {
		super();
		this.codigo = codigo;
		this.nome = nome;
	}
	public UfDto() {
		// TODO Auto-generated constructor stub
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
	
}
