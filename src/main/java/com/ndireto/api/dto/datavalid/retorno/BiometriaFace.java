package com.ndireto.api.dto.datavalid.retorno;

public class BiometriaFace {
	
	private boolean disponivel = true;
    private String probabilidade = "";
    private float similaridade = 0;
    
	public boolean isDisponivel() {
		return disponivel;
	}
	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}
	public String getProbabilidade() {
		return probabilidade;
	}
	public void setProbabilidade(String probabilidade) {
		this.probabilidade = probabilidade;
	}
	public float getSimilaridade() {
		return similaridade;
	}
	public void setSimilaridade(float similaridade) {
		this.similaridade = similaridade;
	}
	
	public boolean isValida() {
		
		return similaridade >= 0.92;
	}
    
    

}
