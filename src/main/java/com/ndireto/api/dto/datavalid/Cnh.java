package com.ndireto.api.dto.datavalid;

public class Cnh {

	 String numero_registro =  "0000001";
	 String registro_nacional_estrangeiro = "123456";
	 String categoria =  "AB";
	 String data_primeira_habilitacao =  "2000-10-10";
	 String data_validade =  "2000-10-10";
	 String data_ultima_emissao =  "2018-05-31";
	 String codigo_situacao =  "3";
	 String descricao_situacao =  "EMITIDA";
	 
	 
	public String getNumero_registro() {
		return numero_registro;
	}
	public void setNumero_registro(String numero_registro) {
		this.numero_registro = numero_registro;
	}
	public String getRegistro_nacional_estrangeiro() {
		return registro_nacional_estrangeiro;
	}
	public void setRegistro_nacional_estrangeiro(String registro_nacional_estrangeiro) {
		this.registro_nacional_estrangeiro = registro_nacional_estrangeiro;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getData_primeira_habilitacao() {
		return data_primeira_habilitacao;
	}
	public void setData_primeira_habilitacao(String data_primeira_habilitacao) {
		this.data_primeira_habilitacao = data_primeira_habilitacao;
	}
	public String getData_validade() {
		return data_validade;
	}
	public void setData_validade(String data_validade) {
		this.data_validade = data_validade;
	}
	public String getData_ultima_emissao() {
		return data_ultima_emissao;
	}
	public void setData_ultima_emissao(String data_ultima_emissao) {
		this.data_ultima_emissao = data_ultima_emissao;
	}
	public String getCodigo_situacao() {
		return codigo_situacao;
	}
	public void setCodigo_situacao(String codigo_situacao) {
		this.codigo_situacao = codigo_situacao;
	}
	public String getDescricao_situacao() {
		return descricao_situacao;
	}
	public void setDescricao_situacao(String descricao_situacao) {
		this.descricao_situacao = descricao_situacao;
	}
	 
	 
	
	
}
