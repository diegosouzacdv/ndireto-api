package com.ndireto.api.dto.datavalid.retorno;

public class CnhRetorno {

	 private float nome_similiridade =  0;
	 private boolean numero_registro =  false;
	 private boolean  registro_nacional_estrangeiro = false;
	 private boolean  categoria =  false;
	 private boolean  data_primeira_habilitacao = false;
	 private boolean  data_validade =  false;
	 private boolean  data_ultima_emissao =  false;
	 private boolean  codigo_situacao =  false;
	 private boolean  descricao_situacao =  false;
	public float getNome_similiridade() {
		return nome_similiridade;
	}
	public void setNome_similiridade(float nome_similiridade) {
		this.nome_similiridade = nome_similiridade;
	}
	public boolean isNumero_registro() {
		return numero_registro;
	}
	public void setNumero_registro(boolean numero_registro) {
		this.numero_registro = numero_registro;
	}
	public boolean isRegistro_nacional_estrangeiro() {
		return registro_nacional_estrangeiro;
	}
	public void setRegistro_nacional_estrangeiro(boolean registro_nacional_estrangeiro) {
		this.registro_nacional_estrangeiro = registro_nacional_estrangeiro;
	}
	public boolean isCategoria() {
		return categoria;
	}
	public void setCategoria(boolean categoria) {
		this.categoria = categoria;
	}
	public boolean isData_primeira_habilitacao() {
		return data_primeira_habilitacao;
	}
	public void setData_primeira_habilitacao(boolean data_primeira_habilitacao) {
		this.data_primeira_habilitacao = data_primeira_habilitacao;
	}
	public boolean isData_validade() {
		return data_validade;
	}
	public void setData_validade(boolean data_validade) {
		this.data_validade = data_validade;
	}
	public boolean isData_ultima_emissao() {
		return data_ultima_emissao;
	}
	public void setData_ultima_emissao(boolean data_ultima_emissao) {
		this.data_ultima_emissao = data_ultima_emissao;
	}
	public boolean isCodigo_situacao() {
		return codigo_situacao;
	}
	public void setCodigo_situacao(boolean codigo_situacao) {
		this.codigo_situacao = codigo_situacao;
	}
	public boolean isDescricao_situacao() {
		return descricao_situacao;
	}
	public void setDescricao_situacao(boolean descricao_situacao) {
		this.descricao_situacao = descricao_situacao;
	}
	 
	 
	
	 
	
}
