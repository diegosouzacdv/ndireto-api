package com.ndireto.api.dto.datavalid.retorno;

import java.util.List;

public class AnswerRetorno {
	
	private boolean nome =false;
	private float nomeSimilaridade =0;
	private boolean sexo = false;
	private boolean data_nascimento = false;
	private boolean situacao_cpf = false;
	private BiometriaFace biometria_face;
	private BiometriaDigital biometria_digital;
	private List<DigitalRetorno> digitais;
	private FiliacaoRetorno filiacao  = new FiliacaoRetorno();
	private boolean nacionalidade =  false;
	private EnderecoRetorno endereco = new EnderecoRetorno();
	private DocumentoRetorno documento = new DocumentoRetorno();
	private CnhRetorno cnh = new CnhRetorno();
	public boolean isNome() {
		return nome;
	}
	public void setNome(boolean nome) {
		this.nome = nome;
	}
	public float getNomeSimilaridade() {
		return nomeSimilaridade;
	}
	public void setNomeSimilaridade(float nomeSimilaridade) {
		this.nomeSimilaridade = nomeSimilaridade;
	}
	public boolean isSexo() {
		return sexo;
	}
	public void setSexo(boolean sexo) {
		this.sexo = sexo;
	}
	public boolean isData_nascimento() {
		return data_nascimento;
	}
	public void setData_nascimento(boolean data_nascimento) {
		this.data_nascimento = data_nascimento;
	}
	public boolean isSituacao_cpf() {
		return situacao_cpf;
	}
	public void setSituacao_cpf(boolean situacao_cpf) {
		this.situacao_cpf = situacao_cpf;
	}
	
	
	
	public BiometriaFace getBiometria_face() {
		return biometria_face;
	}
	public void setBiometria_face(BiometriaFace biometria_face) {
		this.biometria_face = biometria_face;
	}
	public List<DigitalRetorno> getDigitais() {
		return digitais;
	}
	public void setDigitais(List<DigitalRetorno> digitais) {
		this.digitais = digitais;
	}
	public FiliacaoRetorno getFiliacao() {
		return filiacao;
	}
	public void setFiliacao(FiliacaoRetorno filiacao) {
		this.filiacao = filiacao;
	}
	public boolean isNacionalidade() {
		return nacionalidade;
	}
	public void setNacionalidade(boolean nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public EnderecoRetorno getEndereco() {
		return endereco;
	}
	public void setEndereco(EnderecoRetorno endereco) {
		this.endereco = endereco;
	}
	public DocumentoRetorno getDocumento() {
		return documento;
	}
	public void setDocumento(DocumentoRetorno documento) {
		this.documento = documento;
	}
	public CnhRetorno getCnh() {
		return cnh;
	}
	public void setCnh(CnhRetorno cnh) {
		this.cnh = cnh;
	}
	public BiometriaDigital getBiometria_digital() {
		return biometria_digital;
	}
	public void setBiometria_digital(BiometriaDigital biometria_digital) {
		this.biometria_digital = biometria_digital;
	}
	
	
}
