package com.ndireto.api.dto.datavalid;

public class Documento {
	
	int  tipo = 1;
	String  numero  = "000001";
	String  orgao_expedidor  = "SSP";
	String  uf_expedidor  = "MG";
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getOrgao_expedidor() {
		return orgao_expedidor;
	}
	public void setOrgao_expedidor(String orgao_expedidor) {
		this.orgao_expedidor = orgao_expedidor;
	}
	public String getUf_expedidor() {
		return uf_expedidor;
	}
	public void setUf_expedidor(String uf_expedidor) {
		this.uf_expedidor = uf_expedidor;
	}
	

}
