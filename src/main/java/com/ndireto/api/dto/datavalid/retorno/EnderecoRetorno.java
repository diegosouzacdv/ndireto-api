package com.ndireto.api.dto.datavalid.retorno;

public class EnderecoRetorno {
	
	private boolean  logradouro =  false;
	private boolean  numero = false;
	private boolean  complemento = false;
	private boolean  bairro = false;
	private boolean  cep = false;
	private boolean  municipio = false;
	private boolean  uf = false;
	private float  logradouro_similaridade =  0;
	private float  numero_similaridade = 0;
	private float  complemento_similaridade = 0;
	private float  bairro_similaridade = 0;
	private float  municipio_similaridade = 0;
	
	
	public boolean isLogradouro() {
		return logradouro;
	}
	public void setLogradouro(boolean logradouro) {
		this.logradouro = logradouro;
	}
	public boolean isNumero() {
		return numero;
	}
	public void setNumero(boolean numero) {
		this.numero = numero;
	}
	public boolean isComplemento() {
		return complemento;
	}
	public void setComplemento(boolean complemento) {
		this.complemento = complemento;
	}
	public boolean isBairro() {
		return bairro;
	}
	public void setBairro(boolean bairro) {
		this.bairro = bairro;
	}
	public boolean isCep() {
		return cep;
	}
	public void setCep(boolean cep) {
		this.cep = cep;
	}
	public boolean isMunicipio() {
		return municipio;
	}
	public void setMunicipio(boolean municipio) {
		this.municipio = municipio;
	}
	public boolean isUf() {
		return uf;
	}
	public void setUf(boolean uf) {
		this.uf = uf;
	}
	public float getLogradouro_similaridade() {
		return logradouro_similaridade;
	}
	public void setLogradouro_similaridade(float logradouro_similaridade) {
		this.logradouro_similaridade = logradouro_similaridade;
	}
	public float getNumero_similaridade() {
		return numero_similaridade;
	}
	public void setNumero_similaridade(float numero_similaridade) {
		this.numero_similaridade = numero_similaridade;
	}
	public float getComplemento_similaridade() {
		return complemento_similaridade;
	}
	public void setComplemento_similaridade(float complemento_similaridade) {
		this.complemento_similaridade = complemento_similaridade;
	}
	public float getBairro_similaridade() {
		return bairro_similaridade;
	}
	public void setBairro_similaridade(float bairro_similaridade) {
		this.bairro_similaridade = bairro_similaridade;
	}
	public float getMunicipio_similaridade() {
		return municipio_similaridade;
	}
	public void setMunicipio_similaridade(float municipio_similaridade) {
		this.municipio_similaridade = municipio_similaridade;
	}
	
	
}
