package com.ndireto.api.dto.datavalid.retorno;

public class DocumentoRetorno {
	
	private boolean  tipo = false;
	private boolean   numero  = false;
	private float   numero_similaridade  = 0;
	private boolean   orgao_expedidor  = false;
	private boolean   uf_expedidor  = false;
	public boolean isTipo() {
		return tipo;
	}
	public void setTipo(boolean tipo) {
		this.tipo = tipo;
	}
	public boolean isNumero() {
		return numero;
	}
	public void setNumero(boolean numero) {
		this.numero = numero;
	}
	public float getNumero_similaridade() {
		return numero_similaridade;
	}
	public void setNumero_similaridade(float numero_similaridade) {
		this.numero_similaridade = numero_similaridade;
	}
	public boolean isOrgao_expedidor() {
		return orgao_expedidor;
	}
	public void setOrgao_expedidor(boolean orgao_expedidor) {
		this.orgao_expedidor = orgao_expedidor;
	}
	public boolean isUf_expedidor() {
		return uf_expedidor;
	}
	public void setUf_expedidor(boolean uf_expedidor) {
		this.uf_expedidor = uf_expedidor;
	}
	
	
}
