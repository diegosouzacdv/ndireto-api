package com.ndireto.api.dto.datavalid;

import com.ndireto.api.entity.Pessoa;

public class PfValidade {

	
	
	private Key key = new Key();
	private Answer answer = new Answer();
	
	public Key getKey() {
		return key;
	}
	public void setKey(Key key) {
		this.key = key;
	}
	public Answer getAnswer() {
		return answer;
	}
	public void setAnswer(Answer answer) {
		this.answer = answer;
	}
	@Override
	public String toString() {
		return "PfValidade [key=" + key + ", answer=" + answer.toString()+ "]";
	}
	
	public void converterPessoa(Pessoa pessoa,String foto) {
		key.setCpf(pessoa.getCpfSemMascara());
		
		Documento documento = new Documento();
		documento.setTipo(1);//RG
		documento.setUf_expedidor("DF");
		documento.setNumero("1954571");
		documento.setOrgao_expedidor("SSP");
		
		Endereco endereco = new Endereco();
		
		com.ndireto.api.entity.Endereco endPessoa = new com.ndireto.api.entity.Endereco();
		
		if(!pessoa.getEnderecos().isEmpty()) {
			endereco.setNumero(endPessoa.getNumero());
			endereco.setCep(pessoa.getEndereco().getLogradouro().getCep());
			endereco.setBairro(pessoa.getEndereco().getLogradouro().getBairro().getNome());
			endereco.setComplemento(pessoa.getEndereco().getComplemento());
			endereco.setLogradouro(pessoa.getEndereco().getLogradouro().getNome());
			endereco.setUf(pessoa.getEndereco().getLogradouro().getBairro().getCidade().getUfe().getSigla());
			endereco.setNumero(pessoa.getEndereco().getNumero());
			endereco.setMunicipio(pessoa.getEndereco().getLogradouro().getBairro().getCidade().getNome());
		}
		Filiacao filiacao  = new Filiacao();
		
		filiacao.setNome_mae(pessoa.getNomeMae());
		filiacao.setNome_pai(pessoa.getNomePai());
		answer.setSexo(pessoa.getSexo());
		answer.setNome(pessoa.getNome());
		answer.setFiliacao(filiacao);
		answer.setEndereco(endereco);
		answer.setDocumento(documento);
		if(foto!=null) {
			answer.setBiometria_face(foto.toString());
		}
		
		
	}
	
	
	
	
	
}
