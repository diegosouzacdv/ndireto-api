package com.ndireto.api.dto.datavalid.retorno;

public class FiliacaoRetorno {

    boolean nome_mae =  false;
    boolean nome_pai =  false;
    float nome_mae_similaridade =  0;
    float nome_pai_similaridade =  0;
    
	public boolean isNome_mae() {
		return nome_mae;
	}
	public void setNome_mae(boolean nome_mae) {
		this.nome_mae = nome_mae;
	}
	public boolean isNome_pai() {
		return nome_pai;
	}
	public void setNome_pai(boolean nome_pai) {
		this.nome_pai = nome_pai;
	}
	public float getNome_mae_similaridade() {
		return nome_mae_similaridade;
	}
	public void setNome_mae_similaridade(float nome_mae_similaridade) {
		this.nome_mae_similaridade = nome_mae_similaridade;
	}
	public float getNome_pai_similaridade() {
		return nome_pai_similaridade;
	}
	public void setNome_pai_similaridade(float nome_pai_similaridade) {
		this.nome_pai_similaridade = nome_pai_similaridade;
	}
    
    
	
}
