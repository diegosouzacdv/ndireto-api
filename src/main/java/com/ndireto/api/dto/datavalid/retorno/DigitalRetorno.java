package com.ndireto.api.dto.datavalid.retorno;

public class DigitalRetorno {

	private int posicao = 0;
	private String probabilidade = "";
	private float similaridade = 0;
	public int getPosicao() {
		return posicao;
	}
	public void setPosicao(int posicao) {
		this.posicao = posicao;
	}
	public String getProbabilidade() {
		return probabilidade;
	}
	public void setProbabilidade(String probabilidade) {
		this.probabilidade = probabilidade;
	}
	public float getSimilaridade() {
		return similaridade;
	}
	public void setSimilaridade(float similaridade) {
		this.similaridade = similaridade;
	}
	
	
}
