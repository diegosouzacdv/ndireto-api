package com.ndireto.api.dto;

import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import com.ndireto.api.entity.HistoricoFinanceiro;

public class HistoricoFinanceiroDTO {

	private int id;
	private String valor;
	private String tipo;
	private String data;
	private String hora;
	private String observacao;
	private String dataLeitura;
	private String dataConcilicao;
	
	
	public String getDataLeitura() {
		return dataLeitura;
	}
	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}
	public String getDataConcilicao() {
		return dataConcilicao;
	}
	public void setDataConcilicao(String dataConcilicao) {
		this.dataConcilicao = dataConcilicao;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public HistoricoFinanceiroDTO(int id, String valor, String tipo, String data, String observacao) {
		super();
		this.id = id;
		this.valor = valor;
		this.tipo = tipo;
		this.data = data;
		this.observacao = observacao;
	}
	public HistoricoFinanceiroDTO() {
		// TODO Auto-generated constructor stub
	}
	public HistoricoFinanceiroDTO fromDto(HistoricoFinanceiro obj) {
		
		 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		 DateTimeFormatter formatterhora = DateTimeFormatter.ofPattern("HH:mm:ss");
		 
		 NumberFormat nF 
         = NumberFormat 
               .getCurrencyInstance(Locale.getDefault()); 
		
		this.id = obj.getId();
		this.valor = nF.format(obj.getValor());
		this.tipo = obj.getTipoHistoricoFinanceiro().getNome();
		this.data = obj.getDataMovimentacao().format(formatter);
		this.hora = obj.getDataMovimentacao().format(formatterhora);
		this.observacao = obj.getObservacao();
		this.dataConcilicao = obj.getDataConciliacao() == null?null: obj.getDataConciliacao().format(formatter);
		this.dataLeitura = obj.getDataVisualizacao() == null?null: obj.getDataVisualizacao().format(formatter);
		
		
		return this;
	}
	
	
	
}
