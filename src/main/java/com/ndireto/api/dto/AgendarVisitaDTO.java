package com.ndireto.api.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.ndireto.api.entity.Imovel;
import com.ndireto.api.entity.Pessoa;
import com.ndireto.api.entity.StatusVisita;
import com.ndireto.api.entity.Visita;

public class AgendarVisitaDTO {

	private int codigo;
	private String data;
	private String horario;
	private int colaboradorCodigo;
	private int compradorCodigo;
	private int cadastranteCodigo;
	private int tipoCodigo;
	private int codigoImovel;
	private String statusVisita; 
	private String observacao; 
	
	
	
	public int getCodigo() {
		return codigo;
	}




	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}




	public String getData() {
		return data;
	}




	public void setData(String data) {
		this.data = data;
	}




	public String getHorario() {
		return horario;
	}




	public void setHorario(String horario) {
		this.horario = horario;
	}




	public int getColaboradorCodigo() {
		return colaboradorCodigo;
	}




	public void setColaboradorCodigo(int colaboradorCodigo) {
		this.colaboradorCodigo = colaboradorCodigo;
	}




	public int getCompradorCodigo() {
		return compradorCodigo;
	}




	public void setCompradorCodigo(int compradorCodigo) {
		this.compradorCodigo = compradorCodigo;
	}




	public int getCadastranteCodigo() {
		return cadastranteCodigo;
	}




	public void setCadastranteCodigo(int cadastranteCodigo) {
		this.cadastranteCodigo = cadastranteCodigo;
	}




	public int getTipoCodigo() {
		return tipoCodigo;
	}




	public void setTipoCodigo(int tipoCodigo) {
		this.tipoCodigo = tipoCodigo;
	}




	public int getCodigoImovel() {
		return codigoImovel;
	}




	public void setCodigoImovel(int codigoImovel) {
		this.codigoImovel = codigoImovel;
	}




	public String getStatusVisita() {
		return statusVisita;
	}




	public void setStatusVisita(String statusVisita) {
		this.statusVisita = statusVisita;
	}




	public String getObservacao() {
		return observacao;
	}




	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}




	public Visita fromObjeto() {
		Visita retorno = new Visita();
		
		Imovel imovel = new Imovel();
		imovel.setId(codigoImovel);
		
		Pessoa pessoaCadastrante = new Pessoa();
		Pessoa pessoaColaborador = new Pessoa();
		Pessoa pessoaComprador = new Pessoa();
		StatusVisita status = new StatusVisita();
		status.setId(1);
		
		pessoaCadastrante.setId(cadastranteCodigo);
		pessoaColaborador.setId(colaboradorCodigo);
		pessoaComprador.setId(compradorCodigo);
		
		
		String data = this.data+" "+this.horario;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		LocalDateTime dataHoraVisita = LocalDateTime.parse(data,formatter);
		
		
		retorno.setId(0);
		retorno.setImovel(imovel);
		retorno.setDataHoraCadastro(LocalDateTime.now());
		retorno.setDataHoraVisita(dataHoraVisita);
		retorno.setObservacao(observacao);
		retorno.setPessoaCadastrante(pessoaCadastrante);
		retorno.setPessoaColaborador(pessoaColaborador);
		retorno.setStatusVisita(status);
		if(compradorCodigo!=0) {
		retorno.setPessoaComprador(pessoaComprador);
		}
		retorno.setPessoaCadastrante(pessoaCadastrante);
		
		return retorno;
	}




	@Override
	public String toString() {
		return "AgendarVisitaDTO [codigo=" + codigo + ", " + (data != null ? "data=" + data + ", " : "")
				+ (horario != null ? "horario=" + horario + ", " : "") + "colaboradorCodigo=" + colaboradorCodigo
				+ ", compradorCodigo=" + compradorCodigo + ", cadastranteCodigo=" + cadastranteCodigo + ", tipoCodigo="
				+ tipoCodigo + ", codigoImovel=" + codigoImovel + ", "
				+ (statusVisita != null ? "statusVisita=" + statusVisita + ", " : "")
				+ (observacao != null ? "observacao=" + observacao : "") + "]";
	}
	
	
	
	
	
}
