package com.ndireto.api.dto;

public class CaracteristicaImovelDTO {

	private Integer codigoItem;
	private Integer codigoImovel;
	private boolean status;
	private String nome;
	
	
	
	
	
	public CaracteristicaImovelDTO(Integer codigoItem, Integer codigoImovel, boolean status, String nome) {
		super();
		this.codigoItem = codigoItem;
		this.codigoImovel = codigoImovel;
		this.status = status;
		this.nome = nome;
	}
	public CaracteristicaImovelDTO() {
	}
	public Integer getCodigoItem() {
		return codigoItem;
	}
	public void setCodigoItem(Integer codigoItem) {
		this.codigoItem = codigoItem;
	}
	public Integer getCodigoImovel() {
		return codigoImovel;
	}
	public void setCodigoImovel(Integer codigoImovel) {
		this.codigoImovel = codigoImovel;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
	
	
}
